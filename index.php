<?php 
if(!isset($_SESSION)){
	session_start();
}
if(!isset($_SESSION['error'])){
	$_SESSION['error'] = "";
}
include_once('includes/conn.php');
if(isset($_POST['submit'])){

    $email = $_POST['email'];
    $pswrd = $_POST['pswrd'];

    $verify_query ="SELECT * FROM users WHERE email = '$email' AND password = '$pswrd' ";
    $query_exec = mysqli_query($conn,$verify_query) or die("wrong information");

    if(mysqli_num_rows($query_exec) > 0){

        while($row = mysqli_fetch_array($query_exec)){
                $_SESSION['uid'] = $row['id'];
				$_SESSION['email']= $row['email'];
                $_SESSION['utype'] = $row['type'];
				$_SESSION['fullname'] = $row['fname']." ".$row['sname'];
				$_SESSION['section']=$row['section'];
                if($row['type'] == 0){

                    header('Location:admin/index.php?id='.$row['id']);

                }else if($row['type'] == 1){
					header('Location:teacher/index.php?id='.$row['id']);
                    
                }else if($row['type'] == 2){
                    
header('Location:student/index.php?id='.$row['id'].'&&sec='.$row['section']);
                }else{
                    $_SESSION['error'] = "User does not exist";
                }

        }

    }else{
        $_SESSION['error'] = "User does not exist!";
    }



}

?>


<!DOCTYPE html>

<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Welcome to DSA</title>
    <link rel="stylesheet" href="css/login-style.css">
  </head>
  <body>
    <div class="wrapper">
      <div class="title"> Login Form</div>
        <form action="" method="POST">
            
            <div class="field">
                <input type="text" name="email" required>
                <label>Email Address</label>
            </div>

            <div class="field">
                <input type="password" name="pswrd"required>
                <label>Password</label>
            </div>

            <div class="content">

            <?php echo $_SESSION['error']; unset($_SESSION['error']); ?>
                    <!-- <div class="checkbox">
                        <input type="checkbox" id="remember-me">
                        <label for="remember-me">Remember me</label>
                    </div> -->
            <div class="pass-link">
                <!-- <a href="#">Forgot password?</a></div> -->
            </div> 

            <div class="field">
                <input type="submit" value="Login" name="submit">
            </div>
            <!-- <div class="signup-link">
            Not a member? <a href="#">Signup now</a></div> -->
        </form>
    </div>
</body>
</html>
