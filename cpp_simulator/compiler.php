<!DOCTYPE html>
<html>
<head>
  
    
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
       
        <meta name="description" content="">
       
        <link rel="icon" type="../cpp_simulator/image/png" href="img/ruet.png">
        <script src="../cpp_simulator/js/vendor/modernizr-2.8.3.min.js"></script>
        <script src="../cpp_simulator/js/vendor/jquery-1.12.0.min.js"></script>








</head>
<body>
<div class="main">





<div class="col-sm-10">
<div class=""><h3 style="text-align:center;">CPP Compiler</h3></div>
</div>


<form action="compile.php" id="form" name="f2" method="POST" >
<br>





<div id="left-simu">
				<label for="ta">Write Your Code</label>
<textarea class="form-control" id="code_panel" placeholder="Enter C++ Code here" required name="code" rows="10" cols="50"></textarea>
</div>
<script>
//wait for page load to initialize script
$(document).ready(function(){
    //listen for form submission
    $('form').on('submit', function(e){
      //prevent form from submitting and leaving page
      e.preventDefault();

      // AJAX 
      $.ajax({
            type: "POST", //type of submit
            cache: false, //important or else you might get wrong data returned to you
            url: "../cpp_simulator/compile.php", //destination
            datatype: "html", //expected data format from process.php
            data: $('form').serialize(), //target your form's data and serialize for a POST
            success: function(result) { // data is the var which holds the output of your process.php

                // locate the div with #result and fill it with returned data from process.php
                $('#div').html(result);
            }
        });
    });
});
</script>
<div id="right-simu">
<label for="out">Output</label>
<textarea id='div' id="code_panel" placeholder="Enter C++ Code here" required class="form-control" name="output" rows="10" cols="50"></textarea>
</div>
<div id="right-simu">
<label for="in">Enter Your Input<br>
ONE(1) line per input
</label>
<textarea class="form-control" name="input" rows="5" cols="30"></textarea><br>
<input type="submit" id="st" class="btn btn-success" value="Run Code"><br><br><br>
</div>



</form>

<script type="text/javascript">
  
  $(document).ready(function(){

     $("#st").click(function(){
  
           $("#div").html("Loading ......");


     });

  });


</script>



<!--<script>
"use strict";
function submitForm(oFormElement)
{
  var xhr = new XMLHttpRequest();
  var display=document.getElementById('div');
  xhr.onload = function(){ display.innerHTML=xhr.responseText; }
  xhr.open (oFormElement.method, oFormElement.action, true);
  xhr.send (new FormData (oFormElement));
  return false;
}
</script>-->
<!--<label for="out">Output</label>
<textarea id='div' class="form-control" name="output" rows="10" cols="50"></textarea><br><br>-->








</div>

</body>
</html>


