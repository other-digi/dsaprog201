<!DOCTYPE html>
<html lang="en">

<?php
SESSION_START();
include('includes/head.php');
$userId = $_GET['id'];
?>

<body>
	
  <!-- Header bar -->
    <?php include('header.php');?>
  <!-- end of  Header bar -->
  <div class="d-flex" id="wrapper">
	
		<!-- Sidebar -->
		<?php include('menu.php');?>
		<!-- /#sidebar-wrapper -->

		<!-- Page Content -->
		<div id="page-content-wrapper">

			
			<div class="container-fluid">
				
				<div class=""  id="table-wrapper">
					<table id="module_listing">
					<tr>
						<th>Modules</th>
						<th>Exams</th>
						<th>Actions</th>
					</tr>
						
	  <?php 
            $get_menu = "SELECT * FROM modules WHERE module_creator=".$userId." ORDER BY name ASC";
            $get_menu_query = mysqli_query($conn,$get_menu);
            
			if(mysqli_num_rows($get_menu_query)>0){
				
			
				while($menu = mysqli_fetch_array($get_menu_query)){
					echo "<tr><td class='module_name'>".$menu['name']."</td></tr>";
					echo "<tr>";
					echo "<td>&nbsp;</td>";
						///enrichment 1
						$e1 ="SELECT * FROM tbl_exam WHERE exam_name LIKE 'enrich1' && exam_creator=".$userId." && module_id=".$menu['id']." LIMIT 1";
						$e1_r = mysqli_query($conn,$e1)or die("ERROR: ".mysqli_error($conn));
						if(mysqli_num_rows($e1_r)>0){
							$get_assessa1 = mysqli_fetch_array($e1_r);
							if($get_assessa1['status']=='disabled'){
								echo "<td class='module_files'> ENRICMENT 1 </td>
								<td>
									<a href='exam/editexam.php?q=editexam&eid=".$get_assessa1['eid']."&id=".$_GET['id']."' class='btn btn-warning'>Edit</a>
									 | <a href='exam/update.php?eeidquiz=".$get_assessa1['eid']."&id=".$userId."'class='btn btn-success'>Enable</a> | <a href='exam/update.php?q=rmquiz&id=".$userId."&eid=".$get_assessa1['eid']."'class='btn btn-danger'>Delete</a></td>";
							}else{
								echo "<td class='module_files'>ENRICMENT 1</td>
								<td> <a href='exam/editexam.php?q=editexam&eid=".$get_assessa1['eid']."&id=".$_GET['id']."' class='btn btn-warning'>Edit</a> | <a href='exam/update.php?deidquiz=".$get_assessa1['eid']."&id=".$userId."'class='btn btn-danger'>Disable</a> | <a href='exam/update.php?q=rmquiz&id=".$userId."&eid=".$get_assessa1['eid']."'class='btn btn-danger'>Delete</a></td>";
							}
						}else{
							echo "<td class='module_files'>ENRICHMENT 1</td><td><a class='btn btn-primary' href='exam/addexam.php?q=4&&id=".$userId."&&name=".$Fname."&&mid=".$menu['id']."&&ename=enrich1'>Add Exam</a></td>";
						}
					echo "</tr>";
					
						////enrrichemnt 2
					echo "<tr>";
					echo "<td>&nbsp;</td>";
						$e2 ="SELECT * FROM tbl_exam WHERE exam_name LIKE 'enrich2' && exam_creator=".$userId. "&& module_id=".$menu['id']. " LIMIT 1";
						$e2_r = mysqli_query($conn,$e2)or die("ERROR: ".mysqli_error($conn));
						if(mysqli_num_rows($e2_r)>0){
							$get_assessa2 = mysqli_fetch_array($e2_r);
							if($get_assessa2['status']=='disabled'){
								echo "<td class='module_files'> ENRICMENT 2 </td>
								<td> <a href='exam/editexam.php?q=editexam&eid=".$get_assessa2['eid']."&id=".$_GET['id']."'class='btn btn-warning'>Edit</a> | <a href='exam/update.php?eeidquiz=".$get_assessa2['eid']."&id=".$userId."'class='btn btn-success'>Enable</a> | <a href='exam/update.php?q=rmquiz&id=".$userId."&eid=".$get_assessa2['eid']."'class='btn btn-danger'>Delete</a></td>";
							}else{
								echo "<td class='module_files'>ENRICMENT 2 </td><td> <a href='exam/editexam.php?q=editexam&eid=".$get_assessa2['eid']."&id=".$_GET['id']."'class='btn btn-warning'>Edit</a> | <a href='exam/update.php?deidquiz=".$get_assessa2['eid']."&id=".$userId."'class='btn btn-danger'>Disable</a>
								| <a href='exam/update.php?q=rmquiz&id=".$userId."&eid=".$get_assessa2['eid']."'class='btn btn-danger'>Delete</a></td>";
							}
						}else{
								echo "<td class='module_files'>ENRICHMENT 2</td><td><a class='btn btn-primary' href='exam/addexam.php?q=4&&id=".$userId."&&name=".$Fname."&&mid=".$menu['id']."&&ename=enrich2'>Add Exam</a></td>";
						}
					echo "</tr>";	
						///enrichemnt 3
					echo "<tr>";
					echo "<td>&nbsp;</td>";
						$e3 ="SELECT * FROM tbl_exam WHERE exam_name LIKE 'enrich3' && exam_creator=".$userId." && module_id=".$menu['id']. " LIMIT 1";
						$e3_r = mysqli_query($conn,$e3)or die("ERROR: ".mysqli_error($conn));
						if(mysqli_num_rows($e3_r)>0){
							$get_assessa = mysqli_fetch_array($e3_r);
							if($get_assessa['status']=='disabled'){
								echo "<td class='module_files'> ENRICMENT 3 </td><td><a href='exam/editexam.php?q=editexam&eid=".$get_assessa['eid']."&id=".$_GET['id']."' class='btn btn-warning'>Edit</a> | <a href='exam/update.php?eeidquiz=".$get_assessa['eid']."&id=".$userId."'class='btn btn-success'>Enable</a>| <a href='exam/update.php?q=rmquiz&id=".$userId."&eid=".$get_assessa['eid']."'class='btn btn-danger'>Delete</a></td>";
							}else{
								echo "<td class='module_files'>ENRICMENT 3</td><td> <a href='exam/editexam.php?q=editexam&eid=".$get_assessa['eid']."&id=".$_GET['id']."'class='btn btn-warning'>Edit</a> | <a href='exam/update.php?deidquiz=".$get_assessa['eid']."&id=".$userId."'class='btn btn-danger'>Disable</a>| <a href='exam/update.php?q=rmquiz&id=".$userId."&eid=".$get_assessa['eid']."'class='btn btn-danger'>Delete</a></td>";
							}
						}else{
								echo "<td class='module_files'>ENRICHMENT 3</td><td><a class='btn btn-primary' href='exam/addexam.php?q=4&&id=".$userId."&&name=".$Fname."&&mid=".$menu['id']."&&ename=enrich3'>Add Exam</a></td>";
						}
					echo "</tr>";	
						////module ASSESSMENT
					echo "<tr>";
					echo "<td>&nbsp;</td>";
						$me ="SELECT * FROM tbl_exam WHERE exam_name='".$menu['aid']."' && exam_creator=".$userId. " && module_id=".$menu['id']." LIMIT 1";
						$me_r = mysqli_query($conn,$me)or die("ERROR: ".mysqli_error($conn));
						if(mysqli_num_rows($me_r)>0){
							
							$get_assess = mysqli_fetch_array($me_r);
							if($get_assess['status']=='disabled'){
								echo "<td class='module_files'>".$menu['name']." ASSESSMENT </td><td><a href='exam/editexam.php?q=editexam&eid=".$get_assess['eid']."&id=".$_GET['id']."' class='btn btn-warning'>Edit</a> | <a href='exam/update.php?eeidquiz=".$get_assess['eid']."&id=".$userId."'class='btn btn-success'>Enable</a>| <a href='exam/update.php?q=rmquiz&id=".$userId."&eid=".$get_assess['eid']."'class='btn btn-danger'>Delete</a></td>";
							}else{
								echo "<td class='module_files'>".$menu['name']." ASSESSMENT </td><td><a href='exam/editexam.php?q=editexam&eid=".$get_assess['eid']."&id=".$_GET['id']."'class='btn btn-warning'>Edit</a> | <a href='exam/update.php?deidquiz=".$get_assess['eid']."&id=".$userId."'class='btn btn-danger'>Disable</a>| <a href='exam/update.php?q=rmquiz&id=".$userId."&eid=".$get_assess['eid']."'class='btn btn-danger'>Delete</a></td>";
							}
	
						}else{
							echo "<td class='module_files'>".$menu['name']." ASSESSMENT</td><td><a class='btn btn-primary' href='exam/addexam.php?q=4&&id=".$userId."&&name=".$Fname."&&mid=".$menu['id']."&&ename=".$menu['aid']."'>Add Exam</a></td>";
						}
						
					echo "</tr>";	
				}
			}else{
				echo "<h1>NO Modules Created, Create Module First</h1>";
			}
		?>
		 
	  </table>

      
</div>
			
			</div>
	     
		</div>
    <!-- /#page-content-wrapper -->
	  <div class="bg-light border-right" id="sidebar-wrapper">
		<?php include_once('../includes/bot.php');?>
      </div>
  </div>
  <!-- /#wrapper -->
	
  <!-- Bootstrap core JavaScript -->
  <script src="../vendor/jquery/jquery.min.js"></script>
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <script src="../js/active_page.js"></script>

</body>

</html>
