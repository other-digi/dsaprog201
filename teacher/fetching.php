<?php  
session_start();
//index.php
$connect = mysqli_connect("localhost", "root", "", "project_dsa");
$query = "SELECT * FROM announcements ORDER BY id DESC";
$result = mysqli_query($connect, $query);
 ?>  
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Data Tables</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- <link rel="stylesheet" href="datatables/bootstrap.min.css" > -->
		<link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>  
		<!-- <link rel="stylesheet" href="../vendor/bootstrap/css/bootstrap.min.css" >  -->
		<link href="../css/sidebar.css" rel="stylesheet">
		<link href="../css/style.css" rel="stylesheet">
		<script src="datatables/jquery-3.3.1.min.js"></script>
		<link rel="stylesheet" href="datatables/jquery.dataTables.min.css">
		<script src="datatables/jquery.dataTables.min.js" ></script>
	</head>
	<body>


			<div class="container">
				<h2>Simple Pagination Example using Datatables Js Library</h2>
				<div align="right">
				 <button type="button" name="age" id="age" data-toggle="modal" data-target="#add_data_Modal" class="btn btn-warning">Add</button>
				</div>
				<br />
				<table class="table table-fluid" id="announcements" width="800px">
					<thead>
						<tr>
							<th>#</th>
							<th>Title</th>
							<th>Created by</th>
							<th>Post Created</th>
							<th>Action</th>  
						</tr>
					</thead>
					
					<tbody>
						  <?php
						  $count = 1;
						  while($row = mysqli_fetch_array($result))
						  {
						  ?>
						  
							<tr>
								<td><?php echo $count;?></td>
								<td><?php echo $row['title'];?></td>
								<td><?php echo $row['post_creator'];?></td>
								<td><?php echo $row['post_created'];?></td>
							   <td><input type="button" name="view" value="view" id="<?php echo $row["id"]; ?>" class="btn btn-info btn-xs view_data" /></td>
							</tr>
						  <?php
						  $count++;
						  }
					  ?>
					  </tbody>
					  
					  <tfoot>
					 
							<tr>
							  <th>#</th>
							  <th>Title</th>
							  <th>Created by</th>
							  <th>Post Created</th>
							  <th>Action</th>
							</tr>
					  
					  </tfoot>
				</table>
			</div>

	
	

	<div id="add_data_Modal" class="modal fade">
	 <div class="modal-dialog">
	  <div class="modal-content">
	   <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<br />
		<h4 class="modal-title">Announcement</h4>
	   </div>
	   <div class="modal-body">
		<form method="post" id="insert_form">
		 <label>Title</label>
		 <input type="text" name="title" id="title" class="form-control" />
		 <br />
		 <label>Content</label>
		 <textarea name="content" id="content" class="form-control"></textarea>
		 <br />
		 <input type="submit" name="insert" id="insert" value="Insert" class="btn btn-success" />

		</form>
	   </div>
	   <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	   </div>
	  </div>
	 </div>
	</div>

	<div id="dataModal" class="modal fade">
	 <div class="modal-dialog">
	  <div class="modal-content">
	   <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Announcement</h4>
	   </div>
	   <div class="modal-body" id="announce_detail">
		
	   </div>
	   <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	   </div>
	  </div>
	 </div>
	</div>

	<script>
		$(document).ready( function () {
		$('#announcements').DataTable();
	} );

	$(document).on('click', '.view_data', function(){
	  //$('#dataModal').modal();
	  var announce_id = $(this).attr("id");
	  $.ajax({
	   url:"view_ann.php",
	   method:"POST",
	   data:{announce_id:announce_id},
	   success:function(data){
		$('#announce_detail').html(data);
		$('#dataModal').modal('show');
	   }
	  });
	 });

	 $(document).ready(function(){
	 $('#insert_form').on("submit", function(event){  
	  event.preventDefault();  
	  if($('#title').val() == "")  
	  {  
	   alert("Title is required");  
	  }  
	  else if($('#content').val() == '')  
	  {  
	   alert("Content is required");  
	  }  
	  else  
	  {  
	   $.ajax({  
		url:"add_message.php",  
		method:"POST",  
		data:$('#insert_form').serialize(),  
		beforeSend:function(){  
		 $('#insert').val("Inserting");  
		},  
		success:function(data){  
			window.location.reload(); 
		 $('#insert_form')[0].reset();  
		 $('#add_data_Modal').modal('hide');  
		 $('#announce_table').html(data);  
		}  
	   });  
	  }  
	 });
	});  
	</script>

	<!-- <script src="../vendor/jquery/jquery.min.js"></script> -->
	  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	</body>
</html>