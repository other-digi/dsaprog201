
<?php

include('../includes/conn.php');

//$uid = $_SESSION['uid'];
$userId = $_GET['id'];
?>
<div class=""  id="table-wrapper">
     <ul id="module_listing">
	 <div id="add_delete" align="right">
	 <?php 
		 $get_menu = "SELECT * FROM modules WHERE module_creator=".$userId." ORDER BY name ASC";
            $get_menu_query = mysqli_query($conn,$get_menu);
            
			if(mysqli_num_rows($get_menu_query)< 3) {
				
				$get_num_modules="SELECT * FROM modules WHERE module_creator='$userId'";
				$gnm_query = mysqli_query($conn,$get_num_modules);
				
				if(mysqli_num_rows($gnm_query) > 0){
					$count_mod = "SELECT COUNT(*) AS CtrMdl FROM modules WHERE module_creator='$userId'";
					$count_mod1 = "SELECT aid FROM modules WHERE module_creator='$userId' ORDER BY aid ASC";
					$cm_query = mysqli_query($conn,$count_mod);
					$cm_query1 = mysqli_query($conn,$count_mod1);
					$data=mysqli_fetch_assoc($cm_query);
					//echo $data['CtrMdl'];
					$modules_crea =array();
					$mod_ctr = 0;
					$mods="";
					if($data['CtrMdl'] == 1){
						$nameM = "Module 2";
					}else{
						 while($row = mysqli_fetch_array($cm_query1)) { 
						 $the_rows[] = $row;
						 
						 }
		/// later use like:
			foreach($the_rows as $row)
			{	
				$modules_crea[$mod_ctr] = $row['aid'];
				//echo $modules_crea[$mod_ctr];
				$mod_ctr++;
			}
						 
						if($modules_crea[0] == 'module1' && $modules_crea[1] == 'module2'){
							$nameM = "Module 3";
						}else if($modules_crea[0] == 'module2' && $modules_crea[1] == 'module3'){
							$nameM = "Module 1";
						}else if($modules_crea[0] == 'module1' && $modules_crea[1] == 'module3'){
							$nameM = "Module 2";
						}else{
							$nameM = "ERROR";
						}
					}
				}else{
					$nameM = "Module 1";
				}
	 
	 ?>
	 <button type="button" name="age" id="age" data-toggle="modal" data-target="#add_data_Modal" class="btn btn-success btn-sm rounded-0" title="Add Module"><i class="fa fa-plus"></i></button>
          <button class="btn btn-danger btn-sm rounded-0 delete_module" data-target="#deleteModal" type="button" data-toggle="modal" data-placement="top" title="Delete Module"><i class="fa fa-trash"></i></button>
		  
	<?php 
	}else{
		
 ?>
  <button class="btn btn-danger btn-sm rounded-0 delete_module" data-target="#deleteModal" type="button" data-toggle="modal" data-placement="top" title="Delete Module"><i class="fa fa-trash"></i></button>
<?php	}
	?>
	 </div>
	  <?php 
            $get_menu = "SELECT * FROM modules WHERE module_creator=".$userId." ORDER BY name ASC";
            $get_menu_query = mysqli_query($conn,$get_menu);
            
			if(mysqli_num_rows($get_menu_query)< 3) {
				
			}
            while($menu = mysqli_fetch_array($get_menu_query)){
				echo "<li class='module_name'>".$menu['name'];
			
				
				?>
				<a href="<?php echo "?mid=".$menu['id'];?>" data-toggle="modal" id ="<?php echo $menu['id'];?>" data-id="<?php echo $menu['id'];?>" data-target="#uploadModal" title="Add file" class="btn btn-success btn-sm rounded-0 add_file">Add File <i class="fa fa-plus"></i></a>
			<?php		
			///////////getting files corresponding to the module
				$get_files ="SELECT * FROM files WHERE mid=".$menu['id']." && file_creator=".$userId." ORDER BY file_created ASC";
				$get_files_query= mysqli_query($conn,$get_files);
				echo "<table border='0' width='90%'>";
				while($file = mysqli_fetch_array($get_files_query)){
					
					echo "
						
						<tr style='border: none; padding: 10px;'>
						<td style='border: none; padding: 10px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td style='border: none; padding: 10px;' class='module_files'><a href='../pdfviewer/web/viewer.html?file=../../files/uploads/".$file['fname']."' target='_blank'>".$file['title']."</a></td>
						
						<td style='border: none; text-align: right; margin-right: 1em;'>
							<a data-id=".$file["id"]." id=".$file["id"]." class='btn-warning btn-xs archive_file' title='DELETE' ><i class='fa fa-trash'></i>
							</a>
						</td>
						</tr>";
				}
				echo "</table>";
				if (!$get_files_query) {
					printf("Error: %s\n", mysqli_error($conn));
					exit();
				}
				echo "</li>";		
			}
		?>
		 
	  </ul>

      
</div>

 <!-----ADD MODAL----->
 <div id="add_data_Modal" class="modal fade">
	 <div class="modal-dialog">
	  <div class="modal-content">
	   <div class="modal-header">
     <h4 class="modal-title">Add Module</h4>
		<button type="button" class="close" data-dismiss="modal">&times;</button>

	
	   </div>
	   <div class="modal-body">
		<form method="post" id="insert_form">
		 <label>Title</label>
		 <input type="text" name="title" id="title" value="<?php echo $nameM;?>" class="form-control" disabled />
		 <input type="hidden" name="titles" id="titles" value="<?php echo $nameM;?>" class="form-control" />
		 <br />
		 <label>Description</label>
		 <textarea name="description" id="description" class="form-control"></textarea>
		 <br />
		 <input type="hidden" name="userId" value="<?php echo $userId;?>">
		 <input type="submit" name="insert" id="insert" value="Insert" class="btn btn-success" />

		</form>
	   </div>
	   <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	   </div>
	  </div>
	 </div>
	</div>
<!-----END OF ADD MODAL------>


 <!-----DELETE MODAL----->
 <div id="deleteModal" class="modal fade">
	 <div class="modal-dialog">
	  <div class="modal-content">
	   <div class="modal-header">
     <h4 class="modal-title">Delete Module</h4>
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		
	   </div>
	   <div class="modal-body" id="module_detail">
			<form method="POST" id="delete_module">
				<select name="module_id">
				<?php
				$get_modules = "SELECT * FROM modules WHERE module_creator=$userId ORDER BY name ASC";
				$get_modules_query = mysqli_query($conn,$get_modules);
				$output = "";
				while($modules = mysqli_fetch_array($get_modules_query)){

					$output .= '<option value="'.$modules['id'].'">'.$modules['name'].'</option>';

				}
				echo $output;
				?>
				
				
				</select>
				<input type="submit" name="delete" id="deleteM" value="Delete" class="btn btn-warning" />
			</form>
	   </div>
	   <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	   </div>
	  </div>
	 </div>
	</div>
<!-----END OF DELETE MODAL------>

<!----- Confirmation MODAL of DELETE MODULE------>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal">
  <div class="modal-dialog modal-l">
    <div class="modal-content">
      <div class="modal-header">
	  <h4 class="modal-title" id="myModalLabel">Are you Sure to Delete?</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" id="modal-btn-si">Yes</button>
        <button type="button" class="btn btn-primary" id="modal-btn-no">No</button>
      </div>
    </div>
  </div>
</div>
<!----- Confirmation MODAL ENDs HERE------>

<!----- Confirmation MODAL of DELETE FILE------>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="files-modal">
  <div class="modal-dialog modal-l">
    <div class="modal-content">
      <div class="modal-header">
	  <h4 class="modal-title" id="myModalLabel">Are you Sure to Delete this FILE?</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       
      </div>
	  
	  <div id="modal-body">
		<form  method="POST" id="del_file">
			<input type="hidden" id="fId" name="fId" value="">
			<button type="submit" class="btn btn-warning delete" id="modal-btn-sif">Yes</button>
			<button type="button" class="btn btn-primary" id="modal-btn-nof">No</button>
		</form>
	  </div>
	  
      <div class="modal-footer">
		
        
        
      </div>
    </div>
  </div>
</div>
<!----- Confirmation MODAL DELETE FILE ENDs HERE------>

 <!-----ADD File MODAL----->

 <div id="uploadModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
				<h4 class="modal-title">File upload form</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    
                </div>
                <div class="modal-body">
                    <!-- Form -->
                    <form method='post' action='' id="uploadform" enctype="multipart/form-data">
					<input type='hidden' name='mid' id='mid' class='form-control' >
					<input type='hidden' name='userID' id="userID" value="<?php echo $userId;?>">
					Title : <input type='text' name='ftitle' id='ftitle' class='form-control' ><br>
					Description : <input type='text' name='fdescription' id='fdescription' class='form-control' ><br>
                        Select file : <input type='file' name='file' id='file' class='form-control' ><br>
					
                        <input type='button' class='btn btn-info' value='Upload' id='btn_upload'>
                    </form>

                    <!-- Preview-->
                    <div  role="alert" id='preview'></div>
                </div>
                
            </div>

          </div>
        </div>
<!-----END OF ADD File MODAL------>



  <script>


	// $(document).on('click', '.delete_module', function(){
	//   //$('#dataModal').modal();
	//   var module_id = $(this).attr("id");
	//   $.ajax({
	//    url:"delete_module.php",
	//    method:"POST",
	//    data:{module_id:module_id},
	//    success:function(data){
	// 	$('#module_detail').html(data);
	// 	$('#deleteModal').modal('hide');
	//    }
	//   });
	//  });

/////ADD MODULE
	 $(document).ready(function(){
	 $('#insert_form').on("submit", function(event){  
	  event.preventDefault();  
	  if($('#title').val() == "")  
	  {  
	   alert("Title is required");  
	  }  
	  else if($('#description').val() == '')  
	  {  
	   alert("Description is required");  
	  }  
	  else  
	  {  
	   $.ajax({  
		url:"tab/add_module.php",  
		method:"POST",  
		data:$('#insert_form').serialize(),  
		beforeSend:function(){  
		 $('#insert').val("Inserting");  
		},  
		success:function(data){  
			window.location.reload(); 
		 $('#insert_form')[0].reset();  
		 $('#add_data_Modal').modal('hide');  
		// $('#announce_table').html(data);  
		}  
	   });  
	  }  
	 });
	});  
//// ADD MODULE ENDS HERE


////DELETE MODULE /////////////
$(document).ready(function(){
	 $('#delete_module').on("submit", function(event){  
	  event.preventDefault();  
	  if($('#module_id').val() == "")  
	  {  
	   alert("Select one Item to delete");  
	  } 
	  else  
	  {  
	//    $.ajax({  
	// 	url:"tab/delete_module.php",  
	// 	method:"POST",  
	// 	data:$('#delete_module').serialize(),  
	// 	beforeSend:function(){  
	// 	 $('#delete').val("Deleting");  
	// 	},  
	// 	success:function(data){  
	// 		window.location.reload(); 
	// 	 $('#delete_module')[0].reset();  
	// 	 $('#deleteModal').modal('hide');  
	// 	// $('#announce_table').html(data);  
	// 	}  
	//    });  
	  }  
	 });
	});  
////DELETE MODULE ENDS HERE /////////////


////////////
var modalConfirm = function(callback){
  
  $("#deleteM").on("click", function(){
    $("#mi-modal").modal('show');
  });

  $("#modal-btn-si").on("click", function(){
    callback(true);
    $("#mi-modal").modal('hide');
  });
  
  $("#modal-btn-no").on("click", function(){
    callback(false);
    $("#mi-modal").modal('hide');
  });
};

modalConfirm(function(confirm){
  if(confirm){
    //Acciones si el usuario confirma
	$.ajax({  
		url:"tab/delete_module.php",  
		method:"POST",  
		data:$('#delete_module').serialize(),  
		beforeSend:function(){  
		 $('#delete').val("Deleting");  
		},  
		success:function(data){  
			window.location.reload(); 
		 $('#delete_module')[0].reset();  
		 $('#deleteModal').modal('hide');  
		// $('#announce_table').html(data);  
		}  
	   });  
  }else{
    //Acciones si el usuario no confirma
    // $("#result").html("NO CONFIRMADO");
  }
});

////////////


/////ADD File
$(document).ready(function(){
            $('#btn_upload').click(function(){
				var ftitle = $('#ftitle').val();
				var fdescription = $('#fdescription').val();
				var mid = $('#mid').val();
				var userID = $('#userID').val();
                var fd = new FormData();
                var files = $('#file')[0].files[0];
                fd.append('file',files);
				fd.append('ftitle',ftitle);
				fd.append('fdescription',fdescription);
				fd.append('mid',mid);
				fd.append('fuserID',userID);
				var dataId = $(this).attr("data-id");
                // AJAX request
                $.ajax({
                    url: 'tab/add_file.php',
                    type: 'post',
                    data: fd,
                    contentType: false,
                    processData: false,
                    success: function(response){
                        if(response == 0){
							
                            //$('#preview').html(response);
							console.log(response);
                            $('#preview').addClass("alert alert-danger");
                            $('#preview').append("File Not Uploaded Successfully");
							 //$('#uploadform')[0].reset();  
							 $('#preview').addClass("");
							 response = 0;
							
                        }else if(response == 1){
							
							 
							 // Show image preview
							//$('#preview').html(response);
							console.log(response);
							$('#preview').addClass("alert alert-success");
                            $('#preview').append("File Uploaded Successfully");
							// $('#uploadform')[0].reset();  
							 $('#preview').addClass("");
							 response = 0;
							  window.setTimeout(function(){
								$('#uploadModal').modal('hide'); 
								$('.modal-backdrop').remove(); 
								location.reload(); 
								}, 3000);
							
							
                        }else{
							 // Show image preview
							//$('#preview').html(response);
							console.log(response);
							$('#preview').addClass("alert alert-danger");
                            $('#preview').append("File extension must be PDF FILE(.pdf)");
							// $('#uploadform')[0].reset();  
							 $('#preview').addClass("");
							 response = 0;
							 //$('#uploadModal').modal('hide'); 
							// $('.modal-backdrop').remove(); 
							 //location.reload(); 
						}
                    }
                });
            });
        });
//// ADD File ENDS HERE





			$(document).ready(function(){
			$("a").click(function(){
				var dataId = $(this).attr("data-id");
				$('#mid').val(dataId);
			});
			});

			
			
			$(document).ready(function(){
			$("a").click(function(){
				var dataId = $(this).attr("data-id");
				$('#fId').val(dataId);
			});
			});

///////////////////DELETING FILE
var modalConfirms = function(callback){
  
  $(".archive_file").on("click", function(){
    $("#files-modal").modal('show');
  });

  $("#modal-btn-sif").on("click", function(){
    callback(true);
    $("#files-modal").modal('hide');
  });
  
  $("#modal-btn-nof").on("click", function(){
    callback(false);
    $("#files-modal").modal('hide');
  });
};

modalConfirms(function(confirm){
  if(confirm){
    //Acciones si el usuario confirma
	var fId = $(this).attr("id");
	$.ajax({  
		url:"tab/delete_file.php",  
		method:"POST",  
		data:$('#del_file').serialize(),  
		beforeSend:function(){  
		 $('#modal-btn-sif').val("Deleting");  
		},  
		success:function(data){  
			//window.location.reload(); 
			console.log(fId);
		 $('#files-modal').modal('hide');  
		// $('#announce_table').html(data);  
		}  
	   });  
  }else{
    //Acciones si el usuario no confirma
    // $("#result").html("NO CONFIRMADO");
  }
});

////////////////DELETING FILE ENDS HERE
	</script>

 

  <!-- <script src="tab/jquery-2.1.1.min.js"></script> -->
  <!-- <script src="tab/bootstrap.min.js"></script> -->
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	  <!-- Bootstrap core JavaScript -->
	 <!-- <script src="../vendor/jquery/jquery.min.js"></script> -->
	 <!-- <script src="../js/jquery-3.1.1.min.js"></script> -->
  <script src="tab/bootstrap.min.js"></script>


