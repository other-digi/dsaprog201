
<?php

include('../includes/conn.php');

//$uid = $_SESSION['uid'];
$tid= $_GET['id'];
?>
<style>
.grAd3s{
	width:50px;
}
</style>
<div id="results" ></div>
<div class="tab-row"  id="tab-row-wrapper">
      <div  class="tab-col" id="tab-wrapper">
        
        <hr/>
        <div class="tab-col-3"> <!-- required for floating -->
          <!-- Nav tabs -->
          <ul class="nav nav-tabs tabs-left">

		  <?php 
            $get_sect = "SELECT * FROM section WHERE teacher_id=".$tid."";
            $get_sect_query = mysqli_query($conn,$get_sect);
            $count =1;
			if(mysqli_num_rows($get_sect_query)>0){
            while($sect_list = mysqli_fetch_array($get_sect_query)){

              if($count == 1){
                ?>
                
                <li  ><a    href="#<?php echo $sect_list['sect_id'];?>" data-toggle="tab" class="active"><?php echo "SECTION ".$sect_list['section_name'];?></a></li>
                
                <?php
				$count++;
              }else{


              
            
            ?>
           
            <li ><a    href="#<?php echo $sect_list['sect_id'];?>" data-toggle="tab"><?php echo "SECTION ".$sect_list['section_name'];?></a></li>
            <?php
              }
					$count++;
				}
			}else{
				?>
				
				<li> <b>No Class Assigned to you</b> </li>
				<?php
			}
            ?>
            
		  </ul>
		  
        </div>

        <div class="tab-col-9" id="col9-wrapper">
          <!-- Tab panes -->
          <div class="tab-content">
            
			
			<?php 
            $get_sect2 = "SELECT * FROM section WHERE teacher_id=".$tid." ";
            $get_sect2_query = mysqli_query($conn,$get_sect2);
            $counts =1;
            while($sect_list2 = mysqli_fetch_array($get_sect2_query)){

              if($counts == 1){
                ?>
                
				<div class="tab-pane active" id="<?php echo $sect_list2['sect_id'];?>" data-id="<?php echo $sect_list2['sect_id'];?>"><?php echo "SECTION ".$sect_list2['section_name'];?> Tab.
					<ul class="nav nav-tabs tabs-left">
					
						<table>
							<tr>
								<th>SURNAME</th>
								<th>FIRSTNAME</th>
								<th>M.I.</th>
								<th>M1E1</th>
								<th>M1E2</th>
								<th>M1E3</th>
								<th>M1 ASSESSMENT</th>
								<th>M1 GRADE</th>
								<th>M2E1</th>
								<th>M2E2</th>
								<th>M2E3</th>
								<th>M2 ASSESSMENT</th>
								<th>M2 GRADE</th>
								<th>M3E1</th>
								<th>M3E2</th>
								<th>M3E3</th>
								<th>M3 ASSESSMENT</th>
								<th>M3 GRADE</th>
								<th>TOTAL FOR MODULAR</th>
								<th>PERCENTAGE</th>
							</tr>
						
						<?php
						
						$get_users ="SELECT * FROM users WHERE section=".$sect_list2['section_name']." && type=2 ";
						$get_users_query= mysqli_query($conn,$get_users);
						
						while($users_lists = mysqli_fetch_array($get_users_query)){
							// $words = explode(" ", $users_lists['mname']);
							// $acronym = "";
							// foreach ($words as $w) {
							// $acronym .= $w[0].". ";
							// }
							$acronym = substr($users_lists['mname'],0,1);
							echo "<tr>";
							echo "<td>".strtoupper($users_lists['sname'])."</td>
							<td> ".strtoupper($users_lists['fname'])."</td>
							<td> ".$acronym.".</td>";
							
							
							///getting the students scores
							$get_score ="SELECT * FROM assessments WHERE sid='".$users_lists['id']."' && tid='$tid' ";
							$get_score_query = mysqli_query($conn,$get_score) or die("Error: ".$conn);
							
							if(mysqli_num_rows($get_score_query)>0){
								
								while($score = mysqli_fetch_array($get_score_query)){
									
									echo 
									"<td><input class='grAd3s' id='m1e1' data-id=".$users_lists['id']." type='number' value=".$score['m1e1']." usec=".$users_lists['section']." required></td>
									<td><input class='grAd3s' id='m1e2' data-id=".$users_lists['id']." type='number' value=".$score['m1e2']." usec=".$users_lists['section']." required></td>
									<td><input class='grAd3s' id='m1e3' data-id=".$users_lists['id']." type='number' value=".$score['m1e3']." usec=".$users_lists['section']." required></td>
									<td><input class='grAd3s' id='m1assessment' data-id=".$users_lists['id']." type='number' value=".$score['m1assessment']." usec=".$users_lists['section']." required></td>";
									$m1e1= $score['m1e1'] *0.03;
									$m1e2= $score['m1e2'] *0.03;
									$m1e3= $score['m1e3'] *0.03;
									$m1assessment= $score['m1assessment'] *0.11;
									$m1grade = ($m1e1+ $m1e2 + $m1e3 + $m1assessment);
									echo "<td>".$m1grade."</td>";
									
									echo "<td><input class='grAd3s' id='m2e1' data-id=".$users_lists['id']." type='number' value=".$score['m2e1']." usec=".$users_lists['section']." required></td>
									<td><input class='grAd3s' id='m2e2' data-id=".$users_lists['id']." type='number' value=".$score['m2e2']." usec=".$users_lists['section']." required></td>
									<td><input class='grAd3s' id='m2e3' data-id=".$users_lists['id']." type='number' value=".$score['m2e3']." usec=".$users_lists['section']." required></td>
									<td><input class='grAd3s' id='m2assessment' data-id=".$users_lists['id']." type='number' value=".$score['m2assessment']." usec=".$users_lists['section']." required></td>";
									$m2e1= $score['m2e1'] *0.03;
									$m2e2= $score['m2e2'] *0.03;
									$m2e3= $score['m2e3'] *0.03;
									$m2assessment= $score['m2assessment'] *0.11;
									$m2grade = ($m2e1+ $m2e2 + $m2e3 + $m2assessment);
									echo "<td>".$m2grade."</td>";
									
									echo "<td><input class='grAd3s' id='m3e1' data-id=".$users_lists['id']." type='number' value=".$score['m3e1']." usec=".$users_lists['section']." required></td>
									<td><input class='grAd3s' id='m3e2' data-id=".$users_lists['id']." type='number' value=".$score['m3e2']." usec=".$users_lists['section']." required></td>
									<td><input class='grAd3s' id='m3e3' data-id=".$users_lists['id']." type='number' value=".$score['m3e3']." usec=".$users_lists['section']." required></td>
									<td><input class='grAd3s' id='m3assessment' data-id=".$users_lists['id']." type='number' value=".$score['m3assessment']." usec=".$users_lists['section']." required></td>";
									$m3e1= $score['m3e1'] *0.03;
									$m3e2= $score['m3e2'] *0.03;
									$m3e3= $score['m3e3'] *0.03;
									$m3assessment= $score['m3assessment'] *0.11;
									$m3grade = ($m3e1+ $m3e2 + $m3e3 + $m3assessment);
									echo "<td>".$m3grade."</td>";
									$totalMod = $m1grade + $m2grade + $m3grade;
									echo"<td>".$totalMod."</td>";
									$gradePercent = ($totalMod/60)*100; 
									echo"<td>".$gradePercent."</td>";
									
									echo "</tr>";
								}
							
								
							
						}else{
							 echo 
									"<td><input class='grAd3s' id='m1e1' data-id=".$users_lists['id']." type='number' value='0' data-uid=".$tid." usec=".$users_lists['section']." required></td>
									<td><input class='grAd3s' id='m1e2' data-id=".$users_lists['id']." type='number' value='0' data-uid=".$tid." usec=".$users_lists['section']." required></td>
									<td><input class='grAd3s' id='m1e3' data-id=".$users_lists['id']." type='number' value='0' data-uid=".$tid." usec=".$users_lists['section']." required></td>
									<td><input class='grAd3s' id='m1assessment' data-id=".$users_lists['id']." type='number' value='0' data-uid=".$tid." usec=".$users_lists['section']." required></td>";
									
									echo "<td>0</td>";
									
									echo "<td><input class='grAd3s' id='m2e1' data-id=".$users_lists['id']." type='number' value='0' data-uid=".$tid." usec=".$users_lists['section']." required></td>
									<td><input class='grAd3s' id='m2e2' data-id=".$users_lists['id']." type='number' value='0' data-uid=".$tid." usec=".$users_lists['section']." required></td>
									<td><input class='grAd3s' id='m2e3' data-id=".$users_lists['id']." type='number' value='0' data-uid=".$tid." usec=".$users_lists['section']." required></td>
									<td><input class='grAd3s' id='m2assessment' data-id=".$users_lists['id']." type='number' value='0' data-uid=".$tid."usec=".$users_lists['section']." required></td>";
									
									echo "<td>0</td>";
									
									echo "<td><input class='grAd3s' id='m3e1' data-id=".$users_lists['id']." type='number' value='0' data-uid=".$tid." usec=".$users_lists['section']." required></td>
									<td><input class='grAd3s' id='m3e2' data-id=".$users_lists['id']." type='number' value='0' data-uid=".$tid." usec=".$users_lists['section']." required></td>
									<td><input class='grAd3s' id='m3e3' data-id=".$users_lists['id']." type='number' value='0' data-uid=".$tid." usec=".$users_lists['section']." required></td>
									<td><input class='grAd3s' id='m3assessment' data-id=".$users_lists['id']." type='number' value='0' data-uid=".$tid." usec=".$users_lists['section']." required></td>";
									
									echo "<td>0</td>";
									
									echo"<td>0</td>";
									
									echo"<td>0</td>";
									
									echo "</tr>";
						}
						
						}					
					?>
						</table>
					</ul>
				</div>
                
                <?php
				$counts++;
              }else{


              
            
            ?>
           
		   <div class="tab-pane " id="<?php echo $sect_list2['sect_id'];?>" data-id="<?php echo $sect_list2['sect_id'];?>"><?php echo "SECTION ".$sect_list2['section_name'];?> Tab.
		   
		   			<ul class="nav nav-tabs tabs-left">
					
					<table>
							<tr>
								<th>SURNAME</th>
								<th>FIRSTNAME</th>
								<th>M.I.</th>
								<th>M1E1</th>
								<th>M1E2</th>
								<th>M1E3</th>
								<th>M1 ASSESSMENT</th>
								<th>M1 GRADE</th>
								<th>M2E1</th>
								<th>M2E2</th>
								<th>M2E3</th>
								<th>M2 ASSESSMENT</th>
								<th>M2 GRADE</th>
								<th>M3E1</th>
								<th>M3E2</th>
								<th>M3E3</th>
								<th>M3 ASSESSMENT</th>
								<th>M3 GRADE</th>
								<th>TOTAL FOR MODULAR</th>
								<th>PERCENTAGE</th>
							</tr>
						<?php
						
						$get_users2 ="SELECT * FROM users WHERE section=".$sect_list2['section_name']." && type=2 ";
						$get_users2_query= mysqli_query($conn,$get_users2);

						while($user_lists2 = mysqli_fetch_array($get_users2_query)){
							$words = explode(" ", $user_lists2['mname']);
							$acronym = "";
							foreach ($words as $w) {
							$acronym .= $w[0].". ";
							}
							echo "<tr>";
							echo "<td>".strtoupper($user_lists2['sname'])."</td><td> ".strtoupper($user_lists2['fname'])."</td>
							<td> ".strtoupper($acronym)."</td>";
							
							
							//getting the students scores
							$get_score2 ="SELECT * FROM assessments WHERE sid='".$user_lists2['id']."' && tid='$tid' ";
							$get_score2_query =mysqli_query($conn,$get_score2);
							
							if(mysqli_num_rows($get_score2_query)>0){
								while($score2 = mysqli_fetch_array($get_score2_query)){
									
									echo 
									"<td><input class='grAd3s' id='m1e1' data-id=".$user_lists2['id']." type='number' value=".$score2['m1e1']." usec=".$user_lists2['section']." required></td>
									<td><input class='grAd3s' id='m1e2' data-id=".$user_lists2['id']." type='number' value=".$score2['m1e2']." usec=".$user_lists2['section']." required></td>
									<td><input class='grAd3s' id='m1e3' data-id=".$user_lists2['id']." type='number' value=".$score2['m1e3']." usec=".$user_lists2['section']." required></td>
									<td><input class='grAd3s' id='m1assessment' data-id=".$user_lists2['id']." type='number' value=".$score2['m1assessment']."  usec=".$user_lists2['section']." required></td>";
									$m1e1= $score2['m1e1'] *0.03;
									$m1e2= $score2['m1e2'] *0.03;
									$m1e3= $score2['m1e3'] *0.03;
									$m1assessment= $score2['m1assessment'] *0.11;
									$m1grade = ($m1e1+ $m1e2 + $m1e3 + $m1assessment);
									echo "<td>".$m1grade."</td>";
									
									echo "<td><input class='grAd3s' id='m2e1' data-id=".$user_lists2['id']." type='number' value=".$score2['m2e1']." usec=".$user_lists2['section']." required></td>
									<td><input class='grAd3s' id='m2e2' data-id=".$user_lists2['id']." type='number' value=".$score2['m2e2']." usec=".$user_lists2['section']." required></td>
									<td><input class='grAd3s' id='m2e3' data-id=".$user_lists2['id']." type='number' value=".$score2['m2e3']." usec=".$user_lists2['section']." required></td>
									<td><input class='grAd3s' id='m2assessment' data-id=".$user_lists2['id']." type='number' value=".$score2['m2assessment']."  usec=".$user_lists2['section']." required></td>";
									$m2e1= $score2['m2e1'] *0.03;
									$m2e2= $score2['m2e2'] *0.03;
									$m2e3= $score2['m2e3'] *0.03;
									$m2assessment= $score2['m2assessment'] *0.11;
									$m2grade = ($m2e1+ $m2e2 + $m2e3 + $m2assessment);
									echo "<td>".$m2grade."</td>";
									
									echo "<td><input class='grAd3s' id='m3e1' data-id=".$user_lists2['id']." type='number' value=".$score2['m3e1']." usec=".$user_lists2['section']." required></td>
									<td><input class='grAd3s' id='m3e2' data-id=".$user_lists2['id']." type='number' value=".$score2['m3e2']." usec=".$user_lists2['section']." required></td>
									<td><input class='grAd3s' id='m3e3' data-id=".$user_lists2['id']." type='number' value=".$score2['m3e3']." usec=".$user_lists2['section']." required></td>
									<td><input class='grAd3s' id='m3assessment' data-id=".$user_lists2['id']." type='number' value=".$score2['m3assessment']."   usec=".$user_lists2['section']." required></td>";
									$m3e1= $score2['m3e1'] *0.03;
									$m3e2= $score2['m3e2'] *0.03;
									$m3e3= $score2['m3e3'] *0.03;
									$m3assessment= $score2['m3assessment'] *0.11;
									$m3grade = ($m3e1+ $m3e2 + $m3e3 + $m3assessment);
									echo "<td>".$m3grade."</td>";
									$totalMod = $m1grade + $m2grade + $m3grade;
									echo"<td>".$totalMod."</td>";
									$gradePercent = ($totalMod/60)*100; 
									echo"<td>".$gradePercent."</td>";
									
									echo "</tr>";
								}
							
						}else{
							 echo 
									"<td><input class='grAd3s' id='m1e1' data-id=".$user_lists2['id']." type='number' value='0' data-uid=".$tid." usec=".$user_lists2['section']." required></td>
									<td><input class='grAd3s' id='m1e2' data-id=".$user_lists2['id']." type='number' value='0' data-uid=".$tid." usec=".$user_lists2['section']." required></td>
									<td><input class='grAd3s' id='m1e3' data-id=".$user_lists2['id']." type='number' value='0' data-uid=".$tid." usec=".$user_lists2['section']." required></td>
									<td><input class='grAd3s' id='m1assessment' data-id=".$user_lists2['id']." type='number' value='0' data-uid=".$tid." usec=".$user_lists2['section']." required></td>";
									
									echo "<td>0</td>";
									
									echo "<td><input class='grAd3s' id='m2e1' data-id=".$user_lists2['id']." type='number' value='0' data-uid=".$tid." usec=".$user_lists2['section']."  required></td>
									<td><input class='grAd3s' id='m2e2' data-id=".$user_lists2['id']." type='number' value='0' data-uid=".$tid." usec=".$user_lists2['section']." required></td>
									<td><input class='grAd3s' id='m2e3' data-id=".$user_lists2['id']." type='number' value='0' data-uid=".$tid." usec=".$user_lists2['section']." required></td>
									<td><input class='grAd3s' id='m2assessment' data-id=".$user_lists2['id']." type='number' value='0' data-uid=".$tid." usec=".$user_lists2['section']." required></td>";
									
									echo "<td>0</td>";
									
									echo "<td><input class='grAd3s' id='m3e1' data-id=".$user_lists2['id']." type='number' value='0' data-uid=".$tid." usec=".$user_lists2['section']." required></td>
									<td><input class='grAd3s' id='m3e2' data-id=".$user_lists2['id']." type='number' value='0' data-uid=".$tid." usec=".$user_lists2['section']." required></td>
									<td><input class='grAd3s' id='m3e3' data-id=".$user_lists2['id']." type='number' value='0' data-uid=".$tid." usec=".$user_lists2['section']." required></td>
									<td><input class='grAd3s' id='m3assessment' data-id=".$user_lists2['id']." type='number' value='0' data-uid=".$tid." usec=".$user_lists2['section']." required></td>";
									
									echo "<td>0</td>";
									
									echo"<td>0</td>";
									
									echo"<td>0</td>";
									
									echo "</tr>";
						}
			}				
						?>
						</table>
					</ul>
		
			</div>
            <?php
              }
            $count++;
            }
            ?>
          </div>
        </div>

        <div class="clearfix"></div>

      </div>

      
    </div>




 



  <script>
	//////////fade out////
// $(document).ready(function(){
 
   // $("#results").fadeOut(7500);
// });
/////////////fadout ends here
	/////////CHANGING the value
	
$(document).on('change', '.grAd3s', function(){
	//   //$('#dataModal').modal();
			var gvalue = $(this).val(); //value of input box
			var gid = $(this).data('id'); //exam and module located
			var dataId = $(this).attr("data-id"); /////student id
			var uSec = $(this).attr("usec"); /////section section
			var tid = $(this).attr("data-uid"); /////user id
			var mId = $(this).attr("id"); /////user id
	  $.ajax({
	   url:"updateGrade.php",
	   method:"POST",
	   contentType: "application/x-www-form-urlencoded; charset=UTF-8",
	   data:{gvalue: gvalue,
					gid:gid,
					dataId:dataId,
					uSec:uSec,
					tid:tid,
					mId,mId},
	   success:function(data){
		$('#results').html(data);
		console.log(mId);
		setTimeout(
  function() 
  {
    //do something special
	window.location.reload(true);
  }, 3500);

		 //console.log('Done: ', data);
	   }
	  });
	 });
	///////Changing the vaule ends
	

	// $(document).on('click', '.delete_module', function(){
	//   //$('#dataModal').modal();
	//   var module_id = $(this).attr("id");
	//   $.ajax({
	//    url:"delete_module.php",
	//    method:"POST",
	//    data:{module_id:module_id},
	//    success:function(data){
	// 	$('#module_detail').html(data);
	// 	$('#deleteModal').modal('hide');
	//    }
	//   });
	//  });

/////ADD MODULE
	 $(document).ready(function(){
	 $('#insert_form').on("submit", function(event){  
	  event.preventDefault();  
	  if($('#title').val() == "")  
	  {  
	   alert("Title is required");  
	  }  
	  else if($('#description').val() == '')  
	  {  
	   alert("Description is required");  
	  }  
	  else  
	  {  
	   $.ajax({  
		url:"tab/add_module.php",  
		method:"POST",  
		data:$('#insert_form').serialize(),  
		beforeSend:function(){  
		 $('#insert').val("Inserting");  
		},  
		success:function(data){  
			window.location.reload(); 
		 $('#insert_form')[0].reset();  
		 $('#add_data_Modal').modal('hide');  
		// $('#announce_table').html(data);  
		}  
	   });  
	  }  
	 });
	});  
//// ADD MODULE ENDS HERE


////DELETE MODULE /////////////
$(document).ready(function(){
	 $('#delete_module').on("submit", function(event){  
	  event.preventDefault();  
	  if($('#module_id').val() == "")  
	  {  
	   alert("Select one Item to delete");  
	  } 
	  else  
	  {  
	//    $.ajax({  
	// 	url:"tab/delete_module.php",  
	// 	method:"POST",  
	// 	data:$('#delete_module').serialize(),  
	// 	beforeSend:function(){  
	// 	 $('#delete').val("Deleting");  
	// 	},  
	// 	success:function(data){  
	// 		window.location.reload(); 
	// 	 $('#delete_module')[0].reset();  
	// 	 $('#deleteModal').modal('hide');  
	// 	// $('#announce_table').html(data);  
	// 	}  
	//    });  
	  }  
	 });
	});  
////DELETE MODULE ENDS HERE /////////////


////////////
var modalConfirm = function(callback){
  
  $("#deleteM").on("click", function(){
    $("#mi-modal").modal('show');
  });

  $("#modal-btn-si").on("click", function(){
    callback(true);
    $("#mi-modal").modal('hide');
  });
  
  $("#modal-btn-no").on("click", function(){
    callback(false);
    $("#mi-modal").modal('hide');
  });
};

modalConfirm(function(confirm){
  if(confirm){
    //Acciones si el usuario confirma
	$.ajax({  
		url:"tab/delete_module.php",  
		method:"POST",  
		data:$('#delete_module').serialize(),  
		beforeSend:function(){  
		 $('#delete').val("Deleting");  
		},  
		success:function(data){  
			window.location.reload(); 
		 $('#delete_module')[0].reset();  
		 $('#deleteModal').modal('hide');  
		// $('#announce_table').html(data);  
		}  
	   });  
  }else{
    //Acciones si el usuario no confirma
    // $("#result").html("NO CONFIRMADO");
  }
});

////////////


/////ADD File
$(document).ready(function(){
            $('#btn_upload').click(function(){
				var ftitle = $('#ftitle').val();
				var fdescription = $('#fdescription').val();
				var mid = $('#mid').val();
                var fd = new FormData();
                var files = $('#file')[0].files[0];
                fd.append('file',files);
				fd.append('ftitle',ftitle);
				fd.append('fdescription',fdescription);
				fd.append('mid',mid);
				var dataId = $(this).attr("data-id");
                // AJAX request
                $.ajax({
                    url: 'tab/add_file.php',
                    type: 'post',
                    data: fd,
                    contentType: false,
                    processData: false,
                    success: function(response){
                        if(response != 0){
							
                            // Show image preview
							$('#preview').addClass("alert alert-success");
                            $('#preview').append("File Uploaded Successfully");
							 $('#uploadform')[0].reset();  
							 $('#preview').addClass("");
							 response = 1;
							 $('#uploadModal').modal('hide'); 
							 $('.modal-backdrop').remove(); 
							
                        }else{
                            $('#preview').addClass("alert alert-danger");
                            $('#preview').append("File Not Uploaded Successfully");
							 $('#uploadform')[0].reset();  
							 $('#preview').addClass("");
							 response = 0;
                        }
                    }
                });
            });
        });
//// ADD File ENDS HERE





			$(document).ready(function(){
			$("a").click(function(){
				var dataId = $(this).attr("data-id");
				$('#mid').val(dataId);
			});
			});
		
	</script>

 

  <!-- <script src="tab/jquery-2.1.1.min.js"></script> -->
  <!-- <script src="tab/bootstrap.min.js"></script> -->
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	  <!-- Bootstrap core JavaScript -->
	 <!-- <script src="../vendor/jquery/jquery.min.js"></script> -->
	 <!-- <script src="../js/jquery-3.1.1.min.js"></script> -->
  <script src="tab/bootstrap.min.js"></script>


