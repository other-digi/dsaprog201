<?php  
session_start();
include('../includes/conn.php');
//index.php
if(!isset($_SESSION['error'])){
	$_SESSION['error'] = "";
}
if(!isset($_SESSION['success'])){
	$_SESSION['success'] = "";
}
$query = "SELECT * FROM announcements ORDER BY id DESC";
$result = mysqli_query($conn, $query);
 ?>  
<!DOCTYPE html>
<html lang="en">

<?php
include('includes/head.php');?>

	<body>
		<!-- Header bar -->
	  <?php include('header.php');?>
	  <!-- end of  Header bar -->
	  <div class="d-flex" id="wrapper">
		
			<!-- Sidebar -->
			<?php include('menu.php');?>
			<!-- /#sidebar-wrapper -->

			<!-- Page Content -->
			<div id="page-content-wrapper">
				<div class="container" >  
				   <h2>Announcements</h2>
				
				<br />
				<table class="table table-fluid" id="announcements" >
					<thead>
						<tr>
							<th>#</th>
							<th>Title</th>
							<th>Created by</th>
							<th>Post Created</th>
							<th>Action</th>  
						</tr>
					</thead>
					
					<tbody>
						  <?php
						  $count = 1;
						  while($row = mysqli_fetch_array($result))
						  {
						  ?>
						  
							<tr>
								<td><?php echo $count;?></td>
								<td><?php echo $row['title'];?></td>
								<td><?php echo $row['post_creator'];?></td>
								<td><?php echo $row['post_created'];?></td>
							    <td>
									
									<button type="submit" data-id="<?php echo $row["id"]; ?>" id="<?php echo $row["id"]; ?>" class=" btn-info btn-xs view_data"  title="View">View</button>
								 
								</td>
							</tr>
						  <?php
						  $count++;
						  }
					  ?>
					  </tbody>
					  
					  <tfoot>
					 
							<tr>
							  <th>#</th>
							  <th>Title</th>
							  <th>Created by</th>
							  <th>Post Created</th>
							  <th>Action</th>
							</tr>
					  
					  </tfoot>
				</table>
					
					
				</div> 
			</div>
			<!-- /#page-content-wrapper -->
			  <div class="bg-light border-right" id="sidebar-wrapper">
				<?php include_once('../includes/bot.php');?>
			  </div>
	  </div>
	  <!-- /#wrapper -->
		<div id="add_data_Modal" class="modal fade">
	 <div class="modal-dialog">
	  <div class="modal-content">
	   <div class="modal-header">
		<h4 class="modal-title">Announcement</h4>
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		
	
		
	   </div>
	   <div class="modal-body">
		<form method="post" id="insert_form">
		 <label>Title</label>
		 <input type="text" name="title" id="title" class="form-control" />
		 <br />
		 <label>Content</label>
		 <textarea name="content" id="content" class="form-control"></textarea>
		 <br />
		 <input type="submit" name="insert" id="insert" value="Insert" class="btn btn-success" />

		</form>
	   </div>
	   <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	   </div>
	  </div>
	 </div>
	</div>

	<div id="dataModal" class="modal fade">
	 <div class="modal-dialog">
	  <div class="modal-content">
	   <div class="modal-header">
	   <h4 class="modal-title">Announcement</h4>
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		
	   </div>
	   <div class="modal-body" id="announce_details">
		
	   </div>
	   <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	   </div>
	  </div>
	 </div>
	</div>
	
	<!-----DELETE MODAL------------->

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal">
  <div class="modal-dialog modal-l">
    <div class="modal-content">
      <div class="modal-header">
	  <h4 class="modal-title" id="myModalLabel">Are you Sure to Delete?</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       
      </div>
	  <div class="modal-body">
		<form method="post"id="yess_nos" action="delete_announce.php">
		<input type="hidden" id="id" name="id">
		<button type="submit" class="btn btn-warning" data-id=""id="modal-btn-si">Yes</button>
        <button type="button" class="btn btn-primary" id="modal-btn-no">No</button>
		</form>
	  </div>
      <div class="modal-footer">
       
      </div>
    </div>
  </div>
</div>

<!-------DELETE MODAL ENDS HERE------------->

	<script>
		$(document).ready( function () {
		$('#announcements').DataTable();
	} );

	$(document).on('click', '.view_data', function(){
	  //$('#dataModal').modal();
	  var announce_id = $(this).attr("id");
	  $.ajax({
	   url:"view_ann.php",
	   method:"POST",
	   data:{announce_id:announce_id},
	   success:function(data){
		$('#announce_details').html(data);
		$('#dataModal').modal('show');
	   }
	  });
	 });

	 $(document).ready(function(){
	 $('#insert_form').on("submit", function(event){  
	  event.preventDefault();  
	  if($('#title').val() == "")  
	  {  
	   alert("Title is required");  
	  }  
	  else if($('#content').val() == '')  
	  {  
	   alert("Content is required");  
	  }  
	  else  
	  {  
	   $.ajax({  
		url:"add_message.php",  
		method:"POST",  
		data:$('#insert_form').serialize(),  
		beforeSend:function(){  
		 $('#insert').val("Inserting");  
		},  
		success:function(data){  
			window.location.reload(); 
		 $('#insert_form')[0].reset();  
		 $('#add_data_Modal').modal('hide');  
		 $('#announce_table').html(data);  
		}  
	   });  
	  }  
	 });
	});  
	
	
	
	
	
	//////////////Confirmation MODAL
var modalConfirm = function(callback){
  
  $(".archive_data").on("click", function(){
    $("#mi-modal").modal('show');
  });

  $("#modal-btn-si").on("click", function(){
    callback(true);
    $("#mi-modal").modal('hide');
  });
  
  $("#modal-btn-no").on("click", function(){
    callback(false);
    $("#mi-modal").modal('hide');
  });
};

modalConfirm(function(confirm){
  if(confirm){
    //Acciones si el usuario confirma
	 var teacher_id = $(this).attr("id");
	 console.log(teacher_id);
	//$.ajax({  
		//url:"delete_teacher.php",  
		//method:"POST",  
		//data:$('#yess_nos').serialize(),    
		//success:function(data){  
			//window.location.reload(); 
		 //$('#delete_module')[0].reset();  
		 //$('#deleteModal').modal('hide');  
		//$('#teacherlist').html(data);  
		//}  
	 //  });  
  }else{
    //Acciones si el usuario no confirma
    // $("#result").html("NO CONFIRMADO");
  }
});
//////////Confirmation Modal ends here

$(document).ready(function(){
			$(".archive_data").click(function(){
				var dataId = $(this).attr("id");
				$('#id').val(dataId);
			});
			});
//////////fade out////
$(document).ready(function(){
 
   $("#mgss").fadeOut(5000);
});
/////////////fadout ends here
	</script>

	<!-- <script src="../vendor/jquery/jquery.min.js"></script> -->
	  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	  <!-- Bootstrap core JavaScript -->
	 <!-- <script src="../vendor/jquery/jquery.min.js"></script> -->
	  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	 
	  <script src="../js/active_page.js"></script>
	 
	 

	</body>

</html>

