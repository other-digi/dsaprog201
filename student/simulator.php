<!DOCTYPE html>
<html lang="en">

<?php
SESSION_START();
include('includes/head.php');


?>
<body>
	<!-- Header bar -->
  <?php include('header.php');?>
  <!-- end of  Header bar -->
  <div class="d-flex" id="wrapper">
	
    <!-- Sidebar -->
    <?php include('menu.php');?>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

        
	<style>
	#simulator-container{
		width:100%;
		min-height:500px;
		
		margin:0 auto;
	}
	#left-simu, #right-simu{
		width:50%;
		height:100%;
		overflow:auto;
		
		float:left;
		padding:5px;
	}
	
	#left-simu #code_panel{
		width:100%;
		height:100%;
		overflow:auto;
		border:1px solid #efefef;
		float:left;
		padding:5px;
		text-align:left;
		resize:none;
		outline:none;
	}
	#right-simu #code_panel{
		width:100%;
		height:100%;
		overflow:auto;
		border:1px solid #efefef;
		float:left;
		padding:5px;
		text-align:left;
		resize:none;
		outline:none;
	}
	#btnCompile{
		float:left;
	}
	</style>
   
    <div class="container-fluid">
        <form method="POST">
			<div id="simulator-container">
				<?php
				////include('../cpp_simulator/compiler.php');
				?>
				
				<?php
				
				if(isset($_GET['method'])){
					
					
				?>
				<h4><a href="?id=<?php echo $_GET['id']; ?>">NOT Loading Try this</a></h4>
				<iframe  id="comp_box2" width="100%" height="500" src="http://www.cpp.sh/"></iframe>
				<?php }else{ ?>
				<h4><a href="?id=<?php echo $_GET['id']; ?>&method=2">NOT Loading Try this</a></h4>
					<iframe  id="comp_box"width="100%" height="500" src="https://www.programiz.com/cpp-programming/online-compiler/"></iframe>
				<?php } ?>
			</div>
		</form>
      </div>

    
    


      
    </div>
    <!-- /#page-content-wrapper -->
	  <div class="bg-light border-right" id="sidebar-wrapper">
      <?php include_once('../includes/bot.php');?>
      
    </div>
  </div>
  <!-- /#wrapper -->
	
  <!-- Bootstrap core JavaScript -->
  <script src="../vendor/jquery/jquery.min.js"></script>
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <script src="../js/active_page.js"></script>

</body>

</html>
