<!DOCTYPE>
<html>
<?php require '../../includes/conn.php';
session_start();

?>
<head>
<title>Exam</title>
<style>
body {
    background: url("bg.jpg");
	background-size:100%;
	background-repeat: no-repeat;
	position: relative;
	background-attachment: fixed;
}
/* button */
.button {
  display: inline-block;
  border-radius: 4px;
  background-color: #f4511e;
  border: none;
  color: #FFFFFF;
  text-align: center;
  font-size: 28px;
  padding: 20px;
  width: 500px;
  transition: all 0.5s;
  cursor: pointer;
  margin: 5px;
}

.button span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.button span:after {
  content: '\00bb';
  position: absolute;
  opacity: 0;
  top: 0;
  right: -20px;
  transition: 0.5s;
}

.button:hover span {
  padding-right: 25px;
}

.button:hover span:after {
  opacity: 1;
  right: 0;
}
.title{
	background-color: #ccc11e;
	font-size: 28px;
  padding: 20px;
	
}
.button3 {
    border: none;
    color: white;
    padding: 10px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    -webkit-transition-duration: 0.4s; /* Safari */
    transition-duration: 0.4s;
    cursor: pointer;
}
.button3 {
    background-color: white; 
    color: black; 
    border: 2px solid #f4e542;
}

.button3:hover {
    background-color: #f4e542;
    color: Black;
}



#sbutton:disabled,
#sbutton[disabled]{
  border: 1px solid #999999;
  background-color: #cccccc;
  color: #666666;
}
</style>
</head>
<body><center>
<div class="title">Student Exam</div>
<?php 		
if(!isset($_GET['eid'])){
		echo '<meta http-equiv="refresh" content="0; url=../assessments.php?id="'.$_GET['id'].'&sec='.$_GET['sec'].'>';
}
///check if user taken the exam
$eid = $_GET['eid'];	
$idu = $_GET['id'];
$moduleId = $_GET['mid'];
$examc = $_GET['examc'];
$examname = $_GET['exname'];
$usec = $_GET['sec'];
$check_user_exist = "SELECT * FROM tbl_history WHERE uid='$idu' && section='$usec' && eid LIKE '%$eid%' ";
$check_user_exist_que= mysqli_query($conn,$check_user_exist) or die('Error Checking if user exist');

if(mysqli_num_rows($check_user_exist_que)>0){
	//get the number of question in the exam
	$countnoQues ="SELECT COUNT(eid) FROM tbl_exam_que WHERE eid LIKE '%$eid%'";
				$countnoQueqs = mysqli_query($conn,$countnoQues);
				$rrs = mysqli_fetch_array($countnoQueqs);
					//echo "<b> No of Qustions: ".$rrs['COUNT(eid)']."</b>";
					$ctrQuestions = $rrs['COUNT(eid)'];
						
				
	if (isset($_POST['click']) || isset($_GET['start'])) {
		$c = $_GET['ctr'];
		if(isset($_POST['userans'])) { 
		$userselected = $_POST['userans'];
																
		$fetchqry2 = "UPDATE `tbl_exam_que` SET `userans`='$userselected' WHERE `ctr`=".$_GET['ctr']." && `eid` LIKE '%$eid%'"; 
		$result2 = mysqli_query($conn,$fetchqry2) or die("ERROR: ".mysqli_error($conn));
		$ctr_ = $_GET['ctr']+1;
		 header("Location:index.php?id=".$_GET['id']."&n=".$ctrQuestions."&ctr=".$ctr_."&eid=".$_GET['eid']."&sec=".$_GET['sec']."&mid=".$_GET['mid']."&mname=".$_GET['mname']."&exname=".$_GET['exname']."&examc=".$_GET['examc']."&start=");
		}
		 
		
																	
 	} else {
		$_SESSION['clicks'] = 0;
	}
	///////////////////if exam not finished
	if(isset($_POST['finishexm'])){
		$examCrea = $_GET['examc'];
		$Mname = $_GET['mname'];
		$ExName = $_GET['exname'];
		
		////counting the correct answers
			$esCORE = 0;
			$qry3 = "SELECT `answer`, `userans` FROM `tbl_exam_que` WHERE eid LIKE '%$eid%'";
			$result3 = mysqli_query($conn,$qry3);
			while ($row3 = mysqli_fetch_array($result3, MYSQLI_ASSOC)) {
							 if($row3['answer']==$row3['userans']){
								@$_SESSION['score'] += 1;
								$esCORE += 1;
								 
							 }
							} 
		
		$fScore = $esCORE;
		echo "You finished your exam";
		
		echo "Your score is: ". $fScore;
		echo "Total number of correct answer ".$fScore." out of ".$ctrQuestions;
		$exam_Grade = ($fScore / $ctrQuestions)*100 ;
		echo "<br> YOur Grade is: ". $exam_Grade;
		
							$veri_if_exist = "SELECT * FROM assessments WHERE sid='".$idu."' AND section=".$usec;
							$veri_fi_exist_que = mysqli_query($conn,$veri_if_exist) or die("Error: ".$conn);
							
							if(mysqli_num_rows($veri_fi_exist_que)>0){
								
								if($Mname == "module1" && $ExName == "enrich1"){
									$q = "UPDATE assessments SET m1e1='$exam_Grade' WHERE sid='$idu'";
									mysqli_query($conn,$q);
								}else if($Mname == "module1" && $ExName == "enrich2"){
									$q = "UPDATE assessments SET m1e2='$exam_Grade' WHERE sid='$idu'";
									mysqli_query($conn,$q);
								}else if($Mname == "module1" && $ExName == "enrich3"){
									$q = "UPDATE assessments SET m1e3='$exam_Grade' WHERE sid='$idu'";
									mysqli_query($conn,$q);
								}else if($Mname == "module1" && $ExName == "module1"){
									$q = "UPDATE assessments SET m1assessment='$exam_Grade' WHERE sid='$idu'";
									mysqli_query($conn,$q);
									
									/////module 2
								}else if($Mname == "module2" && $ExName == "enrich1"){
									$q = "UPDATE assessments SET m2e1='$exam_Grade' WHERE sid='$idu'";
									mysqli_query($conn,$q);
								}else if($Mname == "module2" && $ExName == "enrich2"){
									$q = "UPDATE assessments SET m2e2='$exam_Grade' WHERE sid='$idu'";
									mysqli_query($conn,$q);
								}else if($Mname == "module2" && $ExName == "enrich3"){
									$q = "UPDATE assessments SET m2e3='$exam_Grade' WHERE sid='$idu'";
									mysqli_query($conn,$q);
								}else if($Mname == "module2" && $ExName == "module2"){
									$q = "UPDATE assessments SET m2assessment='$exam_Grade' WHERE sid='$idu'";
									mysqli_query($conn,$q);
									
									//////////module 3
								}else if($Mname == "module3" && $ExName == "enrich1"){
									$q = "UPDATE assessments SET m3e1='$exam_Grade' WHERE sid='$idu'";
									mysqli_query($conn,$q);
								}else if($Mname == "module3" && $ExName == "enrich2"){
									$q = "UPDATE assessments SET m3e2='$exam_Grade' WHERE sid='$idu'";
									mysqli_query($conn,$q);
								}else if($Mname == "module3" && $ExName == "enrich3"){
									$q = "UPDATE assessments SET m3e3='$exam_Grade' WHERE sid='$idu'";
									mysqli_query($conn,$q);
								}else if($Mname == "module3" && $ExName == "module3"){
									$q = "UPDATE assessments SET m3assessment='$exam_Grade' WHERE sid='$idu'";
									mysqli_query($conn,$q);
								}else{
									echo"<h1> No Corresponding Module found</h1>";
								}
								
								
							}else{
								if($Mname == "module1" && $ExName == "enrich1"){
									$q = "INSERT INTO assessments SET m1e1='$exam_Grade', sid='$idu', tid='$examCrea',section='$usec' ";
									mysqli_query($conn,$q);
								}else if($Mname == "module1" && $ExName == "enrich2"){
									$q = "INSERT INTO assessments SET m1e2='$exam_Grade', sid='$idu', tid='$examCrea',section='$usec' ";
									mysqli_query($conn,$q);
								}else if($Mname == "module1" && $ExName == "enrich3"){
									$q = "INSERT INTO assessments SET m1e3='$exam_Grade', sid='$idu', tid='$examCrea',section='$usec' ";
									mysqli_query($conn,$q);
								}else if($Mname == "module1" && $ExName == "module1"){
									$q = "INSERT INTO assessments SET m1assessments='$exam_Grade', sid='$idu', tid='$examCrea',section='$usec'  ";
									mysqli_query($conn,$q);
									
									/////module 2
								}else if($Mname == "module2" && $ExName == "enrich1"){
									$q = "INSERT INTO assessments SET m2e1='$exam_Grade', sid='$idu', tid='$examCrea',section='$usec'  ";
									mysqli_query($conn,$q);
								}else if($Mname == "module2" && $ExName == "enrich2"){
									$q = "INSERT INTO assessments SET m2e2='$exam_Grade', sid='$idu', tid='$examCrea',section='$usec'  ";
									mysqli_query($conn,$q);
								}else if($Mname == "module2" && $ExName == "enrich3"){
									$q = "INSERT INTO assessments SET m2e3='$exam_Grade', sid='$idu', tid='$examCrea',section='$usec'  ";
									mysqli_query($conn,$q);
								}else if($Mname == "module2" && $ExName == "module2"){
									$q = "INSERT INTO assessments SET m2assessments='$exam_Grade', sid='$idu', tid='$examCrea',section='$usec'  ";
									mysqli_query($conn,$q);
									
									//////////module 3
								}else if($Mname == "module3" && $ExName == "enrich1"){
									$q = "INSERT INTO assessments SET m3e1='$exam_Grade', sid='$idu', tid='$examCrea',section='$usec'  ";
									mysqli_query($conn,$q);
								}else if($Mname == "module3" && $ExName == "enrich2"){
									$q = "INSERT INTO assessments SET m3e2='$exam_Grade', sid='$idu', tid='$examCrea',section='$usec'  ";
									mysqli_query($conn,$q);
								}else if($Mname == "module3" && $ExName == "enrich3"){
									$q = "INSERT INTO assessments SET m3e3='$exam_Grade', sid='$idu', tid='$examCrea',section='$usec'  ";
									mysqli_query($conn,$q);
								}else if($Mname == "module3" && $ExName == "module3"){
									$q = "INSERT INTO assessments SET m3assessments='$exam_Grade' , sid='$idu', tid='$examCrea',section='$usec' ";
									mysqli_query($conn,$q);
								}else{
									echo"<h1> No Corresponding Module found</h1>";
								}
							}
		
						///update the histoty table
						$saveTohistory = "UPDATE tbl_history SET tid='$examCrea', score='$fScore', grade='$exam_Grade' WHERE uid='$idu' && eid LIKE '%$eid%'";
						mysqli_query($conn,$saveTohistory);
						
						header('Location:index.php?id='.$_GET["id"].'&n='.$ctrQuestions.'&ctr='.$_GET["ctr"].'&eid='.$_GET["eid"].'&sec='.$_GET["sec"].'&mid='.$_GET["mid"].'&mname='.$_GET["mname"].'&exname='.$_GET["exname"].'&examc='.$_GET["examc"].'&endquiz=');
						
						
		
	}
	if(isset($_GET['endquiz'])){
		$Escore = @$_SESSION['score'];
		$noQue = $_GET['n'];
		$FgradE = ($Escore / $noQue) * 100;
		echo "<h5>Score: ".$Escore."</h5>";
		echo "<h5>Grade: ".$FgradE."</h5>";
		session_unset();
						 
		echo '<a href="../assessment.php?id='.$_GET['id'].'&sec='.$_GET['sec'].'"><button class="button3" name="click" type="button"> Back To assessments page</button></a>';
		
	}
?>
	<div class="bump">
	<br>
		<form method="POST"action="index.php?id=<?php echo $_GET['id'];?>&n=<?php echo $ctrQuestions;?>&ctr=1&eid=<?php echo $_GET['eid']?>&sec=<?php echo $_GET['sec']?>&mid=<?php echo $_GET['mid'];?>&mname=<?php echo $_GET['mname'];?>&exname=<?php echo $_GET['exname'];?>&examc=<?php echo $_GET['examc'];?>&start=">
			<?php if(!isset($_GET['ctr'])){ ?> 
				<button class="button" name="start" ><span>START QUIZ</span></button>			
			<?php 
			
			} ?>
		</form>
	</div>
	
	<?php 
		if(isset($_POST['click']) || isset($_GET['start'])){
			?>
			<center>
	<div id="countdowns"> TIME Remaining: <h4 id="countdown"> </h4></div>
		<form action="" method="post"> 
			<table>
				<?php 


				
				if(isset($_GET['ctr']) && $_GET['n'] >= $_GET['ctr'] ) {  
				       
				
						

				////////////
				
				$fetchqry = "SELECT * FROM `tbl_exam_que` where eid LIKE '%$eid%' && ctr =".$_GET['ctr']; 
				$result=mysqli_query($conn,$fetchqry) or die("Error: ".mysqli_error($conn));
				$num=mysqli_num_rows($result);
				$rows = mysqli_fetch_array($result,MYSQLI_ASSOC)
				
			?>
			
			<tr>
				<td>
				<h5>
						<br><?php echo $_GET['ctr']."/".$_GET['n']?>
					</h5>
					<h3>
						<br><?php echo @$rows['question'];?>
					</h3>
				</td>
			</tr> 
			
			
			
			<tr>
				<td>
					<input required type="radio" name="userans" id="chcsA" onclick="enable()" value="a">A. &nbsp;<?php echo $rows['option1']; ?>
				</td>
			</tr>
			
			<tr>
				<td>
					<input required type="radio" name="userans" id="chcsB" onclick="enable()" value="b">B. &nbsp;<?php echo $rows['option2'];?>
				</td>
			</tr>
			
			<tr>
				<td>
					<input required type="radio" name="userans" id="chcsC" onclick="enable()" value="c">C. &nbsp;<?php echo $rows['option3']; ?>
				</td>
			</tr>
			
			
			<?php if($_GET['n'] != $_GET['ctr']){ 
			
						
 
			?>
			
			<tr>
				<?php 
				if($_GET['ctr'] == 1){
				?>
				<td>&nbsp;</td>
				<?php }else{ ?>
				<td>
					<a href="index.php?id=<?php echo $_GET['id'];?>&n=<?php echo $ctrQuestions;?>&ctr=<?php echo $_GET['ctr']-1;?>&eid=<?php echo $_GET['eid']?>&sec=<?php echo $_GET['sec']?>&mid=<?php echo $_GET['mid'];?>&mname=<?php echo $_GET['mname'];?>&exname=<?php echo $_GET['exname'];?>&examc=<?php echo $_GET['examc'];?>" class="button3">Prev</a>
				</td>
				<?php }
				?>
				
				
				<td>
					<button class="button3" id="sbutton"  type="submit" name="click" >NEXT</button>
				</td>
				
				
			</tr>
					
<?php           }else{
	
?>
				<tr>
					<td>
						<button class="button3" id="sbutton" name="finishexm" type="submit"> Finish</button>
					</td>
					<?php
					if($_GET['ctr'] == $_GET['n']){
						echo "<td>&nbsp;</td>";
					}else{ ?>
					<td>
						<button class="button3" id="time_btn"  type="submit" name="finishexm" >Submit</button>
					</td>	
					<?php } ?>
					
				</tr>
				
			 
					
					
<?php
						}

//////////////////////////
			//getting the users timestamp
						$timesta_que = mysqli_query($conn, "SELECT * FROM tbl_history WHERE uid='$idu' AND eid='$_GET[eid]' ") or die('Error197');
						if(mysqli_num_rows($timesta_que) > 0){
							$row_t = mysqli_fetch_array($timesta_que);
						    $time   = $row_t['timestamp'];
						           
        				}
        				
						$qq = mysqli_query($conn, "SELECT * FROM tbl_exam WHERE eid='$_GET[eid]' ") or die('Error197');
						$row = mysqli_fetch_array($qq);
							$ttime = $row['time'];
						

            ////geting the exam time
							
             $remaining = (($ttime * 60) - ((time() - $time)));
           // $remaining = (($ttime * 60));
							
			
			echo ' <script>
			///disable the next button
			document.getElementById("sbutton").disabled = true;
			
			
			//Enable next button if rabio button selected
			function enable(){
				document.getElementById("sbutton").disabled = false;
			}
			var seconds = ' . $remaining . ' ;
			function secondPassed() {
				var minutes = Math.round((seconds - 30)/60);
				var remainingSeconds = seconds % 60;
			
				if (remainingSeconds < 10) {
					remainingSeconds = "0" + remainingSeconds; 
				}
				
				document.getElementById(\'countdown\').innerHTML = minutes + ":" +    remainingSeconds;
	 
				if (seconds <= 0) {
					clearInterval(countdownTimer);
					document.getElementById("sbutton").disabled = true;
					document.getElementById(\'countdown\').innerHTML = "Buzz Buzz... \n Time is Up!";
					 document.getElementById("sbutton").style.visibility = "hidden";
					document.getElementById("sbutton").disabled = true;
					document.getElementById("chcsA").disabled = true;
					document.getElementById("chcsB").disabled = true;
					document.getElementById("chcsC").disabled = true;
					document.getElementById("time_btn").disabled = false;
					
					//  window.location ="index.php?id='.$_GET["id"].'&n='.$ctrQuestions.'&ctr='.$_GET["ctr"].'&eid='.$_GET["eid"].'&sec='.$_GET["sec"].'&mid='.$_GET["mid"].'&mname='.$_GET["mname"].'&exname='.$_GET["exname"].'&examc='.$_GET["examc"].'";
				} else {    
					seconds--;
				}
			}
				var countdownTimer = setInterval(\'secondPassed()\', 1000);	
			</script>';
				////////////////// TImer ENDS			
				}else{
					
					if(isset($_GET['n']) && isset($_GET['ctr'])){
						$finalScore = 0;
						$finalScores = 0;
						 if($_GET['ctr'] > $_GET['n'] ){
							$qry3 = "SELECT `answer`, `userans` FROM `tbl_exam_que` WHERE eid LIKE '%$eid%'";
							$result3 = mysqli_query($conn,$qry3);
							$storeArray = Array();
							while ($row3 = mysqli_fetch_array($result3, MYSQLI_ASSOC)) {
							 if($row3['answer']==$row3['userans']){
								 @$_SESSION['score'] += 1 ;
								 $finalScore +=1;
								 $finalScores +=1;
							 }
							} 
							
							$uid = $_GET['id'];
							$mid = $_GET['mid'];
							$exname = $_GET['exname'];
							$mname= $_GET['mname'];
							$excreator = $_GET['examc'];
							$examid = $_GET['eid'];
							$sec = $_GET['sec'];
							$excreator = $_GET['examc'];
							// $save_exam_result = "INSERT INTO tbl_exam_result VALUES(NULL,'$eid','$uid','$mid','$exname','$excreator',' $finalScore ',NOW()) ";
							// $save_exam_result_que = mysqli_query($conn,$save_exam_result);
							///saving the escore to db
							$veri_if_exist = "SELECT * FROM assessments WHERE sid='".$uid."' AND section=".$sec;
							$veri_fi_exist_que = mysqli_query($conn,$veri_if_exist) or die("Error: ".$conn);
							$numOfQue= $_GET['n'];
								$finalScore = ($finalScore / $numOfQue) *100;
							if(mysqli_num_rows($veri_fi_exist_que)>0){
								
								if($mname == "module1" && $exname == "enrich1"){
									$q = "UPDATE assessments SET m1e1='$finalScore' WHERE sid='$uid'";
									mysqli_query($conn,$q);
								}else if($mname == "module1" && $exname == "enrich2"){
									$q = "UPDATE assessments SET m1e2='$finalScore' WHERE sid='$uid'";
									mysqli_query($conn,$q);
								}else if($mname == "module1" && $exname == "enrich3"){
									$q = "UPDATE assessments SET m1e3='$finalScore' WHERE sid='$uid'";
									mysqli_query($conn,$q);
								}else if($mname == "module1" && $exname == "module1"){
									$q = "UPDATE assessments SET m1assessment='$finalScore' WHERE sid='$uid'";
									mysqli_query($conn,$q);
									
									/////module 2
								}else if($mname == "module2" && $exname == "enrich1"){
									$q = "UPDATE assessments SET m2e1='$finalScore' WHERE sid='$uid'";
									mysqli_query($conn,$q);
								}else if($mname == "module2" && $exname == "enrich2"){
									$q = "UPDATE assessments SET m2e2='$finalScore' WHERE sid='$uid'";
									mysqli_query($conn,$q);
								}else if($mname == "module2" && $exname == "enrich3"){
									$q = "UPDATE assessments SET m2e3='$finalScore' WHERE sid='$uid'";
									mysqli_query($conn,$q);
								}else if($mname == "module2" && $exname == "module2"){
									$q = "UPDATE assessments SET m2assessment='$finalScore' WHERE sid='$uid'";
									mysqli_query($conn,$q);
									
									//////////module 3
								}else if($mname == "module3" && $exname == "enrich1"){
									$q = "UPDATE assessments SET m3e1='$finalScore' WHERE sid='$uid'";
									mysqli_query($conn,$q);
								}else if($mname == "module3" && $exname == "enrich2"){
									$q = "UPDATE assessments SET m3e2='$finalScore' WHERE sid='$uid'";
									mysqli_query($conn,$q);
								}else if($mname == "module3" && $exname == "enrich3"){
									$q = "UPDATE assessments SET m3e3='$finalScore' WHERE sid='$uid'";
									mysqli_query($conn,$q);
								}else if($mname == "module3" && $exname == "module3"){
									$q = "UPDATE assessments SET m3assessment='$finalScore' WHERE sid='$uid'";
									mysqli_query($conn,$q);
								}else{
									echo"<h1> No Corresponding Module found</h1>";
								}
								
								
							}else{
								if($mname == "module1" && $exname == "enrich1"){
									$q = "INSERT INTO assessments SET m1e1='$finalScore', sid='$uid', tid='$excreator',section='$sec' ";
									mysqli_query($conn,$q);
								}else if($mname == "module1" && $exname == "enrich2"){
									$q = "INSERT INTO assessments SET m1e2='$finalScore', sid='$uid', tid='$excreator',section='$sec' ";
									mysqli_query($conn,$q);
								}else if($mname == "module1" && $exname == "enrich3"){
									$q = "INSERT INTO assessments SET m1e3='$finalScore', sid='$uid', tid='$excreator',section='$sec' ";
									mysqli_query($conn,$q);
								}else if($mname == "module1" && $exname == "module1"){
									$q = "INSERT INTO assessments SET m1assessments='$finalScore', sid='$uid', tid='$excreator',section='$sec'  ";
									mysqli_query($conn,$q);
									
									/////module 2
								}else if($mname == "module2" && $exname == "enrich1"){
									$q = "INSERT INTO assessments SET m2e1='$finalScore', sid='$uid', tid='$excreator',section='$sec'  ";
									mysqli_query($conn,$q);
								}else if($mname == "module2" && $exname == "enrich2"){
									$q = "INSERT INTO assessments SET m2e2='$finalScore', sid='$uid', tid='$excreator',section='$sec'  ";
									mysqli_query($conn,$q);
								}else if($mname == "module2" && $exname == "enrich3"){
									$q = "INSERT INTO assessments SET m2e3='$finalScore', sid='$uid', tid='$excreator',section='$sec'  ";
									mysqli_query($conn,$q);
								}else if($mname == "module2" && $exname == "module2"){
									$q = "INSERT INTO assessments SET m2assessments='$finalScore', sid='$uid', tid='$excreator',section='$sec'  ";
									mysqli_query($conn,$q);
									
									//////////module 3
								}else if($mname == "module3" && $exname == "enrich1"){
									$q = "INSERT INTO assessments SET m3e1='$finalScore', sid='$uid', tid='$excreator',section='$sec'  ";
									mysqli_query($conn,$q);
								}else if($mname == "module3" && $exname == "enrich2"){
									$q = "INSERT INTO assessments SET m3e2='$finalScore', sid='$uid', tid='$excreator',section='$sec'  ";
									mysqli_query($conn,$q);
								}else if($mname == "module3" && $exname == "enrich3"){
									$q = "INSERT INTO assessments SET m3e3='$finalScore', sid='$uid', tid='$excreator',section='$sec'  ";
									mysqli_query($conn,$q);
								}else if($mname == "module3" && $exname == "module3"){
									$q = "INSERT INTO assessments SET m3assessments='$finalScore' , sid='$uid', tid='$excreator',section='$sec' ";
									mysqli_query($conn,$q);
								}else{
									echo"<h1> No Corresponding Module found</h1>";
								}
							}
							
							
							$saveTohistory = "UPDATE tbl_history SET tid='$excreator', score='$finalScores', grade='$finalScore' WHERE uid='$uid' && eid LIKE '%$eid%'";
						mysqli_query($conn,$saveTohistory);
						
						header('Location:index.php?id='.$_GET["id"].'&n='.$ctrQuestions.'&ctr='.$_GET["ctr"].'&eid='.$_GET["eid"].'&sec='.$_GET["sec"].'&mid='.$_GET["mid"].'&mname='.$_GET["mname"].'&exname='.$_GET["exname"].'&examc='.$_GET["examc"].'&endquiz=');
						?>
						
						
						<?php
						 }
						}	
						
						
			
				?>
				
				
		</table>															 



						
		
<?php
 }


 ?>
			</table>
		</form>
	</center>
		<?php }
	
	?>
	
<?php

}else{
	echo "NO user added";
	$timeN = time();
	echo $timeN;
	 $insertH = "INSERT INTO tbl_history VALUES('','$idu','$usec', '', '$eid','','','$timeN', NOW())";
	
	if(mysqli_query($conn,$insertH) ){
		header("Location:index.php?id=".$_GET['id']."&n=".$rrs['COUNT(eid)']."&eid=".$_GET['eid']."&sec=".$_GET['sec']."&mid=".$_GET['mid']."&mname=".$_GET['mname']."&exname=".$_GET['exname']."&examc=".$_GET['examc']);
	}
	
	
}													
//echo($_SESSION['clicks']);
?>



</body>
</html>