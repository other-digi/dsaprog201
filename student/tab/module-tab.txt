
<?php

include('../includes/conn.php');

$uid = $_SESSION['uid'];
?>
<div class="tab-row"  id="tab-row-wrapper">
      <div  class="tab-col" id="tab-wrapper">
        
        <hr/>
        <div class="tab-col-3"> <!-- required for floating -->
          <!-- Nav tabs -->
          <ul class="nav nav-tabs tabs-left">

		  <?php 
            $get_menu = "SELECT * FROM modules WHERE module_creator=".$uid." ORDER BY name ASC";
            $get_menu_query = mysqli_query($conn,$get_menu);
            $count =1;
            while($menu = mysqli_fetch_array($get_menu_query)){

              if($count == 1){
                ?>
                
                <li ><a    href="#<?php echo $menu['aid'];?>" data-toggle="tab"><?php echo $menu['name'];?></a></li>
                
                <?php
				$count++;
              }else{


              
            
            ?>
           
            <li ><a   href="#<?php echo $menu['aid'];?>" data-toggle="tab"><?php echo $menu['name'];?></a></li>
            <?php
              }
            $count++;
            }
            ?>
            
		  </ul>
		  <button type="button" name="age" id="age" data-toggle="modal" data-target="#add_data_Modal" class="btn btn-success btn-sm rounded-0" title="Add"><i class="fa fa-table"></i></button>
          <button class="btn btn-danger btn-sm rounded-0 delete_module" data-target="#deleteModal" type="button" data-toggle="modal" data-placement="top" title="Delete"><i class="fa fa-trash"></i></button>
        </div>

        <div class="tab-col-9" id="col9-wrapper">
          <!-- Tab panes -->
          <div class="tab-content">
            
			
			<?php 
            $get_menus = "SELECT * FROM modules WHERE module_creator=".$uid." ORDER BY name ASC";
            $get_menus_query = mysqli_query($conn,$get_menus);
            $counts =1;
            while($menus = mysqli_fetch_array($get_menus_query)){

              if($counts == 1){
                ?>
                
				<div class="tab-pane active" id="<?php echo $menus['aid'];?>" data-id="<?php echo $menus['aid'];?>"><?php echo $menus['name'];?> Tab.
					<ul class="nav nav-tabs tabs-left">
						<li >
							<a href="<?php echo "?mid=".$menus['id'];?>" data-toggle="modal" id ="<?php echo $menus['id'];?>" data-id="<?php echo $menus['id'];?>" data-target="#uploadModal" title="Add file" class="btn btn-success btn-sm rounded-0 .add_file">Add File <i class="fa fa-plus"></i></a>
						</li>
						<?php
						
						$get_files ="SELECT * FROM files WHERE mid=".$menus['id']." && file_creator=".$uid." ORDER BY file_created ASC";
						$get_files_query= mysqli_query($conn,$get_files);

						while($file = mysqli_fetch_array($get_files_query)){
							echo "<li> <a href='../files/uploads/".$file['fname']."' target='_blank'>".$file['title']."</a></li>";
						}
						if (!$get_files_query) {
							printf("Error: %s\n", mysqli_error($conn));
							exit();
						}
						?>
					</ul>
				</div>
                
                <?php
				$counts++;
              }else{


              
            
            ?>
           
		   <div class="tab-pane " id="<?php echo $menus['aid'];?>" data-id="<?php echo $menus['aid'];?>"><?php echo $menus['name'];?> Tab.
		   			<ul class="nav nav-tabs tabs-left">
					   <li >
						   <a href="<?php echo "?mid=".$menus['id'];?>" data-toggle="modal" id ="<?php  echo $menus['id'];?>" data-id="<?php echo $menus['id'];?>" data-target="#uploadModal" title="Add file" class="btn btn-success btn-sm rounded-0 add_file">Add File <i class="fa fa-plus"></i></a>
						</li>
						<?php
						
						$get_file ="SELECT * FROM files WHERE mid=".$menus['id']." && file_creator=".$uid." ORDER BY file_created ASC";
						$get_file_query= mysqli_query($conn,$get_file);

						while($files = mysqli_fetch_array($get_file_query)){
							echo "<li> <a href='../files/uploads/".$files['fname']."' target='_blank'>".$files['title']."</a></li>";
						}
						if (!$get_file_query) {
							printf("Error: %s\n", mysqli_error($conn));
							exit();
						}
						?>
					</ul>
		
			</div>
            <?php
              }
            $count++;
            }
            ?>
          </div>
        </div>

        <div class="clearfix"></div>

      </div>

      
    </div>

 <!-----ADD MODAL----->
 <div id="add_data_Modal" class="modal fade">
	 <div class="modal-dialog">
	  <div class="modal-content">
	   <div class="modal-header">
     <h4 class="modal-title">Add Module</h4>
		<button type="button" class="close" data-dismiss="modal">&times;</button>

	
	   </div>
	   <div class="modal-body">
		<form method="post" id="insert_form">
		 <label>Title</label>
		 <input type="text" name="title" id="title" class="form-control" />
		 <br />
		 <label>Description</label>
		 <textarea name="description" id="description" class="form-control"></textarea>
		 <br />
		 <input type="submit" name="insert" id="insert" value="Insert" class="btn btn-success" />

		</form>
	   </div>
	   <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	   </div>
	  </div>
	 </div>
	</div>
<!-----END OF ADD MODAL------>


 <!-----DELETE MODAL----->
 <div id="deleteModal" class="modal fade">
	 <div class="modal-dialog">
	  <div class="modal-content">
	   <div class="modal-header">
     <h4 class="modal-title">Delete Module</h4>
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		
	   </div>
	   <div class="modal-body" id="module_detail">
			<form method="POST" id="delete_module">
				<select name="module_id">
				<?php
				$get_modules = "SELECT * FROM modules WHERE module_creator=$uid ORDER BY name ASC";
				$get_modules_query = mysqli_query($conn,$get_modules);
				$output = "";
				while($modules = mysqli_fetch_array($get_modules_query)){

					$output .= '<option value="'.$modules['id'].'">'.$modules['name'].'</option>';

				}
				echo $output;
				?>
				
				
				</select>
				<input type="submit" name="delete" id="deleteM" value="Delete" class="btn btn-warning" />
			</form>
	   </div>
	   <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	   </div>
	  </div>
	 </div>
	</div>
<!-----END OF DELETE MODAL------>

<!----- Confirmation MODAL------>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal">
  <div class="modal-dialog modal-l">
    <div class="modal-content">
      <div class="modal-header">
	  <h4 class="modal-title" id="myModalLabel">Are you Sure to Delete?</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" id="modal-btn-si">Yes</button>
        <button type="button" class="btn btn-primary" id="modal-btn-no">No</button>
      </div>
    </div>
  </div>
</div>
<!----- Confirmation MODAL ENDs HERE------>

 <!-----ADD File MODAL----->

 <div id="uploadModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
				<h4 class="modal-title">File upload form</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    
                </div>
                <div class="modal-body">
                    <!-- Form -->
                    <form method='post' action='' id="uploadform" enctype="multipart/form-data">
					<input type='hidden' name='mid' id='mid' class='form-control' ><br>
					Title : <input type='text' name='ftitle' id='ftitle' class='form-control' ><br>
					Description : <input type='text' name='fdescription' id='fdescription' class='form-control' ><br>
                        Select file : <input type='file' name='file' id='file' class='form-control' ><br>
                        <input type='button' class='btn btn-info' value='Upload' id='btn_upload'>
                    </form>

                    <!-- Preview-->
                    <div  role="alert" id='preview'></div>
                </div>
                
            </div>

          </div>
        </div>
<!-----END OF ADD File MODAL------>



  <script>


	// $(document).on('click', '.delete_module', function(){
	//   //$('#dataModal').modal();
	//   var module_id = $(this).attr("id");
	//   $.ajax({
	//    url:"delete_module.php",
	//    method:"POST",
	//    data:{module_id:module_id},
	//    success:function(data){
	// 	$('#module_detail').html(data);
	// 	$('#deleteModal').modal('hide');
	//    }
	//   });
	//  });

/////ADD MODULE
	 $(document).ready(function(){
	 $('#insert_form').on("submit", function(event){  
	  event.preventDefault();  
	  if($('#title').val() == "")  
	  {  
	   alert("Title is required");  
	  }  
	  else if($('#description').val() == '')  
	  {  
	   alert("Description is required");  
	  }  
	  else  
	  {  
	   $.ajax({  
		url:"tab/add_module.php",  
		method:"POST",  
		data:$('#insert_form').serialize(),  
		beforeSend:function(){  
		 $('#insert').val("Inserting");  
		},  
		success:function(data){  
			window.location.reload(); 
		 $('#insert_form')[0].reset();  
		 $('#add_data_Modal').modal('hide');  
		// $('#announce_table').html(data);  
		}  
	   });  
	  }  
	 });
	});  
//// ADD MODULE ENDS HERE


////DELETE MODULE /////////////
$(document).ready(function(){
	 $('#delete_module').on("submit", function(event){  
	  event.preventDefault();  
	  if($('#module_id').val() == "")  
	  {  
	   alert("Select one Item to delete");  
	  } 
	  else  
	  {  
	//    $.ajax({  
	// 	url:"tab/delete_module.php",  
	// 	method:"POST",  
	// 	data:$('#delete_module').serialize(),  
	// 	beforeSend:function(){  
	// 	 $('#delete').val("Deleting");  
	// 	},  
	// 	success:function(data){  
	// 		window.location.reload(); 
	// 	 $('#delete_module')[0].reset();  
	// 	 $('#deleteModal').modal('hide');  
	// 	// $('#announce_table').html(data);  
	// 	}  
	//    });  
	  }  
	 });
	});  
////DELETE MODULE ENDS HERE /////////////


////////////
var modalConfirm = function(callback){
  
  $("#deleteM").on("click", function(){
    $("#mi-modal").modal('show');
  });

  $("#modal-btn-si").on("click", function(){
    callback(true);
    $("#mi-modal").modal('hide');
  });
  
  $("#modal-btn-no").on("click", function(){
    callback(false);
    $("#mi-modal").modal('hide');
  });
};

modalConfirm(function(confirm){
  if(confirm){
    //Acciones si el usuario confirma
	$.ajax({  
		url:"tab/delete_module.php",  
		method:"POST",  
		data:$('#delete_module').serialize(),  
		beforeSend:function(){  
		 $('#delete').val("Deleting");  
		},  
		success:function(data){  
			window.location.reload(); 
		 $('#delete_module')[0].reset();  
		 $('#deleteModal').modal('hide');  
		// $('#announce_table').html(data);  
		}  
	   });  
  }else{
    //Acciones si el usuario no confirma
    // $("#result").html("NO CONFIRMADO");
  }
});

////////////


/////ADD File
$(document).ready(function(){
            $('#btn_upload').click(function(){
				var ftitle = $('#ftitle').val();
				var fdescription = $('#fdescription').val();
				var mid = $('#mid').val();
                var fd = new FormData();
                var files = $('#file')[0].files[0];
                fd.append('file',files);
				fd.append('ftitle',ftitle);
				fd.append('fdescription',fdescription);
				fd.append('mid',mid);
				var dataId = $(this).attr("data-id");
                // AJAX request
                $.ajax({
                    url: 'tab/add_file.php',
                    type: 'post',
                    data: fd,
                    contentType: false,
                    processData: false,
                    success: function(response){
                        if(response != 0){
							
                            // Show image preview
							$('#preview').addClass("alert alert-success");
                            $('#preview').append("File Uploaded Successfully");
							 $('#uploadform')[0].reset();  
							 $('#preview').addClass("");
							 response = 1;
							 $('#uploadModal').modal('hide'); 
							 $('.modal-backdrop').remove(); 
							
                        }else{
                            $('#preview').addClass("alert alert-danger");
                            $('#preview').append("File Not Uploaded Successfully");
							 $('#uploadform')[0].reset();  
							 $('#preview').addClass("");
							 response = 0;
                        }
                    }
                });
            });
        });
//// ADD File ENDS HERE





			$(document).ready(function(){
			$("a").click(function(){
				var dataId = $(this).attr("data-id");
				$('#mid').val(dataId);
			});
			});
		
	</script>

 

  <!-- <script src="tab/jquery-2.1.1.min.js"></script> -->
  <!-- <script src="tab/bootstrap.min.js"></script> -->
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	  <!-- Bootstrap core JavaScript -->
	 <!-- <script src="../vendor/jquery/jquery.min.js"></script> -->
	 <!-- <script src="../js/jquery-3.1.1.min.js"></script> -->
  <script src="tab/bootstrap.min.js"></script>


