
<?php

include('../includes/conn.php');
$section = $_GET['sec'];
// $uid = $_SESSION['uid'];
?>
<div class="tab-row"  id="tab-row-wrapper">
      <div  class="tab-col" id="tab-wrapper">
        
        <hr/>
        <div class="tab-col-3"> <!-- required for floating -->
          <!-- Nav tabs -->
          <ul class="nav nav-tabs tabs-left">

		  <?php 

			$get_sec_teacher = "SELECT * FROM section WHERE section_name LIKE '%$section%'";
			$get_sec_teacherq = mysqli_query($conn,$get_sec_teacher) or die("Error: ".mysqli_error($conn));
			
			if(mysqli_num_rows($get_sec_teacherq) > 0){
				$tsec = mysqli_fetch_array($get_sec_teacherq);
				$teacherID = $tsec['teacher_id'];
			
				$get_menu = "SELECT * FROM modules WHERE module_creator=".$teacherID." ORDER BY name ASC LIMIT 3";
				$get_menu_query = mysqli_query($conn,$get_menu);
				$count =1;
				while($menu = mysqli_fetch_array($get_menu_query)){

				  if($count == 1){
					?>
					
					<li ><a href="#<?php echo $menu['aid'];?>" data-toggle="tab"><?php echo $menu['name'];?></a></li>
					
					<?php
					$count++;
				  }else{

				?>
					<li ><a   href="#<?php echo $menu['aid'];?>" data-toggle="tab"><?php echo $menu['name'];?></a></li>
					<?php
				  }
				$count++;
				}
			}else{
				echo "No exam";
			}
            ?>
            
		  </ul>
		  
        </div>

        <div class="tab-col-9" id="col9-wrapper">
          <!-- Tab panes -->
          <div class="tab-content">
            
			
			<?php 
            $get_menus = "SELECT * FROM modules WHERE module_creator=".$teacherID." ORDER BY name ASC";
            $get_menus_query = mysqli_query($conn,$get_menus);
            $counts =1;
			if(mysqli_num_rows($get_menus_query) >0){
				
			
            while($menus = mysqli_fetch_array($get_menus_query)){

              if($counts == 1){
                ?>
                
				<div class="tab-pane active" id="<?php echo $menus['aid'];?>" data-id="<?php echo $menus['aid'];?>"><?php echo $menus['name'];?> Tab.
					<ul class="nav nav-tabs tabs-left">
						
						<?php
						
						$get_files ="SELECT * FROM tbl_exam WHERE exam_creator=".$menus['module_creator']." &&  status='enabled' ORDER BY exam_name ASC";
						$get_files_query= mysqli_query($conn,$get_files) or die("Error: ".mysqli_error($conn));
						if(mysqli_num_rows($get_files_query)>0){
					 while($file = mysqli_fetch_array($get_files_query)){
							
							/////enrichment 1
							if($file['exam_name'] =='enrich1' && $file['exam_creator'] == $menus['module_creator'] && $file['module_id'] ==$menus['id']){
								//check if already taken the exam
								$ver_hist ="SELECT * FROM tbl_history WHERE uid='$_GET[id]' && eid='$file[eid]'";
								$ver_hist_que = mysqli_query($conn, $ver_hist);
								if(mysqli_num_rows($ver_hist_que)>0){
									echo "<li> Enricment 1</li>";
								}else{
									echo "<li> <a href='exam/index.php?que=takeexam&mid=".$menus['id']."&mname=".$menus['aid']."&exname=enrich1&examc=".$menus['module_creator']."&eid=".$file['eid']."&id=".$_GET['id']."&sec=".$_GET['sec']."'>Enricment 1</a></li>";
								}
								
							}else{
								//echo "<li> Enricment 1</li>";
							}
							
							///Enrichemnt 2
							/////enrichment2
							if($file['exam_name'] =='enrich2' && $file['exam_creator'] == $menus['module_creator'] && $file['module_id'] ==$menus['id']){
									//check if already taken the exam
								$ver_hist ="SELECT * FROM tbl_history WHERE uid='$_GET[id]' && eid='$file[eid]'";
								$ver_hist_que = mysqli_query($conn, $ver_hist);
								if(mysqli_num_rows($ver_hist_que)>0){
									echo "<li> Enricment 2</li>";
								}else{
								echo "<li> <a href='exam/index.php?que=takeexam&mid=".$menus['id']."&mname=".$menus['aid']."&exname=enrich2&examc=".$menus['module_creator']."&eid=".$file['eid']."&id=".$_GET['id']."&sec=".$_GET['sec']."'>Enricment 2</a></li>";
							}
							}else{
								//echo "<li> Enricment 2</li>";
							}
							///Enrichemt 3
							/////enrichment3
							if($file['exam_name'] =='enrich3' && $file['exam_creator'] == $menus['module_creator'] && $file['module_id'] ==$menus['id']){
								//check if already taken the exam
								$ver_hist ="SELECT * FROM tbl_history WHERE uid='$_GET[id]' && eid='$file[eid]'";
								$ver_hist_que = mysqli_query($conn, $ver_hist);
								if(mysqli_num_rows($ver_hist_que)>0){
									echo "<li> Enricment 3</li>";
								}else{
								echo "<li> <a href='exam/index.php?que=takeexam&mid=".$menus['id']."&mname=".$menus['aid']."&exname=enrich3&examc=".$menus['module_creator']."&eid=".$file['eid']."&id=".$_GET['id']."&sec=".$_GET['sec']."'>Enricment 3</a></li>";
							}
							}else{
								//echo "<li> Enricment 3</li>";
							}
							
							//
							/////Module Assement
							if($file['exam_name'] ==$menus['aid'] && $file['exam_creator'] == $menus['module_creator'] && $file['module_id'] ==$menus['id']){
								//check if already taken the exam
								$ver_hist ="SELECT * FROM tbl_history WHERE uid='$_GET[id]' && eid='$file[eid]'";
								$ver_hist_que = mysqli_query($conn, $ver_hist);
								if(mysqli_num_rows($ver_hist_que)>0){
									echo "<li> Enricment 1</li>";
								}else{
								
								echo "<li> <a href='exam/index.php?que=takeexam&mid=".$menus['id']."&mname=".$menus['aid']."&exname=".$file['exam_name']."&examc=".$menus['module_creator']."&eid=".$file['eid']."&id=".$_GET['id']."&sec=".$_GET['sec']."'>".$menus['name']." Assessment</a></li>";
							}
							}else{
								//echo "<li>".$menus['name']." Assessment</li>";
							}
							
						}	
						
						}else{
							echo "<li>Enrichment 1</li>";
							echo "<li>Enrichment 2</li>";
							echo "<li>Enrichment 3</li>";
							echo "<li>".$menus['name']." Assessment</li>";
						}
						
						?>
					</ul>
				</div>
                
                <?php
				$counts++;
              }else{


              
            
            ?>
           
		   <div class="tab-pane " id="<?php echo $menus['aid'];?>" data-id="<?php echo $menus['aid'];?>"><?php echo $menus['name'];?> Tab.
		  
		   			<ul class="nav nav-tabs tabs-left">
					  
						<?php
						
						$get_file ="SELECT * FROM tbl_exam WHERE exam_creator=".$menus['module_creator']." && module_id=".$menus['id']." && status='enabled' ORDER BY exam_name ASC ";
						$get_file_query= mysqli_query($conn,$get_file) or die("Error: ".mysqli_error($conn));
						if(mysqli_num_rows($get_file_query)>0){
							
						
						while($files = mysqli_fetch_array($get_file_query)){
							
							/////enrichment 1
							if($files['exam_name'] =='enrich1' && $files['exam_creator'] == $menus['module_creator']){
								echo "<li> <a href='exam/index.php?que=takeexam&mid=".$menus['id']."&mname=".$menus['aid']."&exname=enrich1&examc=".$menus['module_creator']."&eid=".$files['eid']."&id=".$_GET['id']."&sec=".$_GET['sec']."'>Enricment 1</a></li>";
							}else{
								//echo "<li> Enricment 1</li>";
							}
							///Enrichemnt 2
							
							if($files['exam_name'] =='enrich2' && $files['exam_creator'] == $menus['module_creator']){
								echo "<li> <a href='exam/index.php?que=takeexam&mid=".$menus['id']."&mname=".$menus['aid']."&exname=enrich2&examc=".$menus['module_creator']."&eid=".$files['eid']."&id=".$_GET['id']."&sec=".$_GET['sec']."'>Enricment 2</a></li>";
							}else{
								//echo "<li> Enricment 2</li>";
							}
							///Enrichemt 3
							
							if($files['exam_name'] =='enrich3' && $files['exam_creator'] == $menus['module_creator']){
								echo "<li> <a href='exam/index.php?que=takeexam&mid=".$menus['id']."&mname=".$menus['aid']."&exname=enrich3&examc=".$menus['module_creator']."&eid=".$files['eid']."&id=".$_GET['id']."&sec=".$_GET['sec']."'>Enricment 3</a></li>";
							}else{
								//echo "<li> Enricment 3</li>";
							}
							
							//
							/////Module Assement
							if($files['exam_name'] == $menus['aid'] && $files['exam_creator'] == $menus['module_creator']){
								echo "<li> <a href='exam/index.php?que=takeexam&mid=".$menus['id']."&mname=".$menus['aid']."&exname=".$files['exam_name']."&examc=".$menus['module_creator']."&eid=".$files['eid']."&id=".$_GET['id']."&sec=".$_GET['sec']."'>".$menus['name']." Assessment</a></li>";
							}else{
								//echo "<li>".$menus['name']." Assessment</li>";
							}
						}
						
						}else{
							echo "<li>Enrichment 1</li>";
							echo "<li>Enrichment 2</li>";
							echo "<li>Enrichment 3</li>";
							echo "<li>".$menus['name']." Assessment</li>";
						}
						
						?>
						
						
					</ul>
			
			</div>
            <?php
              }
            $count++;
            }
		}else{
			echo "No exam Created or enabled";
		}
            ?>
			
          </div>
		  
        </div>

        <div class="clearfix"></div>

      </div>

      
    </div>





  <script>


	// $(document).on('click', '.delete_module', function(){
	//   //$('#dataModal').modal();
	//   var module_id = $(this).attr("id");
	//   $.ajax({
	//    url:"delete_module.php",
	//    method:"POST",
	//    data:{module_id:module_id},
	//    success:function(data){
	// 	$('#module_detail').html(data);
	// 	$('#deleteModal').modal('hide');
	//    }
	//   });
	//  });

/////ADD MODULE
	 $(document).ready(function(){
	 $('#insert_form').on("submit", function(event){  
	  event.preventDefault();  
	  if($('#title').val() == "")  
	  {  
	   alert("Title is required");  
	  }  
	  else if($('#description').val() == '')  
	  {  
	   alert("Description is required");  
	  }  
	  else  
	  {  
	   $.ajax({  
		url:"tab/add_module.php",  
		method:"POST",  
		data:$('#insert_form').serialize(),  
		beforeSend:function(){  
		 $('#insert').val("Inserting");  
		},  
		success:function(data){  
			window.location.reload(); 
		 $('#insert_form')[0].reset();  
		 $('#add_data_Modal').modal('hide');  
		// $('#announce_table').html(data);  
		}  
	   });  
	  }  
	 });
	});  
//// ADD MODULE ENDS HERE


////DELETE MODULE /////////////
$(document).ready(function(){
	 $('#delete_module').on("submit", function(event){  
	  event.preventDefault();  
	  if($('#module_id').val() == "")  
	  {  
	   alert("Select one Item to delete");  
	  } 
	  else  
	  {  
	//    $.ajax({  
	// 	url:"tab/delete_module.php",  
	// 	method:"POST",  
	// 	data:$('#delete_module').serialize(),  
	// 	beforeSend:function(){  
	// 	 $('#delete').val("Deleting");  
	// 	},  
	// 	success:function(data){  
	// 		window.location.reload(); 
	// 	 $('#delete_module')[0].reset();  
	// 	 $('#deleteModal').modal('hide');  
	// 	// $('#announce_table').html(data);  
	// 	}  
	//    });  
	  }  
	 });
	});  
////DELETE MODULE ENDS HERE /////////////


////////////
var modalConfirm = function(callback){
  
  $("#deleteM").on("click", function(){
    $("#mi-modal").modal('show');
  });

  $("#modal-btn-si").on("click", function(){
    callback(true);
    $("#mi-modal").modal('hide');
  });
  
  $("#modal-btn-no").on("click", function(){
    callback(false);
    $("#mi-modal").modal('hide');
  });
};

modalConfirm(function(confirm){
  if(confirm){
    //Acciones si el usuario confirma
	$.ajax({  
		url:"tab/delete_module.php",  
		method:"POST",  
		data:$('#delete_module').serialize(),  
		beforeSend:function(){  
		 $('#delete').val("Deleting");  
		},  
		success:function(data){  
			window.location.reload(); 
		 $('#delete_module')[0].reset();  
		 $('#deleteModal').modal('hide');  
		// $('#announce_table').html(data);  
		}  
	   });  
  }else{
    //Acciones si el usuario no confirma
    // $("#result").html("NO CONFIRMADO");
  }
});

////////////


/////ADD File
$(document).ready(function(){
            $('#btn_upload').click(function(){
				var ftitle = $('#ftitle').val();
				var fdescription = $('#fdescription').val();
				var mid = $('#mid').val();
                var fd = new FormData();
                var files = $('#file')[0].files[0];
                fd.append('file',files);
				fd.append('ftitle',ftitle);
				fd.append('fdescription',fdescription);
				fd.append('mid',mid);
				var dataId = $(this).attr("data-id");
                // AJAX request
                $.ajax({
                    url: 'tab/add_file.php',
                    type: 'post',
                    data: fd,
                    contentType: false,
                    processData: false,
                    success: function(response){
                        if(response != 0){
							
                            // Show image preview
							$('#preview').addClass("alert alert-success");
                            $('#preview').append("File Uploaded Successfully");
							 $('#uploadform')[0].reset();  
							 $('#preview').addClass("");
							 response = 1;
							 $('#uploadModal').modal('hide'); 
							 $('.modal-backdrop').remove(); 
							
                        }else{
                            $('#preview').addClass("alert alert-danger");
                            $('#preview').append("File Not Uploaded Successfully");
							 $('#uploadform')[0].reset();  
							 $('#preview').addClass("");
							 response = 0;
                        }
                    }
                });
            });
        });
//// ADD File ENDS HERE





			$(document).ready(function(){
			$("a").click(function(){
				var dataId = $(this).attr("data-id");
				$('#mid').val(dataId);
			});
			});
		
	</script>

 

  <!-- <script src="tab/jquery-2.1.1.min.js"></script> -->
  <!-- <script src="tab/bootstrap.min.js"></script> -->
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	  <!-- Bootstrap core JavaScript -->
	 <!-- <script src="../vendor/jquery/jquery.min.js"></script> -->
	 <!-- <script src="../js/jquery-3.1.1.min.js"></script> -->
  <script src="tab/bootstrap.min.js"></script>


