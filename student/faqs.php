<?php  
session_start();
include('../includes/conn.php');
//index.php
if(!isset($_SESSION['error'])){
	$_SESSION['error'] = "";
}
if(!isset($_SESSION['success'])){
	$_SESSION['success'] = "";
}
$query = "SELECT * FROM chatbot ";
$result = mysqli_query($conn, $query);
 ?>  
<!DOCTYPE html>
<html lang="en">

<?php
include('includes/head.php');?>

	<body>
		<!-- Header bar -->
	  <?php include('header.php');?>
	  <!-- end of  Header bar -->
	  <div class="d-flex" id="wrapper">
		
			<!-- Sidebar -->
			<?php include('menu.php');?>
			<!-- /#sidebar-wrapper -->

			<!-- Page Content -->
			<div id="page-content-wrapper">
				<div class="container" >  
				   <h2>Frequently Asked Questions (FAQs)</h2>
				
				<br />
				<table class="table table-fluid" id="faqs" >
					<thead>
						<tr>
							<th>#</th>
							<th>Question</th>
							<th>Answer</th>
							<th>Action</th>
							 
						</tr>
					</thead>
					
					<tbody>
						  <?php
						  $count = 1;
						  while($row = mysqli_fetch_array($result))
						  {
						  ?>
						  
							<tr>
								<td><?php echo $count;?></td>
								<td><?php echo $row['queries'];?></td>
								<td><?php echo $row['replies'];?></td>
								
							     <td>
									
									<button type="submit" data-id="<?php echo $row["id"]; ?>" id="<?php echo $row["id"]; ?>" class=" btn-info btn-xs view_data"  title="View">View</button>
								 
								</td>
							</tr>
						  <?php
						  $count++;
						  }
					  ?>
					  </tbody>
					  
					  <tfoot>
					 
							<tr>
							  <th>#</th>
							  <th>Question</th>
							  <th>Answer</th>
							  <th>Action</th>
							</tr>
					  
					  </tfoot>
				</table>
					
					
				</div> 
			</div>
			<!-- /#page-content-wrapper -->
			  <div class="bg-light border-right" id="sidebar-wrapper">
				<?php include_once('../includes/bot.php');?>
			  </div>
	  </div>
	  <!-- /#wrapper -->
	<div id="dataModal" class="modal fade">
	 <div class="modal-dialog">
	  <div class="modal-content">
	   <div class="modal-header">
	   <h4 class="modal-title">FAQs</h4>
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		
	   </div>
	   <div class="modal-body" id="faqs_details">
		
	   </div>
	   <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	   </div>
	  </div>
	 </div>
	</div>
	


	<script>
		$(document).ready( function () {
		$('#faqs').DataTable();
	} );

	$(document).on('click', '.view_data', function(){
	  //$('#dataModal').modal();
	  var announce_id = $(this).attr("id");
	  $.ajax({
	   url:"view_faqs.php",
	   method:"POST",
	   data:{announce_id:announce_id},
	   success:function(data){
		$('#faqs_details').html(data);
		$('#dataModal').modal('show');
	   }
	  });
	 });

	 $(document).ready(function(){
	 $('#insert_form').on("submit", function(event){  
	  event.preventDefault();  
	  if($('#title').val() == "")  
	  {  
	   alert("Title is required");  
	  }  
	  else if($('#content').val() == '')  
	  {  
	   alert("Content is required");  
	  }  
	  else  
	  {  
	   $.ajax({  
		url:"add_message.php",  
		method:"POST",  
		data:$('#insert_form').serialize(),  
		beforeSend:function(){  
		 $('#insert').val("Inserting");  
		},  
		success:function(data){  
			window.location.reload(); 
		 $('#insert_form')[0].reset();  
		 $('#add_data_Modal').modal('hide');  
		 $('#announce_table').html(data);  
		}  
	   });  
	  }  
	 });
	});  
	
	
	
	
	
	//////////////Confirmation MODAL
var modalConfirm = function(callback){
  
  $(".archive_data").on("click", function(){
    $("#mi-modal").modal('show');
  });

  $("#modal-btn-si").on("click", function(){
    callback(true);
    $("#mi-modal").modal('hide');
  });
  
  $("#modal-btn-no").on("click", function(){
    callback(false);
    $("#mi-modal").modal('hide');
  });
};

modalConfirm(function(confirm){
  if(confirm){
    //Acciones si el usuario confirma
	 var teacher_id = $(this).attr("id");
	 console.log(teacher_id);
	//$.ajax({  
		//url:"delete_teacher.php",  
		//method:"POST",  
		//data:$('#yess_nos').serialize(),    
		//success:function(data){  
			//window.location.reload(); 
		 //$('#delete_module')[0].reset();  
		 //$('#deleteModal').modal('hide');  
		//$('#teacherlist').html(data);  
		//}  
	 //  });  
  }else{
    //Acciones si el usuario no confirma
    // $("#result").html("NO CONFIRMADO");
  }
});
//////////Confirmation Modal ends here

$(document).ready(function(){
			$(".archive_data").click(function(){
				var dataId = $(this).attr("id");
				$('#id').val(dataId);
			});
			});
//////////fade out////
$(document).ready(function(){
 
   $("#mgss").fadeOut(5000);
});
/////////////fadout ends here
	</script>

	<!-- <script src="../vendor/jquery/jquery.min.js"></script> -->
	  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	  <!-- Bootstrap core JavaScript -->
	 <!-- <script src="../vendor/jquery/jquery.min.js"></script> -->
	  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	 
	  <script src="../js/active_page.js"></script>
	 
	 

	</body>

</html>

