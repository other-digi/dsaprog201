<div id="right-col-container">
	<div id="messages-container">
	
		<?php
		$no_message = false;
		
		if(isset($_GET['user'])){
			$_GET['user'] = $_GET['user'];
		}else{
			///if no user pass o the url 
			//use the last message sent
			$q = 'SELECT `sender_name`, `receiver_name` FROM `messages`
			WHERE `sender_name`="'.$tEmail.'"
			OR `receiver_name`="'.$tEmail.'"
			ORDER BY `date_time` DESC LIMIT 1
			';
			$r = mysqli_query($conn,$q);
			
			if($r){
				if(mysqli_num_rows($r)>0){
					while($row=mysqli_fetch_assoc($r)){
						$sender_name = $row['sender_name'];
						$reciever_name= $row['receiver_name'];
						///check who is the sender
						if($tEmail == $sender_name){
							$_GET['user']= $reciever_name;
							
						}else{
							$_GET['user']= $sender_name;
						}
					}
				}else{
					// this user have not yet sent any message
					$no_message=true;
					echo '<p style="padding:5px;">No New Message</p>';
				}
			}else{
				echo $q;
			}
		}
			
			
		if($no_message == false){
			
		
		$q = 'SELECT * FROM `messages` WHERE 
		`sender_name`="'.$tEmail.'" AND receiver_name="'.$_GET['user'].'"
		OR `sender_name`="'.$_GET['user'].'"
		AND `receiver_name`="'.$tEmail.'"';
		
		$r = mysqli_query($conn,$q);
		if($r){
			while($row=mysqli_fetch_assoc($r)){
				$sender_name = $row['sender_name'];
				$reciever_name = $row['receiver_name'];
				$message = $row['message_text'];
				
				///check who is the sender_name
				if($sender_name == $tEmail){
					//show the message with grey background
				?>
				<div class="grey-message">
				<!------Showing the message with grey background-->
					<a href="#">Me</a>
					<p><?php echo $message; ?></p>
				</div>
				<?php
				}else{
					//shoiw the message with white background
				?>
				
				<div class="white-message">
				<!------Showing the message with white background-->
					<a href="#"><?php echo $sender_name;?></a>
					<p><?php echo $message; ?></p>
				</div>	
				
				<?php
					
				}
			}
		}else{
			echo $q;
		}
	////end of no message
	}	
			?>
		
		
		
		
	</div>
	<form method="POST" id="message-form">
		<textarea class="textarea" id="message_text" placeholder="Write you message here"></textarea>
	</form>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<script>
$("document").ready(function(event){
	
	//if form is submitted
		$("#right-col-container").on('submit', '#message-form', function(){
			//get the data from textarea
			var message_text = $("#message_text").val();
			///send the data to database using sending_process.php
			$.post("../messaging/sending_process.php?user=<?php echo $_GET['user'];?>",
			{
				text:message_text,
				
			},
			///in return well get 
			function(data,status){
				//alert(data);
				///remove first the text in text area
				$("#message_text").val("");
				
				// Adding the data to message-container
				document.getElementById("message-container").innerHTML += data;
				//$('#messages-container').load(document.URL +  ' #messages-container');
				
				$(".grey-message").scrollTop($(".grey-message")[0].scrollHeight);
				$(".white-message").scrollTop($(".white-message")[0].scrollHeight);
			}
			);
			
			
		});
	//if any button is pressed
	//right-col-container
	$("#right-col-container").keypress(function(e){
		//submit the data using ENTER button
		if(e.keyCode == 13 && !e.shiftKey){
			// it means that ENTER was pressed without SHIFT key
			$("#message-form").submit();
			
		}
	});
	
});

</script>
