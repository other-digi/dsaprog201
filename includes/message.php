<?php
///connecting to the database
include_once('conn.php');

//getting the message from user thru AJAX
$getMsg = mysqli_real_escape_string($conn,$_POST['text']);

//checkinguser query to database query
$check_data = "SELECT replies FROM chatbot WHERE queries LIKE '%$getMsg%'";
$run_query = mysqli_query($conn,$check_data) or die("Error in Query");

//check if the user query match in chatbot reply
if(mysqli_num_rows($run_query)>0){
	//fetching matched query from database to reply
	$fetch_data = mysqli_fetch_assoc($run_query);
	//storing the reply to variable that use to send in AJAX
	$reply = $fetch_data['replies'];
	echo $reply;
}else{
	echo "Sorry I did not understand what are you saying";
}
?>