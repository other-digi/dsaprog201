<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width-device-width, initial-scale-1.0">
	<title>Chatbot</title>
	<link rel="stylesheet" href="../css/chatbot.css" >
	<script src="https://kit.fontawesome.com/a076d05399.js"></script>
	<!--<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>-->
</head>
<body>

	<div class="bot-wrapper">
		<div class="bot-title">
			<div class="bot"><img class="bot" src="../img/chatbot.png"></div>
			<div class="bot-text">
				DSA Chatbot <br>
				I am here to assist you with platform
			</div>
			
		</div>
		<div class="bot-form">
			<div class="bot-inbox inbox">
				<div class="bot-icon">
					<i class="fas fa-user"></i>
				</div>
				<div class="bot-msg-header">
					<p>Hi!, how can I help you?</p>
				</div>
				
			</div>	
			
			

						<div class="bot-inbox inbox">
							<div class="bot-icon">
								<i class="fas fa-user"></i>
							</div>
				
							<div class="bot-msg-header">
					<?php	if(isset($_GET['sec'])){
					?>
						<p><a href="faqs.php?id=<?php echo $_GET['id'];?>&sec=<?php echo $_GET['sec'];?>" style="color:#fff!important;">Click Here for Frequently Asked Questions(FAQs)</a></p>
					<?php } else{ ?>
					<p><a href="faqs.php?id=<?php echo $_GET['id'];?>" style="color:#fff!important;">Click Here for Frequently Asked Questions(FAQs)</a></p>
					
			
		
					<?php } 
 ?>
			</div>
			</div>				
		</div>			
			
		
		<div class="bot-typing-field">
			<div class="bot-input-data">
				<input id="bot-data" type="text" placeholder="Type something here..." required>
				<button id="bot-send-btn">Send</button>
			</div>
		</div>
	
	</div>

	<script>
		$(document).ready(function(){
			$("#bot-send-btn").on("click",function(){
				$value = $("#bot-data").val();
				$msg = '<div class="bot-user-inbox inbox"><div class="bot-msg-header"><p>' + $value + '</p></div></div>';
				$(".bot-form").append($msg);
				$("#bot-data").val('');
				
				//Start Ajax Code
				$.ajax({
					url:'../includes/message.php',
					type: 'POST',
					data: 'text='+$value,
					success: function(result){
						$reply = '<div class="bot-inbox inbox"><div class="bot-icon"><i class="fas fa-user"></i></div><div class="bot-msg-header"><p>' + result + '</p></div></div>	';
						$(".bot-form").append($reply);
						
						//auto scrollbar down if chat box is full
						$(".bot-form").scrollTop($(".bot-form")[0].scrollHeight);
					}
				});
			});
		});
	</script>
</body>
</html>