-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 14, 2020 at 06:24 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_dsa`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE `announcements` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `post_creator` varchar(50) NOT NULL,
  `post_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `announcements`
--

INSERT INTO `announcements` (`id`, `title`, `content`, `post_creator`, `post_created`) VALUES
(1, 'First Ann', 'This is a good first announcement', '1', '2020-09-24 21:17:53'),
(2, '2nd Anncoun', 'this is the 2nd', '2', '2020-09-24 21:53:57'),
(3, 'Sample_1', 'Sample one', '1', '2020-09-26 19:12:51'),
(4, 'Sample_2', 'Sample_2', '1', '2020-09-26 19:13:15'),
(5, 'Sample_3', 'Sample 3', '1', '2020-09-26 20:59:41'),
(6, 'Sample_4', 'Sample 4', '1', '2020-09-26 21:26:12'),
(7, 'Sample_5', 'sample 5', '1', '2020-09-26 21:38:26'),
(8, 'Sample_6', 'sample 6', '1', '2020-09-26 21:40:56'),
(9, 'Sample_7', 'sample 7', '1', '2020-09-26 21:41:04'),
(10, 'Sample_8', 'sample 8', '1', '2020-09-26 21:41:17'),
(11, 'sample 9', 'sdsd', '1', '2020-09-26 21:41:28'),
(12, 'food', 'dfdfdff', '1', '2020-09-26 22:30:27'),
(13, 'food', 'sdfsfdfdsfsdf', '1', '2020-09-26 22:30:34'),
(14, 'dfsdfdsf', 'dsgdsg', '1', '2020-09-26 22:31:02'),
(15, 'dfd', 'sdfsfsf', '1', '2020-09-26 22:33:00'),
(16, 'Food Panda', 'dfdfd', '1', '2020-09-26 22:34:30'),
(17, 'Sample_10', '10', '1', '2020-09-26 23:32:50'),
(18, 'Sample_11', 'Hello', '1', '2020-09-27 10:00:32'),
(19, 'Sample_12', 'Aloh@', '1', '2020-09-27 10:17:20'),
(20, 'Sample_13', 'Sample 13', '1', '2020-09-27 12:01:07'),
(21, 'Sample_14', 'Sample 14', '1', '2020-09-27 12:01:54'),
(22, 'Sample_15', 'ALoa', '1', '2020-09-27 12:04:14'),
(23, 'Sample_16', '16', '1', '2020-09-27 12:05:38'),
(24, 'Sample_17', '17', '1', '2020-09-27 12:12:47'),
(25, 'Sample_18', '18', '1', '2020-09-27 12:16:04'),
(26, 'Sample_19', '19', '1', '2020-09-27 12:16:20'),
(27, 'Sample_20', '20', '1', '2020-09-27 12:18:46'),
(28, 'Sample_21', 'What do you want?', '1', '2020-09-27 13:53:03'),
(29, 'Sample_22', '22', '1', '2020-09-27 13:55:09'),
(30, 'Sample_23', '23', '1', '2020-09-27 14:00:41'),
(31, 'Sample_24', 'hi 24', '1', '2020-09-27 18:27:14'),
(32, 'Sample_26', 'Aloha', '0', '2020-09-27 21:39:11'),
(33, 'Sample_25', '25', '0', '2020-09-27 21:39:42'),
(35, 'Sample_28', 'Asa', '0', '2020-09-27 21:44:01'),
(36, 'Sample_29', '29', '3', '2020-09-27 21:45:35'),
(37, 'Sample_302', '30', '2', '2020-09-27 21:47:27'),
(43, 'Welcome to my suvbject', 'Hi students', '$Marti Cordero', '2020-10-11 22:17:02');

-- --------------------------------------------------------

--
-- Table structure for table `assessments`
--

CREATE TABLE `assessments` (
  `id` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `tid` int(11) NOT NULL,
  `m1e1` int(11) NOT NULL,
  `m1e2` int(11) NOT NULL,
  `m1e3` int(11) NOT NULL,
  `m1assessment` int(11) NOT NULL,
  `m2e1` int(11) NOT NULL,
  `m2e2` int(11) NOT NULL,
  `m2e3` int(11) NOT NULL,
  `m2assessment` int(11) NOT NULL,
  `m3e1` int(11) NOT NULL,
  `m3e2` int(11) NOT NULL,
  `m3e3` int(11) NOT NULL,
  `m3assessment` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `assessments`
--

INSERT INTO `assessments` (`id`, `sid`, `tid`, `m1e1`, `m1e2`, `m1e3`, `m1assessment`, `m2e1`, `m2e2`, `m2e3`, `m2assessment`, `m3e1`, `m3e2`, `m3e3`, `m3assessment`) VALUES
(1, 20, 17, 90, 89, 88, 98, 100, 76, 23, 54, 11, 99, 100, 98),
(2, 11, 17, 90, 98, 90, 80, 99, 76, 76, 54, 11, 90, 100, 97);

-- --------------------------------------------------------

--
-- Table structure for table `chatbot`
--

CREATE TABLE `chatbot` (
  `id` int(11) NOT NULL,
  `queries` varchar(300) NOT NULL,
  `replies` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chatbot`
--

INSERT INTO `chatbot` (`id`, `queries`, `replies`) VALUES
(1, 'Hi|Hello|Hy|hey', 'Hello there'),
(2, 'What is your name|what is your name?', 'My name is DSA Chatbot'),
(3, 'where are you from|where are you from?', 'I am from this system'),
(4, 'bye|good bye|goodbye', 'Ok. Goodbye, Thank you for talking with me.');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(30) NOT NULL,
  `comment` varchar(300) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `subject` varchar(300) NOT NULL,
  `frM` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `comment`, `status`, `subject`, `frM`) VALUES
(5, 'hello', 1, 'Sample', ''),
(6, '2', 1, 'Sample 2', ''),
(7, '3', 1, 'Sample 3', ''),
(8, '4', 1, 'Sample 4', ''),
(9, 'hrtyutnruityujtuytutfutriktyiuftutrii5nit6urtujyti', 1, 'geryeryhreuhrerth', ''),
(10, 'dfsfsdf', 1, 'geryeryhreuhrerth', 'Jayson'),
(11, 'sdfsdf', 1, 'dfsdfs', 'Jayson'),
(12, 'efefe', 1, 'Sample', 'Jayson'),
(13, 'qq', 1, 'Sample', 'Jayson'),
(14, 'cxcx', 1, 'xc', 'Jayson'),
(15, 'sds', 1, 'sdsd', 'Jayson'),
(16, 'sd', 1, 'dss', 'sd');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `mid` int(11) NOT NULL,
  `fname` varchar(150) NOT NULL,
  `title` varchar(50) NOT NULL,
  `descrip` varchar(150) NOT NULL,
  `file_creator` int(11) NOT NULL,
  `file_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `mid`, `fname`, `title`, `descrip`, `file_creator`, `file_created`) VALUES
(1, 1, 'MC7800(A,AE),NCV7800.pdf', 'MC7800(A,AE),NCV7800.pdf', 'Sample Desc', 5, '2020-09-29 22:26:27'),
(2, 1, 'PUP_General_Clearance.pdf', 'PUP_General_Clearance.pdf', 'Sample Desc', 5, '2020-09-29 22:28:57'),
(3, 1, 'Marketing Updates Aug 12.pdf', 'Marketing Updates Aug 12.pdf', 'Sample Desc', 5, '2020-09-29 22:30:06'),
(4, 1, 'BMS_Standards_of_Business_Conduct_and_Ethics.pdf', 'BMS_Standards_of_Business_Conduct_and_Ethics.pdf', 'Sample Desc', 5, '2020-09-29 22:31:29'),
(5, 1, 'BMS.pdf', 'BMS.pdf', 'Sample Desc', 5, '2020-09-29 22:39:55'),
(6, 1, 'uphold-key.txt', 'uphold-key.txt', 'Sample Desc', 5, '2020-09-29 22:46:22'),
(7, 1, '20180.pdf', '20180.pdf', 'Sample Desc', 5, '2020-09-29 23:06:55'),
(8, 10, '201.pdf', '201.pdf', '', 5, '2020-09-30 23:14:21'),
(9, 10, '20.pdf', '20.pdf', 'err', 5, '2020-09-30 23:15:05'),
(10, 1, 'Napster', 'Napster', '789456123', 5, '2020-10-02 14:38:09'),
(11, 0, '120455689_3687034411315728_994996462044443797_n.jpg', 'Napster', 'jaysonolaguera@gmail.com', 5, '2020-10-02 15:16:57'),
(12, 1, '2011.pdf', 'Gnaparticles', 'jaysonolaguera@gmail.com', 5, '2020-10-02 15:20:34'),
(13, 1, '346566manila-bay-fr-pna-scaled.jpg', 'dsd', 'jaysonolaguera@gmail.com', 5, '2020-10-02 15:32:32'),
(14, 1, '442698Untitled-design-50-1-1200x578.png', 'Napster', 'teacher@gmail.com', 5, '2020-10-02 15:52:43'),
(15, 1, '7893882011.pdf', '', '', 5, '2020-10-02 16:13:49'),
(16, 1, '153504119931366_177664943985337_8400476786489650526_n.jpg', '', '', 5, '2020-10-02 16:22:06'),
(17, 1, '2011.pdf', '', '', 5, '2020-10-02 16:27:30'),
(18, 1, '2011.pdf', 'Sample_1', 'dsdsd', 5, '2020-10-02 16:30:03'),
(19, 1, 'GPA.xlsx', 'Sample_1', '11111', 5, '2020-10-02 16:31:04'),
(20, 1, '120455689_3687034411315728_994996462044443797_n.jpg', 'Sample_1', 'sds', 5, '2020-10-02 16:32:59'),
(21, 1, 'GPA.xlsx', 'sds', 'sds', 5, '2020-10-02 16:34:05'),
(22, 1, '119707587_177664890652009_2417372771448711284_n.jpg', 'Sample_21', '22', 5, '2020-10-02 16:37:32'),
(23, 1, 'Certificate_of_Candidacy.pdf', '', '', 5, '2020-10-02 17:06:35'),
(24, 1, 'robot-04-512.png', '', '', 5, '2020-10-02 17:18:59'),
(25, 1, 'CCGPA.xlsx', 'Chapter 2', '12', 5, '2020-10-02 17:20:38'),
(26, 10, 'Umarkets_Ebook_EN.pdf', 'Chapter 2', '2', 5, '2020-10-02 17:22:56'),
(27, 10, 'CXCGPA.xlsx', 'GWA Compute', 'Excel for GWA Compute', 5, '2020-10-02 18:47:36'),
(28, 10, 'PUP_General_Clearance(1).pdf', 'PUP Clearance', 'Cleance', 5, '2020-10-02 18:49:33'),
(29, 5, '40 NCR-SF050 - Complaint Form.doc', 'Chapter 1', 'Last HUman', 5, '2020-10-02 19:41:42'),
(30, 5, '40 ncr-sf050 - complaint form.doc', 'Chapter 2', 'the last stand', 5, '2020-10-02 19:44:55'),
(31, 11, 'cxcgpa.xlsx', 'Chapter 2', '21', 5, '2020-10-02 19:48:37'),
(32, 11, 'certificate_of_candidacy.pdf', 'Chapter 1', 'ere', 5, '2020-10-02 19:49:02'),
(33, 11, 'c_xc-g-p_a.xlsx', 'Chapter 3', '1212', 5, '2020-10-02 19:51:47'),
(34, 5, 'c_xc-g-p_a.xlsx', 'Chapter 3', '1212', 5, '2020-10-02 19:52:51'),
(35, 5, 'c_xc-g-p_a.xlsx', 'Chapter 4', '4', 5, '2020-10-02 19:55:40'),
(36, 11, 'cxcgpa.xlsx', 'Chapter 4', '4', 5, '2020-10-02 19:56:40'),
(37, 3, 'cxcgpa.xlsx', 'Chapter 1', '1122', 5, '2020-10-02 19:59:26'),
(38, 12, '40 ncrsf050  complaint form.doc', 'Chapter 1', 'The frost Castle', 5, '2020-10-02 20:02:46'),
(39, 12, '40 ncrsf050  complaint form.doc', 'Chapter 2', '2', 5, '2020-10-02 20:03:50'),
(40, 3, '40ncrsf050complaintform.doc', 'Chapter 2', '12', 5, '2020-10-02 20:06:53'),
(41, 3, 'robot04512.png', 'Chapter 3', '3', 5, '2020-10-02 20:09:57'),
(42, 3, 'cxcgpa.xlsx', 'Chapter 4', '3', 5, '2020-10-02 20:13:04'),
(43, 3, 'cxcgpa.xlsx', 'Chapter 5', '44', 5, '2020-10-02 20:16:39'),
(44, 12, 'cxcgpa.xlsx', 'Chapter 3', '11', 5, '2020-10-02 20:19:26'),
(45, 12, 'cxcgpa.xlsx', 'Chapter 4', '111', 5, '2020-10-02 20:22:39'),
(46, 12, '40ncrsf050complaintform.doc', 'Chapter 5', '11', 5, '2020-10-02 20:24:55'),
(47, 12, 'cxcgpa.xlsx', 'Chapter 6', '112', 5, '2020-10-02 20:27:46'),
(48, 12, 'cxcgpa.xlsx', 'Chapter 7', '3456', 5, '2020-10-02 20:28:56'),
(49, 5, 'cxcgpa.xlsx', 'Chapter 5', '1213', 5, '2020-10-02 20:30:23'),
(50, 11, 'cxcgpa.xlsx', 'Chapter 5', '12', 5, '2020-10-02 20:31:08'),
(51, 10, 'cxcgpa.xlsx', 'Chapter 3', 'we', 5, '2020-10-02 20:31:59'),
(52, 10, 'cxcgpa.xlsx', 'Chapter 1', 'ewe', 5, '2020-10-02 20:32:17'),
(53, 10, 'cxcgpa.xlsx', 'Chapter 4', '232', 5, '2020-10-02 20:34:03'),
(54, 3, 'cxcgpa.xlsx', 'Chapter 6', '122', 5, '2020-10-02 20:34:34'),
(55, 13, 'forfreshgraduates.pdf', 'Chapter 1', 'The new', 5, '2020-10-12 00:05:03'),
(56, 13, 'convergingartificialintelligencedsafeedback.docx', 'Chapter 2', '2', 5, '2020-10-12 00:06:15'),
(57, 13, 'odrshowtorequest.pdf', 'Chapter 3', '4', 5, '2020-10-12 00:07:25');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `sender_name` varchar(33) NOT NULL,
  `receiver_name` varchar(33) NOT NULL,
  `message_text` text NOT NULL,
  `status` int(11) NOT NULL,
  `date_time` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `sender_name`, `receiver_name`, `message_text`, `status`, `date_time`) VALUES
(51, 'admin@gmail.com', 'jb007@gmail.com', 'Hello', 1, '2020-10-10 05:50:46'),
(52, 'admin@gmail.com', 'mpcordero@gmail.com', 'Hi Sir cordero', 1, '2020-10-10 05:53:31'),
(53, 'admin@gmail.com', 'mpcordero@gmail.com', 'hi sir', 1, '2020-10-10 05:54:35'),
(54, 'admin@gmail.com', 'admin@gmail.com', '', 1, '2020-10-10 05:54:50'),
(55, 'admin@gmail.com', 'jb007@gmail.com', 'Aloha!', 1, '2020-10-10 05:59:59'),
(56, 'admin@gmail.com', 'jb007@gmail.com', 'HI men', 1, '2020-10-10 06:02:16'),
(57, 'admin@gmail.com', 'jb007@gmail.com', 'sds', 1, '2020-10-10 06:02:55'),
(58, 'admin@gmail.com', 'mpcordero@gmail.com', 'Sir COrdero', 1, '2020-10-10 06:03:36'),
(59, 'admin@gmail.com', 'admin@gmail.com', 'I am admin\r\n', 1, '2020-10-10 06:05:08'),
(60, 'admin@gmail.com', 'jb007@gmail.com', 'What do you want?', 1, '2020-10-10 06:05:41'),
(61, 'mpcordero@gmail.com', 'admin@gmail.com', 'What do you want ADMIN', 1, '2020-10-11 00:15:18'),
(62, 'mpcordero@gmail.com', 'admin@gmail.com', 'admin are you there?', 1, '2020-10-10 06:30:53'),
(63, 'jb007@gmail.com', 'admin@gmail.com', 'I am a TEACHER ADMIN', 1, '2020-10-10 06:31:21'),
(64, 'admin@gmail.com', 'jb007@gmail.com', 'I want you to call me SENPAI!', 1, '2020-10-10 06:31:51'),
(65, 'jb007@gmail.com', 'mpcordero@gmail.com', 'HI Mr. COrdero', 1, '2020-10-10 06:33:05'),
(66, 'mpcordero@gmail.com', 'jb007@gmail.com', 'Yes?', 1, '2020-10-10 06:33:24'),
(67, 'jb007@gmail.com', 'mpcordero@gmail.com', 'hi ccc', 1, '2020-10-10 06:41:06'),
(68, 'mpcordero@gmail.com', 'admin@gmail.com', 'Admin I want to know more abou ', 1, '2020-10-10 06:44:27'),
(69, 'mpcordero@gmail.com', 'admin@gmail.com', 'admin', 1, '2020-10-10 06:44:36'),
(70, 'mpcordero@gmail.com', 'admin@gmail.com', 'I want to eat', 1, '2020-10-10 06:46:30'),
(71, 'mpcordero@gmail.com', 'admin@gmail.com', 'oneha', 1, '2020-10-10 06:47:32'),
(72, 'admin@gmail.com', 'mpcordero@gmail.com', 'nani?', 1, '2020-10-10 06:48:07'),
(73, 'jb07s@gmail.com', 'mpcorderco@gmail.com', 'Kamusta', 0, '2020-10-14 06:19:40'),
(74, 'jb07s@gmail.com', 'mpcorderco@gmail.com', 'Kamusta', 0, '2020-10-14 06:19:46');

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  `aid` varchar(100) NOT NULL,
  `module_creator` int(11) NOT NULL,
  `class` int(11) NOT NULL,
  `module_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `description`, `aid`, `module_creator`, `class`, `module_created`) VALUES
(3, 'Module 2', '', 'module2', 5, 3, '2020-09-28 22:22:53'),
(5, 'Module 4', '', 'module4', 5, 3, '2020-09-28 22:23:18'),
(10, 'Module 1', 'Module 1', 'module1', 5, 3, '2020-09-29 21:00:57'),
(11, 'Module 3', '3', 'module3', 5, 3, '2020-09-29 21:01:28'),
(12, 'Module 5', 'Module 5 - Into the Unknown', 'module5', 5, 3, '2020-10-02 20:02:12');

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

CREATE TABLE `section` (
  `id` int(11) NOT NULL,
  `section_name` varchar(50) NOT NULL,
  `sect_id` varchar(50) NOT NULL,
  `teacher_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `section`
--

INSERT INTO `section` (`id`, `section_name`, `sect_id`, `teacher_id`) VALUES
(1, '1', 'section1', 17),
(2, '2', 'section2', 19),
(3, '3', 'section3', 17),
(4, '4', 'section4', 18),
(5, '5', 'section5', 18);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `sname` varchar(50) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `mname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL,
  `section` varchar(100) NOT NULL,
  `type` int(11) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `sname`, `fname`, `mname`, `email`, `password`, `section`, `type`, `date_created`) VALUES
(3, 'Bond', 'James', 'Smith', 'jb007@gmail.com', '1sYos4p2', '3', 2, '2020-10-10 02:33:45'),
(5, 'Cordero', 'Marti', 'Carter', 'mpcordero@gmail.com', 'vNghqc6O', '1|2|', 1, '2020-10-10 02:57:33'),
(6, 'admin', 'admin', 'admin', 'admin@gmail.com', 'admin', '0', 0, '2020-10-10 21:02:44'),
(7, 'Cruz', 'Juan', 'Dela', 'jcd@gmail.com', 'BRaqpL6C', '', 1, '2020-10-14 03:29:40'),
(8, 'Fernando', 'John', 'Arizala', 'fja@gmail.com', 'dvjExZQl', '', 1, '2020-10-14 03:31:36'),
(9, 'Natividad', 'Ferdidnand ', 'Ovipee', 'fno@gmail.com', 'De3EcT0i', '3', 1, '2020-10-14 03:38:40'),
(10, 'Catañeda', 'Jhun', 'Venerasion', 'jvc@gmail.com', 'eOVGTgk0', '', 1, '2020-10-14 03:52:26'),
(11, 'Catañedas', 'Jhuns', 'Venerasions', 'jvcs@gmail.com', 'r4tIvMLx', '3', 2, '2020-10-14 03:53:35'),
(12, 'Cansino', 'Julius', 'Santiago', 'jscansino@gmail.com', 'gOeXK9r3', '', 1, '2020-10-14 03:56:08'),
(13, 'Poloyapos', 'Mathew', 'Martias', 'mmp@yahoo.com', 'n!GNlwNp', '', 1, '2020-10-14 04:08:24'),
(14, 'Ramno', 'Keneth', 'Santa', 'ksr@gmail.com', 'gh6pRMOa', '', 1, '2020-10-14 04:10:42'),
(15, 'Bonds', 'John', 'Carter', 'jaysonolaguera@gmail.com', 'Dwhbnb!c', '', 1, '2020-10-14 04:11:49'),
(16, 'Bond', 'Jamess', 'Doe', 'jb007s@gmail.com', 'N1Z8Xv3c', '', 1, '2020-10-14 04:13:01'),
(17, 'Bond', 'Jamess', 'Does', 'jb07s@gmail.com', 'EZf4dZZz', '', 1, '2020-10-14 04:13:55'),
(18, 'Carter', 'Jamess', 'Row', 'teacher@gmail.com', 'KcUgKrGL', '', 1, '2020-10-14 04:17:24'),
(19, 'Smith', 'Mcclane', 'Panganiban', 'teacher2@gmail.com', 'LCWACnvi', '', 1, '2020-10-14 04:19:21'),
(20, 'Olaguera', 'Minchin', 'Smith', 'minch@gmail.com', '123456', '1', 2, '2020-10-14 22:45:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assessments`
--
ALTER TABLE `assessments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chatbot`
--
ALTER TABLE `chatbot`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `assessments`
--
ALTER TABLE `assessments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `chatbot`
--
ALTER TABLE `chatbot`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `section`
--
ALTER TABLE `section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
