-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 22, 2020 at 05:39 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_dsa`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE `announcements` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `post_creator` varchar(50) NOT NULL,
  `post_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `announcements`
--

INSERT INTO `announcements` (`id`, `title`, `content`, `post_creator`, `post_created`) VALUES
(45, 'Please upload your modules ', 'Please upload your modules.\r\nIf you have question just ask to us.', 'admin admin', '2020-11-22 22:52:11'),
(46, 'Bug Found!', 'If you found some bug or ERROR please message us', 'admin admin', '2020-11-22 22:56:18');

-- --------------------------------------------------------

--
-- Table structure for table `assessments`
--

CREATE TABLE `assessments` (
  `id` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `tid` int(11) NOT NULL,
  `section` int(100) NOT NULL,
  `m1e1` int(105) NOT NULL,
  `m1e2` int(11) NOT NULL,
  `m1e3` int(11) NOT NULL,
  `m1assessment` int(11) NOT NULL,
  `m2e1` int(11) NOT NULL,
  `m2e2` int(11) NOT NULL,
  `m2e3` int(11) NOT NULL,
  `m2assessment` int(11) NOT NULL,
  `m3e1` int(11) NOT NULL,
  `m3e2` int(11) NOT NULL,
  `m3e3` int(11) NOT NULL,
  `m3assessment` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `assessments`
--

INSERT INTO `assessments` (`id`, `sid`, `tid`, `section`, `m1e1`, `m1e2`, `m1e3`, `m1assessment`, `m2e1`, `m2e2`, `m2e3`, `m2assessment`, `m3e1`, `m3e2`, `m3e3`, `m3assessment`) VALUES
(11, 27, 22, 1, 100, 99, 99, 100, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `chatbot`
--

CREATE TABLE `chatbot` (
  `id` int(11) NOT NULL,
  `queries` varchar(300) NOT NULL,
  `replies` varchar(300) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chatbot`
--

INSERT INTO `chatbot` (`id`, `queries`, `replies`, `created_by`, `date_created`) VALUES
(1, 'Hi|Hello|Hy|hey| hi', 'Hello there!', '', '2020-11-17 22:14:40'),
(2, 'What is your name|what is your name?', 'My name is DSA Chatbot', '', '2020-11-17 22:14:40'),
(3, 'where are you from|where are you from?', 'I am from this system', '', '2020-11-17 22:14:40'),
(4, 'bye|good bye|goodbye', 'Ok. Goodbye, Thank you for talking with me.', '', '2020-11-17 22:14:40'),
(5, 'Define DSA| What is DSA?', 'DSA - Data Structure and Algorithm', 'admin admin', '2020-11-17 22:38:44');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(30) NOT NULL,
  `comment` varchar(300) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `subject` varchar(300) NOT NULL,
  `frM` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `comment`, `status`, `subject`, `frM`) VALUES
(5, 'hello', 1, 'Sample', ''),
(6, '2', 1, 'Sample 2', ''),
(7, '3', 1, 'Sample 3', ''),
(8, '4', 1, 'Sample 4', ''),
(9, 'hrtyutnruityujtuytutfutriktyiuftutrii5nit6urtujyti', 1, 'geryeryhreuhrerth', ''),
(10, 'dfsfsdf', 1, 'geryeryhreuhrerth', 'Jayson'),
(11, 'sdfsdf', 1, 'dfsdfs', 'Jayson'),
(12, 'efefe', 1, 'Sample', 'Jayson'),
(13, 'qq', 1, 'Sample', 'Jayson'),
(14, 'cxcx', 1, 'xc', 'Jayson'),
(15, 'sds', 1, 'sdsd', 'Jayson'),
(16, 'sd', 1, 'dss', 'sd');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `mid` int(11) NOT NULL,
  `fname` varchar(150) NOT NULL,
  `title` varchar(50) NOT NULL,
  `descrip` varchar(150) NOT NULL,
  `file_creator` int(11) NOT NULL,
  `file_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `mid`, `fname`, `title`, `descrip`, `file_creator`, `file_created`) VALUES
(79, 25, 'c.pdf', 'Chapter 1', 'Basic Function', 22, '2020-11-23 00:15:11'),
(81, 24, 'november.pdf', 'Chapter 1', 'NOV', 22, '2020-11-23 00:18:39');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `sender_name` varchar(33) NOT NULL,
  `receiver_name` varchar(33) NOT NULL,
  `message_text` text NOT NULL,
  `status` int(11) NOT NULL,
  `date_time` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `sender_name`, `receiver_name`, `message_text`, `status`, `date_time`) VALUES
(107, 'admin@gmail.com', 'teacher1@gmail.com', 'I am your admin', 0, '2020-11-22 03:51:13');

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  `aid` varchar(100) NOT NULL,
  `module_creator` int(11) NOT NULL,
  `class` int(11) NOT NULL,
  `module_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `description`, `aid`, `module_creator`, `class`, `module_created`) VALUES
(24, 'Module 2', 'Module 2 - Application', 'module2', 22, 0, '2020-11-22 23:56:54'),
(25, 'Module 1', 'Module 1 - Intro', 'module1', 22, 0, '2020-11-22 23:59:30'),
(26, 'Module 3', 'Module 3 - Final', 'module3', 22, 0, '2020-11-22 23:59:50');

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

CREATE TABLE `section` (
  `id` int(11) NOT NULL,
  `section_name` varchar(50) NOT NULL,
  `sect_id` varchar(50) NOT NULL,
  `teacher_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `section`
--

INSERT INTO `section` (`id`, `section_name`, `sect_id`, `teacher_id`) VALUES
(1, '1', 'section1', 22),
(2, '2', 'section2', 23),
(3, '3', 'section3', 24),
(4, '4', 'section4', 25),
(5, '5', 'section5', 26);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_exam`
--

CREATE TABLE `tbl_exam` (
  `id` int(11) NOT NULL,
  `eid` text NOT NULL,
  `total` int(11) NOT NULL,
  `rght` int(11) NOT NULL,
  `wrng` int(11) NOT NULL,
  `exam_creator` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `exam_name` varchar(200) NOT NULL,
  `exam_title` varchar(200) NOT NULL,
  `status` varchar(20) NOT NULL,
  `time` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_exam`
--

INSERT INTO `tbl_exam` (`id`, `eid`, `total`, `rght`, `wrng`, `exam_creator`, `module_id`, `exam_name`, `exam_title`, `status`, `time`, `date_created`) VALUES
(18, '5fba8d366bc8d', 1, 1, 0, 22, 25, 'enrich1', 'M1 Enrichment 1', 'enabled', 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_exam_que`
--

CREATE TABLE `tbl_exam_que` (
  `id` int(11) NOT NULL,
  `eid` text NOT NULL,
  `question` text NOT NULL,
  `ctr` int(100) NOT NULL,
  `option1` varchar(500) NOT NULL,
  `option2` varchar(500) NOT NULL,
  `option3` varchar(500) NOT NULL,
  `answer` varchar(500) NOT NULL,
  `userans` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_exam_que`
--

INSERT INTO `tbl_exam_que` (`id`, `eid`, `question`, `ctr`, `option1`, `option2`, `option3`, `answer`, `userans`) VALUES
(26, '5fba8d366bc8d', 'What is DSA?', 1, 'Data Structure and Algorithm', 'Data Structures and Algorithm', 'Data Structure and Algorithms', 'a', 'a');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_exam_result`
--

CREATE TABLE `tbl_exam_result` (
  `id` int(11) NOT NULL,
  `eid` text NOT NULL,
  `uid` int(11) NOT NULL,
  `mid` int(11) NOT NULL,
  `ename` varchar(50) NOT NULL,
  `exam_creator` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `date_taken` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_history`
--

CREATE TABLE `tbl_history` (
  `hid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `section` int(12) NOT NULL,
  `tid` int(11) NOT NULL,
  `eid` text NOT NULL,
  `score` int(20) NOT NULL,
  `grade` int(100) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_history`
--

INSERT INTO `tbl_history` (`hid`, `uid`, `section`, `tid`, `eid`, `score`, `grade`, `date_created`) VALUES
(15, 27, 1, 22, '5fba8d366bc8d', 1, 100, '2020-11-23 00:29:36');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `sname` varchar(50) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `mname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL,
  `section` varchar(100) NOT NULL,
  `type` int(11) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `sname`, `fname`, `mname`, `email`, `password`, `section`, `type`, `date_created`) VALUES
(6, 'admin', 'admin', 'admin', 'admin@gmail.com', 'admin', '0', 0, '2020-10-10 21:02:44'),
(22, 'Oliveros', 'Mark', 'Oliver', 'teacher1@gmail.com', 'teacher1', '', 1, '2020-11-22 03:28:04'),
(23, 'Bautista', 'Christian', 'Santiago', 'teacher2@gmail.com', 'teacher2', '', 1, '2020-11-22 03:32:56'),
(24, 'Nadal', 'Raphael', 'Simon', 'teacher3@gmail.com', 'teacher3', '', 1, '2020-11-22 03:33:40'),
(25, 'Sharapova', 'Maria', 'Chenkov', 'teacher4@gmail.com', 'teacher4', '', 1, '2020-11-22 03:34:27'),
(26, 'Evagelista', 'Maria', 'Elena', 'teacher5@gmail.com', 'teacher5', '', 1, '2020-11-22 03:35:19'),
(27, 'Dela Cruz', 'Juan', 'Bautista', 'student1@gmail.com', 'student1', '1', 2, '2020-11-22 03:36:03'),
(28, 'Ramos', 'Ramon', 'Bautista', 'student2@gmail.com', 'student2', '2', 2, '2020-11-22 03:40:13'),
(29, 'Simon', 'Fitz', 'Finch', 'student3@gmail.com', 'student3', '3', 2, '2020-11-22 03:41:43'),
(30, 'Remedios', 'Elenor', 'Santi', 'student4@gmail.com', 'student4', '4', 2, '2020-11-22 03:42:49'),
(31, 'Aquino', 'Kris', 'Elpidio', 'student5@gmail.com', 'student5', '5', 2, '2020-11-22 03:43:34');

-- --------------------------------------------------------

--
-- Table structure for table `user_pic`
--

CREATE TABLE `user_pic` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `img_name` varchar(200) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assessments`
--
ALTER TABLE `assessments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chatbot`
--
ALTER TABLE `chatbot`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_exam`
--
ALTER TABLE `tbl_exam`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_exam_que`
--
ALTER TABLE `tbl_exam_que`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_exam_result`
--
ALTER TABLE `tbl_exam_result`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_history`
--
ALTER TABLE `tbl_history`
  ADD PRIMARY KEY (`hid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_pic`
--
ALTER TABLE `user_pic`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `assessments`
--
ALTER TABLE `assessments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `chatbot`
--
ALTER TABLE `chatbot`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `section`
--
ALTER TABLE `section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_exam`
--
ALTER TABLE `tbl_exam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tbl_exam_que`
--
ALTER TABLE `tbl_exam_que`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `tbl_exam_result`
--
ALTER TABLE `tbl_exam_result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_history`
--
ALTER TABLE `tbl_history`
  MODIFY `hid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `user_pic`
--
ALTER TABLE `user_pic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
