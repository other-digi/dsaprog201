<!DOCTYPE html>
<html lang="en">

<?php
SESSION_START();
include('includes/head.php');?>

<body>
	
  <!-- Header bar -->
    <?php include('header.php');?>
  <!-- end of  Header bar -->
  <div class="d-flex" id="wrapper">
	
    <!-- Sidebar -->
    <?php include('menu.php');?>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

        
    <div class="container-fluid">
        <h1 class="mt-4">Assessment</h1>
        <p>The starting state of the menu will appear collapsed on smaller screens, and will appear non-collapsed on larger screens. When toggled using the button below, the menu will change.</p>
        <p>Make sure to keep all page content within the <code>#page-content-wrapper</code>. The top navbar is optional, and just for demonstration. Just create an element with the <code>#menu-toggle</code> ID which will toggle the menu when clicked.</p>
      </div>
   
    

    
    


      
    </div>
    <!-- /#page-content-wrapper -->
	  <div class="bg-light border-right" id="sidebar-wrapper">
      <?php include_once('../includes/bot.php');?>
      
    </div>
  </div>
  <!-- /#wrapper -->
	
  <!-- Bootstrap core JavaScript -->
  <script src="../vendor/jquery/jquery.min.js"></script>
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <script src="../js/active_page.js"></script>

</body>

</html>
