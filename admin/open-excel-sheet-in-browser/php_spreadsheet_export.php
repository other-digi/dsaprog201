<?php

//php_spreadsheet_export.php

include 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;


$connect = new PDO("mysql:host=localhost;dbname=mhwkoigt_dsaprog201", "mhwkoigt_dsaprog201", "Hg!6Bsrxi!2zAqf");


$query = "SELECT * FROM users WHERE type=2 ORDER BY section ASC, sname ASC";

$statement = $connect->prepare($query);

$statement->execute();

$result = $statement->fetchAll();

if(isset($_POST["export"]))
{
  $file = new Spreadsheet();

  $active_sheet = $file->getActiveSheet();

  $active_sheet->setCellValue('A1', 'Last Name');
  $active_sheet->setCellValue('B1', 'First Name');
  $active_sheet->setCellValue('C1', 'Middle Name');
  $active_sheet->setCellValue('D1', 'Email');
  $active_sheet->setCellValue('E1', 'Password');
  $active_sheet->setCellValue('F1', 'Section');
  

  $count = 2;

  foreach($result as $row)
  {
    $active_sheet->setCellValue('A' . $count, $row["sname"]);
    $active_sheet->setCellValue('B' . $count, $row["fname"]);
    $active_sheet->setCellValue('C' . $count, $row["mname"]);
    $active_sheet->setCellValue('D' . $count, $row["email"]);
	$active_sheet->setCellValue('E' . $count, $row["password"]);
	$active_sheet->setCellValue('F' . $count, $row["section"]);
	

    $count = $count + 1;
  }

  $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($file, $_POST["file_type"]);

  $file_name = time() . '.' . strtolower($_POST["file_type"]);

  $writer->save($file_name);

  header('Content-Type: application/x-www-form-urlencoded');

  header('Content-Transfer-Encoding: Binary');

  header("Content-disposition: attachment; filename=\"".$file_name."\"");

  readfile($file_name);

  unlink($file_name);

  exit;

}

?>
<!DOCTYPE html>
<html>
  	<head>
    	<title>Export Data</title>
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  	</head>
  	<body>
    	<div class="container">
    		<br />
    		<h3 align="center">Export Student List</h3>
    		<br />
        <div class="panel panel-default">
          <div class="panel-heading">
            <form method="post">
              <div class="row">
                <div class="col-md-6">User Data</div>
                <div class="col-md-4">
                  <select name="file_type" class="form-control input-sm">
                    <option value="Xlsx">Xlsx</option>
                    <option value="Xls">Xls</option>
                    <option value="Csv">Csv</option>
                  </select>
                </div>
                <div class="col-md-2">
                  <input type="submit" name="export" class="btn btn-primary btn-sm" value="Export" />
                </div>
              </div>
            </form>
          </div>
          <div class="panel-body">
        		<div class="table-responsive">
        			<table class="table table-striped table-bordered">
                <tr>
                  <th>#</th>
                  <th>Last Name</th>
				  <th>First Name</th>
				  <th>Middle Name</th>
				  <th>Email</th>
				  <th>Password</th>
                  <th>Section</th>
                  <th>Updated At</th>
                </tr>
                <?php
				$ctr = 0;
                foreach($result as $row)
                {
					$ctr++;
                  echo '
                  <tr>
				  <td>'.$ctr.'</td>
                    <td>'.$row["sname"].'</td>
                    <td>'.$row["fname"].'</td>
                    <td>'.$row["mname"].'</td>
                    <td>'.$row["email"].'</td>
					<td>'.$row["password"].'</td>
					<td>'.$row["section"].'</td>
					<td>'.$row["date_created"].'</td>
                  </tr>
                  ';
                }
                ?>

              </table>
        		</div>
          </div>
        </div>
    	</div>
      <br />
      <br />
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
  </body>
</html>