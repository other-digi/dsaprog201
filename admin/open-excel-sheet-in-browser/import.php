<?php

//import.php

include 'vendor/autoload.php';

$connect = new PDO("mysql:host=localhost;dbname=mhwkoigt_dsaprog201", "mhwkoigt_dsaprog201", "Hg!6Bsrxi!2zAqf");

if($_FILES["import_excel"]["name"] != '')
{
	$allowed_extension = array('xls', 'csv', 'xlsx');
	$file_array = explode(".", $_FILES["import_excel"]["name"]);
	$file_extension = end($file_array);

	if(in_array($file_extension, $allowed_extension))
	{
		$file_name = time() . '.' . $file_extension;
		move_uploaded_file($_FILES['import_excel']['tmp_name'], $file_name);
		$file_type = \PhpOffice\PhpSpreadsheet\IOFactory::identify($file_name);
		$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($file_type);

		$spreadsheet = $reader->load($file_name);
		

		unlink($file_name);

		$data = $spreadsheet->getActiveSheet()->toArray();
		
		$count = 0;
		foreach($data as $row)
		{
			$count++;
			if ($count == 1) {
                continue; // skip the heading header of sheet
            }
			$insert_data = array(
				':sname'		=>	$row[0],
				':fname'		=>	$row[1],
				':mname'		=>	$row[2],
				':email'		=>	$row[3],
				':password'		=>	$row[4],
				':section'		=>	$row[5],
				':type'		=>	$row[6]
			);
			
			

			$query = "
			INSERT INTO users 
			(sname, fname, mname, email, password, section, type) 
			VALUES (:sname, :fname, :mname, :email, :password, :section, :type)
			";

			$statement = $connect->prepare($query);
			$statement->execute($insert_data);
		}
		$message = '<div class="alert alert-success">Data Imported Successfully. <br> Data will be reloaded in 3 seconds </div>
		
		';

	}
	else
	{
		$message = '<div class="alert alert-danger">Only .xls .csv or .xlsx file allowed</div>';
	}
}
else
{
	$message = '<div class="alert alert-danger">Please Select File</div>';
}

echo $message;

?>