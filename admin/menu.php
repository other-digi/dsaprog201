<?php 

$uid = $_GET['id'];
?>
<div class="bg-light border-right" id="sidebar-wrapper">
     
      <div class="list-group list-group-flush">
        <a href="index.php?id=<?php echo $uid;?>" class="list-group-item list-group-item-action bg-light">Dashboard</a>
        <a href="teachers.php?id=<?php echo $uid;?>" class="list-group-item list-group-item-action bg-light">Teachers</a>
        <a href="students.php?id=<?php echo $uid;?>" class="list-group-item list-group-item-action bg-light">Students</a>
		<a href="chatbot.php?id=<?php echo $uid;?>" class="list-group-item list-group-item-action bg-light">Chatbot</a>
        <a href="messages.php?id=<?php echo $uid;?>" class="list-group-item list-group-item-action bg-light">Messages</a>
		<a href="bak.php?id=<?php echo $uid;?>" class="list-group-item list-group-item-action bg-light">Backup</a>
        <a href="logout.php" class="list-group-item list-group-item-action bg-light">Logout</a>
      </div>
    </div>


  