<!DOCTYPE html>
<html lang="en">

<?php
SESSION_START();

if(!isset($_SESSION['error'])){
	$_SESSION['error'] = "";
}
if(!isset($_SESSION['success'])){
	$_SESSION['success'] = "";
}
include('includes/head.php');?>

<body>
	<!-- Header bar -->
  <?php include('header.php');?>
  <!-- end of  Header bar -->
  <div class="d-flex" id="wrapper">
	
    <!-- Sidebar -->
    <?php include('menu.php');?>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

        

   
    <div class="container-fluid">
		<div  style="width:500px; float:right; padding:5px;" id="mgss">
							<?php
							
							if($_SESSION['success']){
								echo "<h4 class='btn-success'>".$_SESSION['success']."<h4>"; unset($_SESSION['success']);
							}else{
								echo "<h4 class='btn-warning'>".$_SESSION['error']."</h4>"; unset($_SESSION['error']);
							}
							 ?>
						
					</div>
        <?php 
		
			include_once('../messaging/index.php');
		
		?>
    </div>

    
    


      
    </div>
    <!-- /#page-content-wrapper -->
	  <div class="bg-light border-right" id="sidebar-wrapper">
      <?php include_once('../includes/bot.php');?>
      
    </div>
  </div>
  <!-- /#wrapper -->
	
  <!-- Bootstrap core JavaScript -->
  <script src="../vendor/jquery/jquery.min.js"></script>
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <script src="../js/active_page.js"></script>

</body>

</html>
