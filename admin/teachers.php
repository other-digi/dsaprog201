<?php  
session_start();
include('../includes/conn.php');
include('includes/password_generate.php');

//index.php

if(!isset($_SESSION['error'])){
	$_SESSION['error'] = "";
}
if(!isset($_SESSION['success'])){
	$_SESSION['success'] = "";
}

$query = "SELECT * FROM users WHERE type='1' ORDER BY sname ASC";
$result = mysqli_query($conn, $query);
 ?>  
<!DOCTYPE html>
<html lang="en">

<?php
include('includes/head.php');?>

<SCRIPT language=Javascript>
      <!--
      function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
      //-->
   </SCRIPT>
	<body>
		<!-- Header bar -->
	  <?php include('header.php');?>
	  <!-- end of  Header bar -->
	  <div class="d-flex" id="wrapper">
		
			<!-- Sidebar -->
			<?php include('menu.php');?>
			<!-- /#sidebar-wrapper -->

			<!-- Page Content -->
			<div id="page-content-wrapper">
				<div class="container" >  
				   <h2>List of Teachers</h2>
				<div align="left">
				 <button type="button" name="age" id="age" data-toggle="modal" data-target="#add_teacher_Modal" class="btn btn-primary">Add Teacher</button>
				<div  style="width:500px; float:right; padding:5px;" id="mgss">
							<?php
							
							if($_SESSION['success']){
								echo "<h4 class='btn-success'>".$_SESSION['success']."<h4>"; unset($_SESSION['success']);
							}else{
								echo "<h4 class='btn-warning'>".$_SESSION['error']."</h4>"; unset($_SESSION['error']);
							}
							 ?>
						
					</div>
				</div>
				<br />
				<table class="table table-fluid" id="teacherlist" >
					<thead>
						<tr>
							<th>#</th>
							<th>NAME</th>
							<th>EMAIL</th>
							<th>SECTION HANDLE</th>
							<th >Action</th>  
						</tr>
					</thead>
					
					<tbody>
						  <?php
						  $count = 1;
						  while($row = mysqli_fetch_array($result))
						  {
						  ?>
						  
							<tr>
								<td><?php echo $count;?></td>
								<td><?php echo $row['fname']." ".$row['sname'];?></td>
								<td><?php echo $row['email'];?></td>
								<td><?php $tID = $row['id'];
								
								$outputs ="";
								$qq = "SELECT * FROM section WHERE teacher_id='".$tID."'";
								$rr = mysqli_query($conn,$qq);
								if(mysqli_num_rows($rr)>0){
									while($rw = mysqli_fetch_array($rr)){
										
										$outputs .= $rw['section_name']." | ";
									}
									echo $outputs;
								}else{
									echo "No Section Assigned";
								}
								
								
								?></td>
							  
								<td>
								   
								   <button type="submit" data-id="<?php echo $_GET["id"]; ?>" id="<?php echo $row["id"]; ?>" class=" btn-info btn-xs view_data"  title="UPDATE"><i class="fa fa-pencil"></i></button>
								   <button type="submit" data-id="<?php echo $_GET["id"]; ?>" id="<?php echo $row["id"]; ?>" class=" btn-warning btn-xs archive_data" title="ARCHIVE" ><i class="fa fa-trash"></i></button>
							   </td>
							  
							</tr>
						  <?php
						  $count++;
						  }
					  ?>
					  </tbody>
					  
					  <tfoot>
					 
							<tr>
							  <th>#</th>
							<th>NAME</th>
							<th>EMAIL</th>
							<th>SECTION HANDLE</th>
							<th >Action</th>
							</tr>
					  
					  </tfoot>
				</table>
					
					
				</div> 
			</div>
			<!-- /#page-content-wrapper -->
			  <div class="bg-light border-right" id="sidebar-wrapper">
				<?php include_once('../includes/bot.php');?>
			  </div>
	  </div>
	  <!-- /#wrapper -->
	<div id="add_teacher_Modal" class="modal fade" tabindex="-1">
		
			<div class="modal-dialog  modal-lg">
				<div class="modal-content" >
					<div class="modal-header">
						<h4 class="modal-title">Add Teacher</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<br />
		
					</div>
					<div class="modal-body">
						<form method="POST" id="add_teacher" action="add_teacher.php">
						  <div class="container-fluid">
							<div class="row">
							  <div class="col-lg ">
								<label >SURNAME</label><br/>
									<input type="text" name="sname" id="sname" class="form-control" />
							  </div>
							  <div class="col-md ">
							  <label >FIRST NAME</label><br/>
									<input type="text" name="fname" id="fname" class="form-control" />
							  </div>
							  <div class="col-md ">
								<label >MIDDLE NAME</label><br/>
									<input type="text" name="mname" id="mname" class="form-control" />
							  </div>
							</div>  
						  </div>
						  <br>
						  
						  <div class="container-fluid">
							<div class="row">
							  <div class="col-md ">
								<label >EMAIL/USERNAME</label><br/>
									<input type="email" onkeyup="check_email();" name="email" id="email" class="form-control col-sm-6"  required />
									<div id="checking"></div>
							  </div>
							</div>  
						  </div>
						  
						   <div class="container-fluid">
							<div class="row">
							  <div class="col-md ">
								<label >PASSWORD (AUTO GENERATED)</label><br/>
									<input type="password" name="pswrd" id="pswrd" value="<?php echo generatePassword();?>" class="form-control col-sm-6" />
									<span toggle="#pswrd" class="fa fa-fw fa-eye field-icon toggle-password" onclick="myFunction()" title="Show Password"></span>
							  </div>
							</div>  
						  </div>
						  
						  <div class="container-fluid">
							<div class="row">
							  <div class="col-md " id="sections">
								<label >SECTION HANDLED</label><br/>
								<button type="button" name="add" id="add" class="btn btn-success col-md-3 btn_add_sec" style="float: right;">Add More</button>
									<input type="number" onkeypress="return isNumberKey(event)" id="sectionhandle" name="sectionhandle[]"  class="form-control col-sm-6" required />
									<input type="hidden" value="<?php echo $_GET['id'];?>" name="uid" >
							  </div>
							</div>  
						  </div>
						  <br/>
						  <div class="container-fluid">
							<div class="row">
							  <div class="col-md " id="sections">
								<input type="submit" name="insert" id="insert" value="Insert" class="btn btn-success" />
									
							  </div>
							</div>  
						  </div>
						</form> 
					</div>
				   <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				   </div>
				</div>
			</div>
			
	</div>

	<div id="teacherdataModal" class="modal fade">
	 <div class="modal-dialog modal-lg">
	  <div class="modal-content">
	   <div class="modal-header">
			<h4 class="modal-title">Teacher Details</h4>
			<button type="button" class="close" data-dismiss="modal">&times;</button>
		
	   </div>
	   <div class="modal-body" id="teacher_details">
		
		
	   </div>
	   <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	   </div>
	  </div>
	 </div>
	</div>
	
<!-----DELETE MODAL------------->

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal">
  <div class="modal-dialog modal-l">
    <div class="modal-content">
      <div class="modal-header">
	  <h4 class="modal-title" id="myModalLabel">Are you Sure to Delete?</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       
      </div>
	  <div class="modal-body">
		<form method="post"id="yess_nos" action="delete_teacher.php">
		<input type="hidden" id="tid" name="tid">
		<input type="hidden" id="uid" name="uid" value="<?php echo $_GET['id']?>">
		<button type="submit" class="btn btn-warning" data-id=""id="modal-btn-si">Yes</button>
        <button type="button" class="btn btn-primary" id="modal-btn-no">No</button>
		</form>
	  </div>
      <div class="modal-footer">
       
      </div>
    </div>
  </div>
</div>

<!-------DELETE MODAL ENDS HERE------------->

	<script>
		$(document).ready( function () {
		$('#teacherlist').DataTable();
	} );

	$(document).on('click', '.view_data', function(){
	 $('#teacherdataModal').modal();
	  var teacher_id = $(this).attr("id");
	  var uid = $(this).data("id");
	  $.ajax({
	   url:"view_teacher.php",
	   method:"POST",
	   data:{teacher_id:teacher_id,
	   uid:uid},
	   success:function(data){
		   $('#insert').val("Update"); 
		$('#teacher_details').html(data);
		$('#teacherdataModal').modal('show');
		
		
	   }
	  });
	 });
	 
	
	 /////////ADD TEACHER/////////

	 // $(document).ready(function(){
	 // $('#add_teacher').on("submit", function(event){  
	  // event.preventDefault();  
	  // if($('#fname').val() == "")  
	  // {  
	   // alert("First name is required");  
	  // }  
	  // else if($('#sname').val() == '')  
	  // {  
	   // alert("Surname is required");  
	  // }  
	  // else  
	  // {  
	   // $.ajax({  
		// url:"add_teacher.php",  
		// method:"POST",  
		// data:$('#add_teacher').serialize(),  
		// beforeSend:function(){  
		 // $('#insert').val("Inserting");  
		// },  
		// success:function(data){  
			//window.location.reload(); 
		 //$('#add_teacher')[0].reset();  
		 // $('#add_teacher_Modal').modal('hide');  
		// $('#list_teachers').html(data);  
		// }  
	   // });  
	  // }  
	 // });
	// });  
////////////ADD TEACHER

	
	 $(document).on("submit","#update_teacher", function(){  
	  event.preventDefault();  
	  if($('#fname').val() == "")  
	  {  
	   alert("First name is required");  
	  }  
	  else if($('#sname').val() == '')  
	  {  
	   alert("Surname is required");  
	  }  
	  else  
	  {  
	   $.ajax({  
		url:"update_teacher.php",  
		method:"POST",  
		data:{
				tid: $('#tid').val(),
				sname: $('#sname').val(),
				fname: $('#fname').val(),
				mname: $('#mname').val(),
				email: $('#email').val(),
				pswrd: $('#pswrd').val(),
				sectionhandle: $('#sectionhandle').val(),
		},  
		beforeSend:function(){  
		 $('#updatedata').val("Updating");  
		},  
		success:function(data){  
		//location.reload(); 
		// $('#update_teacher')[0].reset();  
		// $('#teacherdataModal').modal('s');  
		 $('#teacher_details').html(data);  
		 $('#teacherdataModal').modal('show');
		 var data = JSON.parse(data);
				if(data.statusCode==200){
					$('#teacherdataModal').modal().hide();
					alert('Data updated successfully !');
					location.reload();					
				}
		}  
	   });  
	  }  
	 });
	

//////////////UPDATING TEACHER ENDS HERE
	
	
	/////////////ADDING FIELD
	$(document).ready(function(){
	 $('.btn_add_sec').prop('disabled', true);
	var i=1;
	

	
	$('#add').click(function(){
		i++;
		$('#sections').append('<tr id="row'+i+'"><td><input type="number"  onkeypress="return isNumberKey(event);" id="sectionhandle'+i+'" name="sectionhandle[]" placeholder="Enter section" class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');
		if(i >= 20){ alert("Maximum number of Section REACHED!");}
		
		
		
		///////
		
			$(document).on("change","#sectionhandle"+(i), function(){  
	  event.preventDefault();  
	
	  var secId = $('#sectionhandle'+(i)).val();
	  //var secId = $(this).attr("id");
		
	//  alert(secId);
	// alert(i);
	 //console.log("ID count: "+i);
	  if(secId == ""){  
	   alert("Section Required");  
	   $('.btn_add_sec').prop('disabled', true);
	  }else {  
	   $.ajax({  
		url:"includes/check_sec_on_db.php",  
		method:"POST",  
		data:{
				secId: secId
				
		},  
		success:function(data){  
		
		if(data == 'sect_taken'){
			$('#insert').prop('disabled', true);
			/////$('#sectionhandle').empty();
			$('.btn_add_sec').prop('disabled', true);
			$("#sectionhandle"+(i)).val("");
			alert('Section Already Assigned!');
			
		}else if(data == 'no_sect'){
			$('#insert').prop('disabled', true);
			$("#sectionhandle"+(i)).val("");
			$('.btn_add_sec').prop('disabled', true);
			/////$('#sectionhandle').empty();
			alert('Section cannot be found!');
			
		}else{
			$('#insert').prop('disabled', false);
			$('.btn_add_sec').prop('disabled', false);
		}
		}  
	   });  
	  }  
	  
	 });
	});
	
	$(document).on('click', '.btn_remove', function(){
		var button_id = $(this).attr("id"); 
		$('#row'+button_id+'').remove();
		i--;
	})
	
	
////////////
	
	
});

	////////////////ADDING FIELD ENDS HERE
	

//////////////Confirmation MODAL
var modalConfirm = function(callback){
  
  $(".archive_data").on("click", function(){
    $("#mi-modal").modal('show');
  });

  $("#modal-btn-si").on("click", function(){
    callback(true);
    $("#mi-modal").modal('hide');
  });
  
  $("#modal-btn-no").on("click", function(){
    callback(false);
    $("#mi-modal").modal('hide');
  });
};

modalConfirm(function(confirm){
  if(confirm){
    //Acciones si el usuario confirma
	 var teacher_id = $(this).attr("id");
	 console.log(teacher_id);
	//$.ajax({  
		//url:"delete_teacher.php",  
		//method:"POST",  
		//data:$('#yess_nos').serialize(),    
		//success:function(data){  
			//window.location.reload(); 
		 //$('#delete_module')[0].reset();  
		 //$('#deleteModal').modal('hide');  
		//$('#teacherlist').html(data);  
		//}  
	 //  });  
  }else{
    //Acciones si el usuario no confirma
    // $("#result").html("NO CONFIRMADO");
  }
});
//////////Confirmation Modal ends here

// $(".archive_data").on("click", function(){
    // var dataId = $(this).attr("id");
				// $('#tid').val(dataId);
  // });
$(document).ready(function(){
			$(".archive_data").click(function(){
				var dataId = $(this).attr("id");
				$('#tid').val(dataId);
			});
			});
			
//////////fade out////
$(document).ready(function(){
 
   $("#mgss").fadeOut(5000);
});
/////////////fadout ends here
	</script>
<script>
document.getElementById("insert").disabled= true;
////////////check if email exist in d
function check_email(){
	var email_check = document.getElementById('email').value;
	$.post("includes/email_check.php",
	{
		email:email_check
	},
	
	function(data,status){
		if(data =='<p style="color:red;">User Already exist!</p>'){
			document.getElementById("insert").disabled= true;
		}else{
			document.getElementById("insert").disabled= false;
		}
		document.getElementById("checking").innerHTML = data;
	}
	);
}





//////


///hide password
function myFunction() {
  var x = document.getElementById("pswrd");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}



</script>
<script>

/////////////////////////////////////////
$(document).on("change","#sectionhandle", function(){  
	  event.preventDefault();  
	  var secId = $('#sectionhandle').val() || [];
	  if($('#sectionhandle').val() == "")  
	  {  
	   alert("Section Required");  
	  }  
	   else  
	  {  
	   $.ajax({  
		url:"includes/check_sec_on_db.php",  
		method:"POST",  
		data:{
				secId: secId
				
		},  
		success:function(data){  
		//location.reload(); 
		// $('#update_teacher')[0].reset();  
		// $('#teacherdataModal').modal('s');  
		 // $('#teacher_details').html(data);  
		 // $('#teacherdataModal').modal('show');
		// alert(data);
		if(data == 'sect_taken'){
			$('#insert').prop('disabled', true);
			//$('#sectionhandle').empty();
			$('.btn_add_sec').prop('disabled', true);
			$('#sectionhandle').val("");
			alert('Section Already Assigned!');
			
		}else if(data == 'no_sect'){
			$('#insert').prop('disabled', true);
			$('.btn_add_sec').prop('disabled', true);
			
			$('#sectionhandle').val("");
			//$('#sectionhandle').empty();
			alert('Section cannot be found!');
			
		}else{
			$('#insert').prop('disabled', false);
			$('.btn_add_sec').prop('disabled', false);
		}
		}  
	   });  
	  }  
	 });


//////////////////

// $('document').ready(function(){
 // var username_state = false;
 
 // $('#sectionhandle').on('blur', function(){
  // var secId = $('#sectionhandle').val();
  // if (secId) {
  	// username_state = false;
  	// return;
  // }
  // $.ajax({
    // url: 'includes/check_sec_on_db.php',
    // type: 'post',
    // data: {
    	
    	// 'secId' : secId
    // },
    // success: function(response){
      // if (response == 'taken' ) {
      	// username_state = false;
      	// $('#username').parent().removeClass();
      	// $('#username').parent().addClass("form_error");
      	// $('#username').siblings("span").text('Sorry... Username already taken');
      // }else if (response == 'not_taken') {
      	// username_state = true;
      	// $('#username').parent().removeClass();
      	// $('#username').parent().addClass("form_success");
      	// $('#username').siblings("span").text('Username available');
      // }
    // }
  // });
 // });
// }); 
</script>
	<!-- <script src="../vendor/jquery/jquery.min.js"></script> -->
	  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	  <!-- Bootstrap core JavaScript -->
	 <!-- <script src="../vendor/jquery/jquery.min.js"></script> -->
	  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	 
	  <script src="../js/active_page.js"></script>
	 
	 

	</body>

</html>

