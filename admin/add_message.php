<?php
SESSION_START();
include('../includes/conn.php');
$uid = $_SESSION['uid'];
$fullname = $_SESSION['fullname'];
if(!empty($_POST))
{
 $output = '';
	$title = mysqli_real_escape_string($conn, $_POST["title"]);  
    $content = mysqli_real_escape_string($conn, $_POST["content"]);  
	$uname = mysqli_real_escape_string($conn, $_POST["uname"]);
    
    $query = "
    INSERT INTO announcements(title, content, post_creator, post_created)  
     VALUES('$title', '$content', '$uname', now())
    ";
    if(mysqli_query($conn, $query))
    {
     $output .= '<label class="text-success">Data Inserted</label>';
     $select_query = "SELECT * FROM announcements ORDER BY post_created DESC";
     $result = mysqli_query($conn, $select_query);
     $output .= '
     <table class="table table-fluid" id="myTable">
     <thead>
         <tr>
               <th>#</th>
               <th>Title</th>
               <th>Created by</th>
               <th>Post Created</th>
               <th>Action</th>
               
         </tr>
         
       </thead>
       <tbody>
     ';
     $count = 1; 
     while($row = mysqli_fetch_array($result))
     {
      
        
		$output .= '
      <tr>
        <td>'.$count.'</td>
        <td>'.$row['title'].'</td>
        <td>'.$row['post_creator'].'</td>
        <td>'.$row['post_created'].'</td>
		<td><input type="button" name="view" value="view" id="' . $row["id"] . '" class="btn btn-info btn-xs view_data" /></td>  
        
      </tr>';
      $count++;
     }
     $output .= '</tbody>
     
     <tfoot>
     
        <tr>
              <th>#</th>
              <th>Title</th>
              <th>Created by</th>
              <th>Post Created</th>
              <th>Action</th>
        </tr>
      
      </tfoot>
     
     </table>
     
     
     ';
    }
    echo $output;
}
?>