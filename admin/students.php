<?php  
session_start();
include('../includes/conn.php');
include('includes/password_generate.php');
//index.php

if(!isset($_SESSION['error'])){
	$_SESSION['error'] = "";
}
if(!isset($_SESSION['success'])){
	$_SESSION['success'] = "";
}

$query = "SELECT * FROM users WHERE type='2' ORDER BY sname ASC";
$result = mysqli_query($conn, $query);
 ?>  
<!DOCTYPE html>
<html lang="en">

<?php
include('includes/head.php');?>

	<body>
		<!-- Header bar -->
	  <?php include('header.php');?>
	  <!-- end of  Header bar -->
	  <div class="d-flex" id="wrapper">
		
			<!-- Sidebar -->
			<?php include('menu.php');?>
			<!-- /#sidebar-wrapper -->

			<!-- Page Content -->
			<div id="page-content-wrapper">
				<div class="container" >  
				   <h2>List of Students</h2>
				 
				<div align="left">
				 <button type="button" name="age" id="age" data-toggle="modal" data-target="#add_student_Modal" class="btn btn-primary">Add Student</button>
				 
				 <button type="button" name="age" id="age" data-toggle="modal" data-target="#add_student_many_Modal" class="btn btn-primary">Upload EXCEL/CSV</button>
				 
				 <button type="button" name="age" id="age" data-toggle="modal" data-target="#export_student_Modal" class="btn btn-primary">EXPORT EXCEL/CSV</button>
					<div  style="width:500px; float:right; padding:5px;" id="mgss">
							<?php
							
							if($_SESSION['success']){
								echo "<h4 class='btn-success'>".$_SESSION['success']."<h4>"; unset($_SESSION['success']);
							}else{
								echo "<h4 class='btn-warning'>".$_SESSION['error']."</h4>"; unset($_SESSION['error']);
							}
							 ?>
						
					</div>
				</div>
				<br />
				<table class="table table-fluid" id="studentlist" >
					<thead>
						<tr>
							<th>#</th>
							<th>NAME</th>
							<th>EMAIL</th>
							<th>SECTION</th>
							<th >Action</th>  
						</tr>
					</thead>
					
					<tbody>
						  <?php
						  $count = 1;
						  while($row = mysqli_fetch_array($result))
						  {
						  ?>
						  
							<tr>
								<td><?php echo $count;?></td>
								<td><?php echo $row['fname']." ".$row['sname'];?></td>
								<td><?php echo $row['email'];?></td>
								<td><?php echo $row['section'];?></td>
							  
								<td>
								   
								   <button type="submit" data-id="<?php echo $_GET["id"]; ?>" id="<?php echo $row["id"]; ?>" class=" btn-info btn-xs view_data"  title="UPDATE"><i class="fa fa-pencil"></i></button>
								   <button type="submit" data-id="<?php echo $_GET["id"]; ?>" id="<?php echo $row["id"]; ?>" class=" btn-warning btn-xs archive_data" title="ARCHIVE" ><i class="fa fa-trash"></i></button>
							   </td>
							  
							</tr>
						  <?php
						  $count++;
						  }
					  ?>
					  </tbody>
					  
					  <tfoot>
					 
							<tr>
							  <th>#</th>
							<th>NAME</th>
							<th>EMAIL</th>
							<th>SECTION </th>
							<th >Action</th>
							</tr>
					  
					  </tfoot>
				</table>
					
					
				</div> 
			</div>
			<!-- /#page-content-wrapper -->
			  <div class="bg-light border-right" id="sidebar-wrapper">
				<?php include_once('../includes/bot.php');?>
			  </div>
	  </div>
	  <!-- /#wrapper -->
	<div id="add_student_Modal" class="modal fade" tabindex="-1">
		
			<div class="modal-dialog  modal-lg">
				<div class="modal-content" >
					<div class="modal-header">
						<h4 class="modal-title">Add Student</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<br />
		
					</div>
					<div class="modal-body">
						<form method="POST" id="add_student" action="add_student.php">
						  <div class="container-fluid">
							<div class="row">
							  <div class="col-lg ">
							  <input type="hidden" name="uid" id="uid" class="form-control" value="<?php echo $_GET['id']?>"  />
								<label >SURNAME</label><br/>
									<input type="text" name="sname" id="sname" class="form-control" required />
							  </div>
							  <div class="col-md ">
							  <label >FIRST NAME</label><br/>
									<input type="text" name="fname" id="fname" class="form-control" required />
							  </div>
							  <div class="col-md ">
								<label >MIDDLE NAME</label><br/>
									<input type="text" name="mname" id="mname" class="form-control" />
							  </div>
							</div>  
						  </div>
						  <br>
						  
						  <div class="container-fluid">
							<div class="row">
							  <div class="col-md ">
								<label >EMAIL/USERNAME</label><br/>
									<input type="email" onkeyup="check_email();" name="email" id="email" class="form-control col-sm-6"  required />
									<div id="checking"></div>
							  </div>
							</div>  
						  </div>
						  <br>
						   <div class="container-fluid">
							<div class="row">
							  <div class="col-md ">
								<label >PASSWORD (AUTO GENERATED)</label><br/>
									<input type="password" name="pswrd" id="pswrd" value="<?php echo generatePassword();?>" class="form-control col-sm-6" required />
									
									 <span toggle="#pswrd" class="fa fa-fw fa-eye field-icon toggle-password" onclick="myFunction()" title="Show Password"></span>
							  </div>
							</div>  
						  </div>
						  
						  <div class="container-fluid">
							<div class="row">
							  <div class="col-md " id="sections">
								<label >SECTION</label><br/>
								
									<input type="text" id="section" name="section"  class="form-control col-sm-6" required />
									
							  </div>
							</div>  
						  </div>
						  <br/>
						  <div class="container-fluid">
							<div class="row">
							  <div class="col-md " id="sections">
								<input type="submit" name="insert" id="insert" value="Insert" class="btn btn-success" />
									
							  </div>
							</div>  
						  </div>
						</form> 
					</div>
				   <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				   </div>
				</div>
			</div>
			
	</div>
	
	
	<div id="add_student_many_Modal" class="modal fade" tabindex="-1">
		
			<div class="modal-dialog  modal-lg">
				<div class="modal-content" >
					<div class="modal-header">
						<h4 class="modal-title">Add Student via Uploading</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<br />
		
					</div>
					<div class="modal-body">
						<?php //include('open-excel-sheet-in-browser/php_spreadsheet_import.php');?>
						<iframe width="100%" height="350px" src="open-excel-sheet-in-browser/php_spreadsheet_import.php?id=<?php echo $_GET['id'];?>"></iframe>
					</div>
				   <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				   </div>
				</div>
			</div>
			
	</div>
	
	
	<div id="export_student_Modal" class="modal fade" tabindex="-1">
		
			<div class="modal-dialog  modal-lg">
				<div class="modal-content" >
					<div class="modal-header">
						<h4 class="modal-title">Export Student List</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<br />
		
					</div>
					<div class="modal-body">
						<?php //include('open-excel-sheet-in-browser/php_spreadsheet_import.php');?>
						<iframe width="100%" height="350px" src="open-excel-sheet-in-browser/php_spreadsheet_export.php?id=<?php echo $_GET['id'];?>"></iframe>
					</div>
				   <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				   </div>
				</div>
			</div>
			
	</div>
	
	
	<div id="StudentdataModal" class="modal fade">
	 <div class="modal-dialog modal-lg">
	  <div class="modal-content">
	   <div class="modal-header">
	   <h4 class="modal-title">Student's Details</h4>
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		
	   </div>
	   <div class="modal-body" id="student_details">
		
	   </div>
	   <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	   </div>
	  </div>
	 </div>
	</div>
<!-----DELETE MODAL------------->

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal">
  <div class="modal-dialog modal-l">
    <div class="modal-content">
      <div class="modal-header">
	  <h4 class="modal-title" id="myModalLabel">Are you Sure to Delete?</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       
      </div>
	  <div class="modal-body">
		<form method="post"id="yess_nos" action="delete_student.php">
		<input type="hidden" id="sid" name="sid">
		<input type="hidden" id="uid" name="uid" value="<?php echo $_GET['id']?>">
		<button type="submit" class="btn btn-warning" data-id=""id="modal-btn-si">Yes</button>
        <button type="button" class="btn btn-primary" id="modal-btn-no">No</button>
		</form>
	  </div>
      <div class="modal-footer">
       
      </div>
    </div>
  </div>
</div>

<!-------DELETE MODAL ENDS HERE------------->
	<script>
		$(document).ready( function () {
		$('#studentlist').DataTable();
	} );

	$(document).on('click', '.view_data', function(){
	 $('#StudentdataModal').modal();
	  var student_id = $(this).attr("id");
	  var uid = $(this).data("id");
	  $.ajax({
	   url:"view_student.php",
	   method:"POST",
	   data:{student_id:student_id,
	   uid:uid},
	   success:function(data){
		   $('#insert').val("Update"); 
		$('#student_details').html(data);
		$('#StudentdataModal').modal('show');
		
		
	   }
	  });
	 });
	 
	
	 /////////ADD Student/////////

	 // $(document).ready(function(){
	 // $('#add_student').on("submit", function(event){  
	  // event.preventDefault();  
	  // if($('#fname').val() == "")  
	  // {  
	   // alert("First name is required");  
	  // }  
	  // else if($('#sname').val() == '')  
	  // {  
	   // alert("Surname is required");  
	  // }  
	  // else  
	  // {  
	   // $.ajax({  
		// url:"add_student.php",  
		// method:"POST",  
		// data:$('#add_student').serialize(),  
		// beforeSend:function(){  
		 // $('#insert').val("Inserting");  
		// },  
		// success:function(data){  
			// window.location.reload(); 
		 // $('#add_student')[0].reset();  
		 // $('#add_student_Modal').modal('hide');  
		// $('#studentlist').html(data);  
		// }  
	   // });  
	  // }  
	 // });
	// });  
////////////ADD Student ends here//////////////////

	
	 $(document).on("submit","#update_teacher", function(){  
	  event.preventDefault();  
	  if($('#fname').val() == "")  
	  {  
	   alert("First name is required");  
	  }  
	  else if($('#sname').val() == '')  
	  {  
	   alert("Surname is required");  
	  }  
	  else  
	  {  
	   $.ajax({  
		url:"update_teacher.php",  
		method:"POST",  
		data:{
				tid: $('#tid').val(),
				sname: $('#sname').val(),
				fname: $('#fname').val(),
				mname: $('#mname').val(),
				email: $('#email').val(),
				pswrd: $('#pswrd').val(),
				sectionhandle: $('#sectionhandle').val(),
		},  
		beforeSend:function(){  
		 $('#updatedata').val("Updating");  
		},  
		success:function(data){  
		//location.reload(); 
		// $('#update_teacher')[0].reset();  
		// $('#teacherdataModal').modal('s');  
		 $('#teacher_details').html(data);  
		 $('#teacherdataModal').modal('show');
		 var data = JSON.parse(data);
				if(data.statusCode==200){
					$('#teacherdataModal').modal().hide();
					alert('Data updated successfully !');
					location.reload();					
				}
		}  
	   });  
	  }  
	 });
	

//////////////UPDATING TEACHER ENDS HERE

	

//////////////Confirmation MODAL
var modalConfirm = function(callback){
  
  $(".archive_data").on("click", function(){
    $("#mi-modal").modal('show');
  });

  $("#modal-btn-si").on("click", function(){
    callback(true);
    $("#mi-modal").modal('hide');
  });
  
  $("#modal-btn-no").on("click", function(){
    callback(false);
    $("#mi-modal").modal('hide');
  });
};

modalConfirm(function(confirm){
  if(confirm){
    //Acciones si el usuario confirma
	 var teacher_id = $(this).attr("id");
	 console.log(teacher_id);
	//$.ajax({  
		//url:"delete_teacher.php",  
		//method:"POST",  
		//data:$('#yess_nos').serialize(),    
		//success:function(data){  
			//window.location.reload(); 
		 //$('#delete_module')[0].reset();  
		 //$('#deleteModal').modal('hide');  
		//$('#teacherlist').html(data);  
		//}  
	 //  });  
  }else{
    //Acciones si el usuario no confirma
    // $("#result").html("NO CONFIRMADO");
  }
});
//////////Confirmation Modal ends here

// $(".archive_data").on("click", function(){
    // var dataId = $(this).attr("id");
				// $('#tid').val(dataId);
  // });
$(document).ready(function(){
			$(".archive_data").click(function(){
				var dataId = $(this).attr("id");
				$('#sid').val(dataId);
			});
			});
			
			
			
			
			
function myFunction() {
  var x = document.getElementById("pswrd");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}


//////////fade out////
$(document).ready(function(){
 
   $("#mgss").fadeOut(5000);
});
/////////////fadout ends here





	</script>
<script>
document.getElementById("insert").disabled= true;
////////////check if email exist in d
function check_email(){
	var email_check = document.getElementById('email').value;
	$.post("includes/email_check.php",
	{
		email:email_check
	},
	
	function(data,status){
		if(data =='<p style="color:red;">User Already exist!</p>'){
			document.getElementById("insert").disabled= true;
		}else{
			document.getElementById("insert").disabled= false;
		}
		document.getElementById("checking").innerHTML = data;
	}
	);
}
//////
</script>
	<!-- <script src="../vendor/jquery/jquery.min.js"></script> -->
	  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	  <!-- Bootstrap core JavaScript -->
	 <!-- <script src="../vendor/jquery/jquery.min.js"></script> -->
	  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	 
	  <script src="../js/active_page.js"></script>
	 
	 

	</body>

</html>

