<?php
// connect to the database
include('../includes/conn.php');

// Uploads files
if (isset($_POST['insert'])) { // if save button on the form is clicked
    // name of the uploaded file
    $filename = $_FILES['add_file']['name'];

    // destination of the file on the server
    $destination = '../files/uploads/' . $filename;

    // get the file extension
    $extension = pathinfo($filename, PATHINFO_EXTENSION);

    // the physical file on a temporary uploads directory on the server
    $file = $_FILES['add_file']['tmp_name'];
    $size = $_FILES['add_file']['size'];

    if (!in_array($extension, ['zip', 'pdf', 'docx', 'txt'])) {
        echo "You file extension must be .zip, .pdf or .docx";
    } elseif ($_FILES['add_file']['size'] > 50000000) { // file shouldn't be larger than 50Megabyte
        echo "File too large!";
    } else {
        // move the uploaded (temporary) file to the specified destination
        if (move_uploaded_file($file, $destination)) {
            $sql = "INSERT INTO files (mid, fname, title, descrip, file_creator, file_created) 
            VALUES ('1', '$filename', '$filename', 'Sample Desc', '3', now())";
            if (mysqli_query($conn, $sql)) {
                echo "File uploaded successfully";
            }
        } else {
            echo "Failed to upload file.";
        }
    }
}