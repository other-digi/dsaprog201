<?php include 'filesLogic.php';?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <link rel="stylesheet" href="style.css">
    <title>Files Upload and Download</title>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  </head>
  <body>
    <div class="container">
      <div class="row">
        <form action="index.php" method="post" enctype="multipart/form-data" >
          <h3>Upload File</h3>
          <input type="file" name="add_file"> <br>
          <button type="submit" name="insert">upload</button>
        </form>
      </div>
    </div>
	
	
	
<div>
    <div>
        <input type="text" id="tbTableName" placeholder="Enter Table Name" />
        <input type="button" id="btAdd" value="Add Field" class="bt" />
    </div>

    <%--THE CONTAINER TO HOLD THE DYNAMICALLY CREATED ELEMENTS.--%>
    <div id="main"></div>
</div>
<script>
    $(document).ready(function () {
        BindControls();
    });

    function BindControls() {

        var itxtCnt = 0;    // COUNTER TO SET ELEMENT IDs.

        // CREATE A DIV DYNAMICALLY TO SERVE A CONTAINER TO THE ELEMENTS.
        var container = $(document.createElement('div')).css({
            width: '20%',
            clear: 'both',
            'margin-top': '10px',
            'margin-bottom': '10px'
        });

        // CREATE THE ELEMENTS.
        $('#btAdd').click(function () {
            itxtCnt = itxtCnt + 1;

            $(container).append('<input type="text"' +
                'placeholder="Field Name" class="input" id=tb' + itxtCnt + ' value="" />');

            if (itxtCnt == 1) {
                var divSubmit = $(document.createElement('div'));
                $(divSubmit).append('<input type="button" id="btSubmit" value="Submit" class="bt"' +
                    'onclick="getTextValue()" />');
            }

            // ADD EVERY ELEMENT TO THE MAIN CONTAINER.
            $('#main').after(container, divSubmit);
        });
    }

    // THE FUNCTION TO EXTRACT VALUES FROM TEXTBOXES AND POST THE VALUES (TO A WEB METHOD) USING AJAX.
    var values = new Array();
    function getTextValue() {
        $('.input').each(function () {
            if (this.value != '')
                values.push(this.value);
        });

        if (values != '') {
            // NOW CALL THE WEB METHOD WITH THE PARAMETERS USING AJAX.
            $.ajax({
                type: 'POST',
                url: 'default.aspx/loadFields',
                data: "{'fields':'" + values + "', 'table': '" + $('#tbTableName').val() + "'}",
                dataType: 'json',
                headers: { "Content-Type": "application/json" },
                success: function (response) {
                    values = [];    // EMPTY THE ARRAY.
                    alert(response.d);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(errorThrown);
                }
            });
        }
        else { alert("Fields cannot be empty.") }
    }
</script>
  </body>
</html>